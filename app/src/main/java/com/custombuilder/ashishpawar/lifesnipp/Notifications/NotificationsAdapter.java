package com.custombuilder.ashishpawar.lifesnipp.Notifications;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetView;
import com.custombuilder.ashishpawar.lifesnipp.Profile.ProfileView;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/6/17.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NotificationsItem> notificationsItems;
    private Context context;

    public NotificationsAdapter(ArrayList<NotificationsItem> notificationsItems) {
        this.notificationsItems = notificationsItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.notifications_single_item,parent,false);

        return new NotificationsHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final NotificationsHolder notificationsHolder=(NotificationsHolder)holder;
        context=notificationsHolder.ivType.getContext();

        /*
            setting notification text
         */
        notificationsHolder.tvNotification.setText(notificationsItems.get(position).getNotificationContent());

        /*
            setting on click listener to notifications
         */
        notificationsHolder.llNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notificationsItems.get(notificationsHolder.getAdapterPosition()).getNotifType().equals("like")||notificationsItems.get(notificationsHolder.getAdapterPosition()).getNotifType().equals("comment")||notificationsItems.get(notificationsHolder.getAdapterPosition()).getNotifType().equals("view")) {
                    DbHelper dbHelper = new DbHelper(context);
                    SnippetItem snippetItem = dbHelper.getSnippet(notificationsItems.get(notificationsHolder.getAdapterPosition()).getSnippet_user_ID(), "timeline");
                    Intent i = new Intent(context, SnippetView.class);
                    i.putExtra("snippetInfo", snippetItem);
                    i.putExtra("origin", "timeline");
                    context.startActivity(i);
                }else if(notificationsItems.get(notificationsHolder.getAdapterPosition()).getNotifType().equals("follow")){
                    Intent i=new Intent(context, ProfileView.class);
                    i.putExtra("userID",notificationsItems.get(notificationsHolder.getAdapterPosition()).getSnippet_user_ID());
                    context.startActivity(i);
                }
            }
        });


        /*
            loading image either profile pic, heart, view, or comment
         */
        if(notificationsItems.get(position).getNotifType().equals("follow")){
            Drawable defaultProfPic=context.getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(context).load("http://www.lifesnipp.com/profile_pic/"+notificationsItems.get(position).getSnippet_user_ID()+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(notificationsHolder.ivType) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    notificationsHolder.ivType.setImageDrawable(circularBitmapDrawable);
                }
            });
        }else if(notificationsItems.get(position).getNotifType().equals("like"))
            notificationsHolder.ivType.setImageResource(R.mipmap.heart);
        else if(notificationsItems.get(position).getNotifType().equals("comment"))
            notificationsHolder.ivType.setImageResource(R.mipmap.comments);
        else if(notificationsItems.get(position).getNotifType().equals("view"))
            notificationsHolder.ivType.setImageResource(R.mipmap.views);


        /*
            setting date and time
         */
        notificationsHolder.tvDateTime.setText(convertDate(notificationsItems.get(position).getDate(),2)+", "+notificationsItems.get(position).getTime());

    }

    @Override
    public int getItemCount() {
        return notificationsItems.size();
    }

    private class NotificationsHolder extends RecyclerView.ViewHolder {
        TextView tvNotification;
        ImageView ivType;
        LinearLayout llNotifications;
        TextView tvDateTime;
        public NotificationsHolder(View itemView) {
            super(itemView);
            tvNotification=(TextView)itemView.findViewById(R.id.tvNotification);
            ivType =(ImageView)itemView.findViewById(R.id.ivTypeNotification);
            llNotifications=(LinearLayout)itemView.findViewById(R.id.llNotification);
            tvDateTime=(TextView)itemView.findViewById(R.id.tvDateTimeNotif);
        }

    }

    public void update(ArrayList<NotificationsItem> notificationsItems){
        this.notificationsItems=notificationsItems;
        this.notifyDataSetChanged();
    }

    private String convertDate(String time, int type) {
        String inputPattern = null;
        String outputPattern = null;
        if (type == 1) {
            inputPattern = "MMM dd";
            outputPattern = "yyyy-MM-dd";
        } else if (type == 2) {
            outputPattern = "MMM dd";
            inputPattern = "yyyy-MM-dd";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
