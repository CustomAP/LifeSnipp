package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 3/7/17.
 */

public class ProfilePicEvent {
    boolean imageUploaded;

    public boolean isImageUploaded() {
        return imageUploaded;
    }

    public ProfilePicEvent(boolean imageUploaded) {

        this.imageUploaded = imageUploaded;
    }
}
