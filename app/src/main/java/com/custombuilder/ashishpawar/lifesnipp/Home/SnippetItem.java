package com.custombuilder.ashishpawar.lifesnipp.Home;

import java.io.Serializable;

/**
 * Created by Ashish Pawar on 3/18/2017.
 */

public class SnippetItem implements Serializable{
    private int snippetID,userID;
    private String content,views, personName,likes,comments,date,time,title,category,userInfo;
    private int isLiked,anonymous;

    public String getViews() {
        return views;
    }


    public int isLiked() {
        return isLiked;
    }

    public int getAnonymous() {
        return anonymous;
    }

    public SnippetItem(int snippetID, int userID, String content, String views, String personName, String likes, String comments, String date, String time, String title, String category, String userInfo, int isLiked,int anonymous) {
        this.snippetID = snippetID;
        this.userID = userID;
        this.content = content;
        this.views = views;
        this.personName = personName;
        this.likes = likes;
        this.comments = comments;
        this.date = date;
        this.time = time;
        this.title = title;
        this.category = category;
        this.userInfo = userInfo;
        this.isLiked=isLiked;
        this.anonymous=anonymous;

    }

    public int getSnippetID() {
        return snippetID;
    }

    public int getUserID() {
        return userID;
    }

    public String getComments() {
        return comments;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public String getContent() {
        return content;
    }

    public String getPersonName() {
        return personName;
    }

    public String getLikes() {
        return likes;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }
}
