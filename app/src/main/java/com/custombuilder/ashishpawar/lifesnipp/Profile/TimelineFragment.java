package com.custombuilder.ashishpawar.lifesnipp.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ShareEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.TimelineEvent;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetAdapter;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.FetchTimelineSnippets;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/6/17.
 */

public class TimelineFragment extends Fragment {

    public RecyclerView rvTimeline;
    public  ArrayList<SnippetItem> snippetItems;
    DbHelper dbHelper;
    private int selfUserID,userID;
    private String origin;
    private CardView cvEmptyTimeline;
    private TextView tvEmptyTimeline;
    public SnippetAdapter adapter;
    private int num=0;
    AVLoadingIndicatorView avi;
    LinearLayout llTimelineFragment;
    private boolean loading = true;
    PullRefreshLayout prlTimeline;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_timeline,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            getting arguments passed from profile view activity or main activity.
         */
        origin=getArguments().getString("origin");
        userID=getArguments().getInt("userID");

        /*
            getting self userID
         */
        SharedPreferences preferences =getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        selfUserID =preferences.getInt("UserID",0);

        /*
            initializing views
         */
        avi=(AVLoadingIndicatorView)view.findViewById(R.id.aviTimeline);
        llTimelineFragment=(LinearLayout)view.findViewById(R.id.llTimelineFragment);
        rvTimeline = (RecyclerView) view.findViewById(R.id.rvTimeline);
        cvEmptyTimeline=(CardView)view.findViewById(R.id.cvEmptyTimeline);
        tvEmptyTimeline=(TextView)view.findViewById(R.id.tvEmptyTimeline);
        prlTimeline=(PullRefreshLayout)view.findViewById(R.id.prlTimeline);
        prlTimeline.setColor(R.color.colorPrimaryDark);

        /*
            initializing database helper and array list
         */
        snippetItems = new ArrayList<>();
        dbHelper=new DbHelper(getContext());

        /*
            showing loading indicator till data is fetched from server
         */
        if(origin.equals("ProfileView")){
            llTimelineFragment.setVisibility(View.GONE);
            avi.show();
            getDataFromServer();
        }

        /*
            setting recycler view
         */
        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        rvTimeline.setLayoutManager(manager);
        rvTimeline.setItemAnimator(new DefaultItemAnimator());

        /*
            getting timeline snippets from database
         */
        snippetItems=dbHelper.getTimelineSnippets(userID);

        /*
            showing no snippets message if snippet items are empty
         */
        if(!snippetItems.isEmpty())
            cvEmptyTimeline.setVisibility(View.GONE);

        /*
            setting adapter
         */
        adapter=new SnippetAdapter(snippetItems,"timeline");

        rvTimeline.setAdapter(adapter);

        /*
            hiding no snippets message if origin is from profile view activity
         */
        if(origin.equals("ProfileView")) {
            tvEmptyTimeline.setText(R.string.empty_timeline_other_users);
        }else{
            avi.hide();
            avi.setVisibility(View.GONE);
            llTimelineFragment.setVisibility(View.VISIBLE);
        }

        /*
            adding onscrolllistener if origin is profile view
         */
        if(origin.equals("ProfileView")) {
            rvTimeline.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    int pastVisiblesItems, visibleItemCount, totalItemCount;
                    if (dy > 0) //check for scroll down
                    {
                        visibleItemCount = manager.getChildCount();
                        totalItemCount = manager.getItemCount();
                        pastVisiblesItems = manager.findFirstVisibleItemPosition();

                        if (loading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                loading = false;
                                Log.d("timeline", "Last Item Wow !");
                                num++;
                                getDataFromServer();
                            }
                        }
                    }
                }
            });
        }

        /*
            setting on refresh listener
         */
        prlTimeline.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FetchTimelineSnippets fetchTimelineSnippets=new FetchTimelineSnippets(getContext());
                if(userID==selfUserID)
                    num=-1;
                fetchTimelineSnippets.getTimelineSnippets(userID,num,origin+"_refresh");
            }
        });

    }

    private void getDataFromServer() {
        FetchTimelineSnippets timelineSnippets=new FetchTimelineSnippets(getContext());
        timelineSnippets.getTimelineSnippets(userID,num,origin);
    }

    public void fillList() {
        snippetItems.clear();
        snippetItems=dbHelper.getTimelineSnippets(userID);
        adapter.updateAdapter(snippetItems);
        if(!snippetItems.isEmpty())
            cvEmptyTimeline.setVisibility(View.GONE);
    }

    public void onEvent(ShareEvent shareEvent){
        boolean isSnippetShared= shareEvent.isSharedSnippet();
        if(isSnippetShared){
            fillList();
        }
    }


    public void onEvent(TimelineEvent timelineEvent){

        /*
            filling list when data is received, hiding loading indicator, setting timeline fragment visibility to visible
         */
        boolean isDataReceived=timelineEvent.isReceivedData();
        if(isDataReceived){
            avi.hide();
            llTimelineFragment.setVisibility(View.VISIBLE);
            avi.setVisibility(View.GONE);
            fillList();
            loading=true;

            /*
                setting refresh to false
             */
            try{
                prlTimeline.setRefreshing(false);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
            registering event bus with fragment
         */
        try {
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {

        /*
            updating recycler view after snippet view refreshed.
        */
        super.onResume();
        snippetItems=dbHelper.getTimelineSnippets(userID);
        adapter.updateAdapter(snippetItems);
    }

    public void onEvent(FetchFailed fetchFailed){
        if(fetchFailed.isRefreshFailed()){
            prlTimeline.setRefreshing(false);
        }
    }
}
