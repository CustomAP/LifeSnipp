package com.custombuilder.ashishpawar.lifesnipp.Notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.custombuilder.ashishpawar.lifesnipp.EventBus.NotificationEvent;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar on 3/12/2017.
 */

public class NotificationsFragment extends Fragment {
    RecyclerView rvNotifications;
    private ArrayList<NotificationsItem> notificationsItems;
    CardView cvNoNotifications;
    NotificationsAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing recycler view
         */
        rvNotifications= (RecyclerView) view.findViewById(R.id.rvNotifications);
        cvNoNotifications=(CardView)view.findViewById(R.id.cvNoNotification);

        /*
            setting recycler view
         */
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getContext());
        rvNotifications.setItemAnimator(new DefaultItemAnimator());
        rvNotifications.setLayoutManager(layoutManager);

        notificationsItems=new ArrayList<>();

        /*
            setting recycler view items
         */
        fillList();

        /*
            setting no notifications visibility gone if notifications item is not empty
         */
        if(!notificationsItems.isEmpty())
            cvNoNotifications.setVisibility(View.GONE);

        /*
            setting adapter
         */
        adapter=new NotificationsAdapter(notificationsItems);
        rvNotifications.setAdapter(adapter);
    }

    private void fillList() {
        DbHelper dbHelper=new DbHelper(getContext());
        notificationsItems=dbHelper.getNotifications();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onEvent(NotificationEvent notificationEvent){
        notificationsItems=notificationEvent.getNotificationsItems();
        adapter.update(notificationsItems);
        /*
            setting no notifications visibility gone if notifications item is not empty
         */
        if(!notificationsItems.isEmpty())
            cvNoNotifications.setVisibility(View.GONE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser){
            DbHelper dbHelper=new DbHelper(getContext());
            notificationsItems=dbHelper.getNotifications();
            adapter.update(notificationsItems);
        }
    }
}
