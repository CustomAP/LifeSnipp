package com.custombuilder.ashishpawar.lifesnipp.Profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 9/7/17.
 */

public class Settings extends AppCompatActivity implements View.OnClickListener{
    LinearLayout llDisplayName,llBio,llPassword,llBirthDate,llRate,llReport,llLegal,llPermissions,llAds,llAppInfo;
    TextView tvDisplayName,tvBio,tvBirthDate;
    MaterialEditText etOldPw,etNewPw,etNewPwConfirm,etFirstName,etLastName;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    /*
        links
     */
    String changeDisplayName="http://www.lifesnipp.com/index.php/User_profile/editDisplayName";
    String editBio="http://www.lifesnipp.com/index.php/User_profile/editBio";
    String editBirthDate="http://www.lifesnipp.com/index.php/User_profile/editBirthDate";
    String changePw="http://www.lifesnipp.com/index.php/User_login/changePassword";
    String logout="http://www.lifesnipp.com/index.php/User_login/logout";

    AQuery aQuery;
    String sessionIDString;
    String birthDate=null;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialize();
        setUI();
    }

    private void initialize() {
        /*
            initialize views
         */
        llDisplayName=(LinearLayout)findViewById(R.id.llDisplayNameSettings);
        llBio=(LinearLayout)findViewById(R.id.llBioSettings);
        llPassword=(LinearLayout)findViewById(R.id.llPasswordSettings);
        llBirthDate=(LinearLayout)findViewById(R.id.llBirthDateSettings);
        llRate=(LinearLayout)findViewById(R.id.llRateSettings);
        llReport=(LinearLayout)findViewById(R.id.llReportSettings);
        llLegal=(LinearLayout)findViewById(R.id.llLegalSettings);
        llPermissions=(LinearLayout)findViewById(R.id.llPermissionsSettings);
        llAds=(LinearLayout)findViewById(R.id.llAdsSettings);
        llAppInfo=(LinearLayout)findViewById(R.id.llAppInfoSettings);
       // llLogout=(LinearLayout)findViewById(R.id.llLogoutSettings);
        tvDisplayName=(TextView)findViewById(R.id.tvDisplayNameSettings);
        tvBio=(TextView)findViewById(R.id.tvBioSettings);
        tvBirthDate=(TextView)findViewById(R.id.tvBirthDateSettings);

        preferences=getSharedPreferences("Profile", Context.MODE_PRIVATE);

        /*
            setting on click listeners
         */
        llDisplayName.setOnClickListener(this);
        llBio.setOnClickListener(this);
        llBirthDate.setOnClickListener(this);
        llPassword.setOnClickListener(this);
        llRate.setOnClickListener(this);
        llReport.setOnClickListener(this);
        llLegal.setOnClickListener(this);
        llPermissions.setOnClickListener(this);
        llAds.setOnClickListener(this);
        llAppInfo.setOnClickListener(this);
       // llLogout.setOnClickListener(this);

        /*
            initialize
         */
        aQuery=new AQuery(getApplicationContext());
        SessionID sessionID=new SessionID(getApplicationContext());
        sessionIDString=sessionID.getSessionID();

    }

    private void setUI() {
        tvDisplayName.setText(preferences.getString("firstName","")+" "+preferences.getString("lastName",""));
        tvBirthDate.setText(preferences.getString("birthDate",""));
        tvBio.setText(preferences.getString("bio",""));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llDisplayNameSettings:
                /*
                    change display name
                 */
                dialog=new LovelyCustomDialog(this)
                        .setView(R.layout.fragment_change_display_name)
                        .configureView(new LovelyCustomDialog.ViewConfigurator() {
                            @Override
                            public void configureView(View v) {
                                etFirstName=(MaterialEditText)v.findViewById(R.id.etFirstNameSettings);
                                etLastName=(MaterialEditText)v.findViewById(R.id.etLastNameSettings);

                                etFirstName.setText(preferences.getString("firstName",""));
                                etLastName.setText(preferences.getString("lastName",""));
                            }
                        })
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.mipmap.display_name)
                        .setListener(R.id.tvSaveChangeDisplayNameSettings, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final String firstName=etFirstName.getText().toString();
                                final String lastName=etLastName.getText().toString();

                                if(firstName.trim().isEmpty())
                                    Toast.makeText(getApplicationContext(),"Firstname cannot be blank",Toast.LENGTH_SHORT).show();
                                else if(lastName.trim().isEmpty())
                                    Toast.makeText(getApplicationContext(),"Lastname cannot be blank",Toast.LENGTH_SHORT).show();
                                else{
                                    Map<String,Object> displayNameParams=new HashMap<>();
                                    displayNameParams.put("sessionID",sessionIDString);
                                    displayNameParams.put("firstName",firstName);
                                    displayNameParams.put("lastName",lastName);

                                    aQuery.ajax(changeDisplayName,displayNameParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                                        @Override
                                        public void callback(String url, JSONObject object, AjaxStatus status) {
                                            int code=0;

                                            if(object!=null){
                                                Log.d("changeDisplayName","object!=null");
                                                try{
                                                    code=object.getInt("ResultSet");
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }
                                            }else{
                                                Log.d("changeDisplayName","object=null");
                                                Toast.makeText(getApplicationContext(),"Failed to change display name",Toast.LENGTH_SHORT).show();
                                            }

                                            if(status.getCode()==200&&status.getCode()<300){
                                                if(code==0){
                                                    editor=preferences.edit();
                                                    editor.putString("firstName",firstName);
                                                    editor.putString("lastName",lastName);
                                                    editor.apply();

                                                    Toast.makeText(getApplicationContext(),"Display name changed successfully",Toast.LENGTH_SHORT).show();

                                                    tvDisplayName.setText(preferences.getString("firstName","")+" "+preferences.getString("lastName",""));

                                                    dialog.dismiss();
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .show();
                break;
            case R.id.llBioSettings:
                /*
                    change bio
                 */
                new LovelyTextInputDialog(this)
                        .setTopColorRes(R.color.colorPrimary)
                        .setTitle(R.string.bio)
                        .setIcon(R.mipmap.info)
                        .setInitialInput(preferences.getString("bio",""))
                        .setConfirmButton(R.string.save, new LovelyTextInputDialog.OnTextInputConfirmListener() {
                            @Override
                            public void onTextInputConfirmed(final String text) {
                                Map<String,Object> bioParams=new HashMap<>();
                                bioParams.put("sessionID",sessionIDString);
                                bioParams.put("bio",text);
                                aQuery.ajax(editBio,bioParams, JSONObject.class,new AjaxCallback<JSONObject>(){
                                    @Override
                                    public void callback(String url, JSONObject object, AjaxStatus status) {
                                        int code=0;

                                        if(object!=null){
                                            try{
                                                Log.d("editBio","object!=null");
                                                code=object.getInt("ResultSet");
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }else{
                                            Log.d("editBio","object=null");
                                            Toast.makeText(getApplicationContext(),"Failed to edit bio",Toast.LENGTH_SHORT).show();
                                        }
                                        if(status.getCode()==200&&status.getCode()<300){
                                            if(code==0){
                                                editor=preferences.edit();
                                                editor.putString("bio",text);
                                                editor.apply();
                                                tvBio.setText(text);
                                            }
                                        }
                                    }
                                });
                            }
                        })
                        .show();
                break;
            case R.id.llPasswordSettings:
                /*
                    change password
                 */
                 dialog=new LovelyCustomDialog(this)
                        .setView(R.layout.fragment_change_pw)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.mipmap.password)
                        .setTitle(R.string.password)
                        .configureView(new LovelyCustomDialog.ViewConfigurator() {
                            @Override
                            public void configureView(View v) {
                                etOldPw=(MaterialEditText)v.findViewById(R.id.etOldPwSettings);
                                etNewPw=(MaterialEditText)v.findViewById(R.id.etNewPwSettings);
                                etNewPwConfirm=(MaterialEditText)v.findViewById(R.id.etNewPwConfirmSettings);

                                /*
                                    setting input type as password
                                 */
                                etOldPw.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                etOldPw.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                etNewPw.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                etNewPw.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                etNewPwConfirm.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                etNewPwConfirm.setTransformationMethod(PasswordTransformationMethod.getInstance());

                                etNewPwConfirm.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        String newPw=etNewPw.getText().toString();
                                        String newPwConfirm=etNewPwConfirm.getText().toString();

                                        if(!newPw.equals(newPwConfirm))
                                            etNewPwConfirm.setError("Password does not match");
                                        else
                                            etNewPwConfirm.setError(null);
                                    }
                                });
                            }
                        })
                        .setListener(R.id.tvSaveChangePwSettings, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String oldPw=etOldPw.getText().toString();
                                final String newPw=etNewPw.getText().toString();
                                String newPwConfirm=etNewPwConfirm.getText().toString();

                                if(oldPw.trim().isEmpty())
                                    Toast.makeText(getApplicationContext(),"Old password cannot be blank",Toast.LENGTH_SHORT).show();
                                else if (newPw.length()<6&&newPwConfirm.length()<6)
                                    Toast.makeText(getApplicationContext(),"Password should be minimum 6 characters",Toast.LENGTH_SHORT).show();
                                else if(!newPw.equals(newPwConfirm))
                                    Toast.makeText(getApplicationContext(),"Password does not match",Toast.LENGTH_SHORT).show();
                                else{
                                    Map<String,Object> passwordParams=new HashMap<>();
                                    passwordParams.put("sessionID",sessionIDString);
                                    passwordParams.put("oldPw",oldPw);
                                    passwordParams.put("newPw",newPw);

                                    aQuery.ajax(changePw,passwordParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                                        @Override
                                        public void callback(String url, JSONObject object, AjaxStatus status) {
                                            int code=0;
                                            if(object!=null){
                                                try{
                                                    code=object.getInt("ResultSet");
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }
                                            }else{
                                                Log.d("changePw","object=null");
                                                Toast.makeText(getApplicationContext(),"Failed to change password",Toast.LENGTH_SHORT).show();
                                            }
                                            if(status.getCode()==200&&status.getCode()<300){
                                                if(code==0){
                                                    Toast.makeText(getApplicationContext(),"Password changed successfully",Toast.LENGTH_SHORT).show();
                                                    editor=preferences.edit();
                                                    editor.putString("password",newPw);
                                                    editor.apply();
                                                    dialog.dismiss();
                                                }else if(code==1){
                                                    Toast.makeText(getApplicationContext(),"Incorrect old password",Toast.LENGTH_SHORT).show();
                                                }else if(code==2){
                                                    Toast.makeText(getApplicationContext(),"Server error",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        })
                        .show();

                break;
            case R.id.llBirthDateSettings:
                /*
                    change birth date
                 */
                final String birthdate=preferences.getString("birthDate","");

                String year=birthdate.substring(0,birthdate.indexOf("-"));
                String month_date=birthdate.substring(birthdate.indexOf("-")+1);
                String month=month_date.substring(0,month_date.indexOf("-"));
                String date=month_date.substring(month_date.indexOf("-")+1);
                final DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            birthDate=i+"-"+i1+"-"+i2;
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            birthDate=i+"-"+i1+"-0"+i2;
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            birthDate=i+"-0"+i1+"-"+i2;
                        else
                            birthDate=i+"-0"+i1+"-0"+i2;

                        Map<String,Object> birthdateParams=new HashMap<>();
                        birthdateParams.put("sessionID",sessionIDString);
                        birthdateParams.put("birthDate",birthDate);
                        aQuery.ajax(editBirthDate,birthdateParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                int code=0;
                                if(object!=null){
                                    try{
                                        Log.d("editBirthDate","object!=null");
                                        code=object.getInt("ResultSet");
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }else{
                                    Log.d("editBirthDate","object=null");
                                    Toast.makeText(getApplicationContext(),"Failed to change birth date",Toast.LENGTH_SHORT).show();
                                }
                                if(status.getCode()==200&&status.getCode()<300){
                                    if(code==0){
                                        Toast.makeText(getApplicationContext(),"Birthdate changed successfully",Toast.LENGTH_SHORT).show();
                                        tvBirthDate.setText(birthDate);
                                        editor=preferences.edit();
                                        editor.putString("birthDate",birthDate);
                                        editor.apply();
                                    }
                                }
                            }
                        });
                    }
                },Integer.parseInt(year),Integer.parseInt(month)-1,Integer.parseInt(date));
                datePickerDialog.show();
                break;
            case R.id.llRateSettings:
                /*
                    rate app
                 */
                new LovelyStandardDialog(this, R.style.myDialog)
                        .setTopColorRes(R.color.colorPrimary)
                        .setButtonsColorRes(R.color.colorAccent)
                        .setIcon(R.mipmap.rate)
                        .setTitle("Loved this app?")
                        .setMessage("Rate it a 5 star to support us.")
                        .setPositiveButton("Rate", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                // To count with Play market backstack, After pressing back button,
                                // to taken back to our application, we need to add following flags to intent.
                                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                try {
                                    startActivity(goToMarket);
                                } catch (ActivityNotFoundException e) {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                                }
                                SharedPreferences preferences=getApplicationContext().getSharedPreferences("AppInfo",Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor=preferences.edit();
                                editor.putBoolean("isRated",true);
                                editor.apply();
                            }
                        })
                        .show();
                break;
            case R.id.llReportSettings:
                /*
                    report
                 */
                String[] emailID={"developer.ashishpawar@gmail.com"};
                Intent i=new Intent(Intent.ACTION_SEND);
                i.putExtra(Intent.EXTRA_EMAIL,emailID);
                i.setType("Plain/Text");
                startActivity(i);
                break;
            case R.id.llLegalSettings:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.lifesnipp.com/index.php/Extras/Legal"));
                startActivity(browserIntent);
                break;
            case R.id.llPermissionsSettings:
                /*
                    permissions
                 */
                new LovelyInfoDialog(this)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.mipmap.info)
                        .setTitle(R.string.app_permissions)
                        .setMessage(R.string.permissions_details)
                        .show();
                break;
            case R.id.llAdsSettings:
                /*
                    ads
                 */
                new LovelyStandardDialog(this,R.style.myDialog)
                        .setTopColorRes(R.color.colorPrimary)
                        .setButtonsColorRes(R.color.colorAccent)
                        .setIcon(R.mipmap.advertise)
                        .setTitle(R.string.advertisement)
                        .setMessage(R.string.advertise_details)
                        .setPositiveButton(R.string.contact_us, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String[] emailID={"developer.ashishpawar@gmail.com"};
                                Intent i=new Intent(Intent.ACTION_SEND);
                                i.putExtra(Intent.EXTRA_EMAIL,emailID);
                                i.setType("Plain/Text");
                                startActivity(i);
                            }
                        })
                        .show();

                break;
            case R.id.llAppInfoSettings:
                /*
                    app info
                 */
                new LovelyInfoDialog(this)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.mipmap.info)
                        .setTitle(R.string.app_info)
                        .setMessage(R.string.app_info_details)
                        .setMessageGravity(Gravity.CENTER)
                        .show();
                break;
//            case R.id.llLogoutSettings:
//                /*
//                    logout
//                 */
//                new LovelyStandardDialog(this,R.style.myDialog)
//                        .setTopColorRes(R.color.colorPrimary)
//                        .setButtonsColorRes(R.color.colorAccent)
//                        .setIcon(R.mipmap.attention)
//                        .setTitle(R.string.logout)
//                        .setMessage(R.string.logout_info)
//                        .setPositiveButton(R.string.logout_capital, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Map<String,Object> logoutParams=new HashMap<>();
//                                logoutParams.put("sessionID",sessionIDString);
//
//
//                                aQuery.ajax(logout,logoutParams,JSONObject.class,new AjaxCallback<JSONObject>(){
//                                    @Override
//                                    public void callback(String url, JSONObject object, AjaxStatus status) {
//                                        int code=0;
//
//                                        if(object!=null){
//                                            try{
//                                                Log.d("logout","object!=null");
//                                                code=object.getInt("ResultSet");
//                                            }catch (Exception e){
//                                                e.printStackTrace();
//                                            }
//                                        }else{
//                                            Log.d("logout","object=null");
//                                            Toast.makeText(getApplicationContext(),"Failed to logout",Toast.LENGTH_SHORT).show();
//                                        }
//
//                                        if(status.getCode()==200){
//                                            if(code==0){
//                                                DbHelper dbHelper = new DbHelper(getApplicationContext());
//                                                dbHelper.clearDB();
//
//                                                try {
//                                                    SharedPreferences preferences = getSharedPreferences("Profile", Context.MODE_PRIVATE);
//                                                    String path = preferences.getString("profilePic", null);
//                                                    File f = new File(path, "profilePic.jpg");
//
//                                                    f.delete();
//                                                }catch (Exception e){
//                                                    e.printStackTrace();
//                                                }
//
//                                                try {
//                                                    File sharedPreferenceFile = new File(getApplicationContext().getFilesDir().getPath() + getPackageName() + "/shared_prefs/");
//                                                    File[] listFiles = sharedPreferenceFile.listFiles();
//                                                    for (File file : listFiles) {
//                                                        Log.d("delete files","inside");
//                                                        file.delete();
//                                                    }
//                                                }catch (Exception e){
//                                                    e.printStackTrace();
//                                                }
//                                            }
//                                        }
//                                    }
//                                });
//                            }
//                        })
//                        .setNegativeButton(R.string.cancel_capital,null)
//                        .show();
//                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) // Press Back Icon
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
