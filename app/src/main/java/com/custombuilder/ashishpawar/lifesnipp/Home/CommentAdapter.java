package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.custombuilder.ashishpawar.lifesnipp.Profile.ProfileView;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.yarolegovich.lovelydialog.LovelyChoiceDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/6/17.
 */

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<CommentItem> commentItem;
    private int userID;
    private Context context;
    AQuery aQuery;
    private int anonymous;

    /*
        links
     */
    String editComment = "http://www.lifesnipp.com/index.php/Snippets/editComment";
    String deleteComment="http://www.lifesnipp.com/index.php/Snippets/deleteComment";

    public CommentAdapter(ArrayList<CommentItem> commentItem, int userID,int anonymous) {
        this.commentItem = commentItem;
        this.userID = userID;
        this.anonymous=anonymous;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_single_item, parent, false);
        return new CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final CommentHolder commentHolder = (CommentHolder) holder;
        context = commentHolder.ivProfile.getContext();

        aQuery = new AQuery(context);

        /*
            getting sessionID string from sessionID class
         */
        SessionID sessionID = new SessionID(context);
        final String sessionIDString = sessionID.getSessionID();

        /*
            setting text in views
         */
        commentHolder.tvDate.setText(convertDate(commentItem.get(position).getDate(),2));
        commentHolder.tvComment.setText(commentItem.get(position).getComment());
        commentHolder.tvPersonName.setText(commentItem.get(position).getPersonName());

        /*
            if person name != anonymous , setting profile image and setting onclick listener on person name.
         */
        if(!commentItem.get(position).getPersonName().equals("Anonymous ")) {
            Drawable defaultProfPic = context.getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(context).load("http://www.lifesnipp.com/profile_pic/" + commentItem.get(position).getUserID() + ".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(commentHolder.ivProfile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    commentHolder.ivProfile.setImageDrawable(circularBitmapDrawable);
                }
            });

            commentHolder.tvPersonName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (commentItem.get(position).getUserID() != 0) {
                        Intent i = new Intent(commentHolder.tvPersonName.getContext(), ProfileView.class);
                        i.putExtra("userID", commentItem.get(position).getUserID());
                        commentHolder.tvPersonName.getContext().startActivity(i);
                    }
                }
            });
        }

        /*
            setting personName(Anonymous) as person name
         */
        if(userID==context.getSharedPreferences("Profile",Context.MODE_PRIVATE).getInt("userID",-1)&&anonymous==1&&commentItem.get(position).getUserID()==userID){
            commentHolder.tvPersonName.setText(commentItem.get(position).getPersonName()+"\n(Anonymous)");
        }

        /*
            setting onclick listener to edit or delete comment if comment person is not anonymous
         */
        if (commentItem.get(position).getUserID() == context.getSharedPreferences("Profile", Context.MODE_PRIVATE).getInt("userID", -1)) {
            commentHolder.llCommment.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final String[] list = new String[]{"Edit", "Delete"};
                    new LovelyChoiceDialog(context)
                            .setTopColorRes(R.color.colorPrimary)
                            .setTitle("Choose an action")
                            .setIcon(R.mipmap.action)
                            .setItems(list, new LovelyChoiceDialog.OnItemSelectedListener<String>() {
                                @Override
                                public void onItemSelected(int which, String item) {
                                    switch (which) {
                                        case 0:
                                            /*
                                                edit comment
                                             */
                                            new LovelyTextInputDialog(context)
                                                    .setTopColorRes(R.color.colorPrimary)
                                                    .setTitle(R.string.edit_comment)
                                                    .setIcon(R.mipmap.edit_white)
                                                    .setInitialInput(commentItem.get(position).getComment())
                                                    .setConfirmButton(R.string.save, new LovelyTextInputDialog.OnTextInputConfirmListener() {
                                                        @Override
                                                        public void onTextInputConfirmed(final String text) {
                                                            Map<String, Object> commentParams = new HashMap<>();
                                                            commentParams.put("sessionID", sessionIDString);
                                                            commentParams.put("comment", text);
                                                            commentParams.put("commentID",commentItem.get(position).getCommentID());

                                                            aQuery.ajax(editComment, commentParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                                                                @Override
                                                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                                                    int code = 0;

                                                                    if (object != null) {
                                                                        try {
                                                                            Log.d("editComment", "object!=null");
                                                                            code = object.getInt("ResultSet");
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else {
                                                                        Log.d("editComment", "object=null");
                                                                        Toast.makeText(context, "Failed to edit comment", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                    if (status.getCode() == 200 && status.getCode() < 300) {
                                                                        if (code == 0) {
                                                                            Toast.makeText(context,"Comment edited",Toast.LENGTH_SHORT).show();
                                                                            commentHolder.tvComment.setText(text);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    })
                                                    .show();

                                            break;
                                        case 1:
                                            /*
                                                delete comment
                                             */
                                            new LovelyStandardDialog(context)
                                                    .setTopColorRes(R.color.colorPrimary)
                                                    .setButtonsColorRes(R.color.colorAccent)
                                                    .setIcon(R.mipmap.info)
                                                    .setTitle("Delete comment?")
                                                    .setMessage("This cannot be undone")
                                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            Map<String,Object> deleteCommentParams=new HashMap<>();
                                                            deleteCommentParams.put("sessionID",sessionIDString);
                                                            deleteCommentParams.put("commentID",commentItem.get(position).getCommentID());

                                                            aQuery.ajax(deleteComment,deleteCommentParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                                                                @Override
                                                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                                                    int code=0;
                                                                    if(object!=null){
                                                                        try{
                                                                            Log.d("deleteComment","object!=null");
                                                                            code=object.getInt("ResultSet");
                                                                        }catch (Exception e){
                                                                            e.printStackTrace();
                                                                        }
                                                                    }else{
                                                                        Log.d("deleteComment","object=null");
                                                                        Toast.makeText(context,"Failed to delete comment",Toast.LENGTH_SHORT).show();
                                                                    }

                                                                    if(status.getCode()==200){
                                                                        if(code==0){
                                                                            removeComment(commentItem.get(position));
                                                                            Toast.makeText(context,"Comment deleted",Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    })
                                                    .setNegativeButton("Cancel", null)
                                                    .show();
                                            break;
                                    }
                                }
                            })
                            .show();


                    return false;
                }
            });
        }

    }

    private String convertDate(String time, int type) {
        String inputPattern = null;
        String outputPattern = null;
        if (type == 1) {
            inputPattern = "MMM dd";
            outputPattern = "yyyy-MM-dd";
        } else if (type == 2) {
            outputPattern = "MMM dd";
            inputPattern = "yyyy-MM-dd";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getItemCount() {
        return commentItem.size();
    }

    private class CommentHolder extends RecyclerView.ViewHolder {
        TextView tvPersonName, tvComment, tvDate;
        CircularImageView ivProfile;
        LinearLayout llCommment;

        public CommentHolder(View itemView) {
            super(itemView);
            tvPersonName = (TextView) itemView.findViewById(R.id.tvPersonNameComment);
            tvComment = (TextView) itemView.findViewById(R.id.tvComment);
            tvDate = (TextView) itemView.findViewById(R.id.tvDateComment);
            ivProfile = (CircularImageView) itemView.findViewById(R.id.ivProfileComment);
            llCommment = (LinearLayout) itemView.findViewById(R.id.llComment);
        }
    }

    public void update(ArrayList<CommentItem> commentItems) {
        commentItem.clear();
        commentItem = commentItems;
        this.notifyDataSetChanged();
    }

    public void addComment(CommentItem commentItem1) {
        commentItem.add(commentItem1);
        this.notifyDataSetChanged();
    }

    public void removeComment(CommentItem commentItem1){
        commentItem.remove(commentItem1);
        this.notifyDataSetChanged();
    }
}
