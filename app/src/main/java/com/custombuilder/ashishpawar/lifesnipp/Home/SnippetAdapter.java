package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.ViewUsers.UsersView;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.DiarySnippet;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Snippet;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.yarolegovich.lovelydialog.LovelyChoiceDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Ashish Pawar on 3/18/2017.
 */

public class SnippetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<SnippetItem> snippetItem;
    private String origin;

    public SnippetAdapter(ArrayList<SnippetItem> snippetItem,String origin) {
        this.snippetItem = snippetItem;
        this.origin=origin;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 0) {
            View view = inflater.inflate(R.layout.snippet_single_item, parent, false);
            holder = new SnippetHolder(view);
        } else if (viewType == 1) {

        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SnippetHolder) {
            final SnippetHolder snippetHolder = (SnippetHolder) holder;

            /*
                setting person name as: Anonymous , Person Name OR Person Name(Anonymous)
             */
            if(origin.equals("timeline")&&snippetItem.get(position).getAnonymous()==0)
                snippetHolder.personName.setText(snippetItem.get(position).getPersonName());
            else if(origin.equals("timeline")&&snippetItem.get(position).getAnonymous()==1) {
                snippetHolder.personName.setText(snippetItem.get(position).getPersonName() + "\n(Anonymous)");
            }else
                snippetHolder.personName.setText(snippetItem.get(position).getPersonName());

            /*
                setting content text and likes count
             */
            snippetHolder.content.setText(snippetItem.get(position).getContent());
            snippetHolder.likes.setText(snippetItem.get(position).getLikes());

            /*
                setting isLiked checked
             */
            if(snippetItem.get(position).isLiked()>0)
                snippetHolder.heart.setChecked(true);
            else
                snippetHolder.heart.setChecked(false);

            /*
                setting views, title, category text
             */
            snippetHolder.views.setText(snippetItem.get(position).getViews());
            snippetHolder.tvTitle.setText(snippetItem.get(position).getTitle());
            snippetHolder.category.setText("#"+snippetItem.get(position).getCategory());

            /*
                getting selfUserID
             */
            SharedPreferences preferences=snippetHolder.category.getContext().getSharedPreferences("Profile",Context.MODE_PRIVATE);
            final int selfUserID=preferences.getInt("userID",0);

            /*
                setting on click listener to read more.. text
             */
            snippetHolder.readMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view){
                    String likes=snippetHolder.likes.getText().toString();
                    int isLiked=snippetItem.get(position).isLiked();

                    if(origin.equals("feed")) {
                        try {
                            int views = Integer.valueOf(snippetHolder.views.getText().toString());
                            views++;
                            snippetItem.get(position).setViews(views + "");
                            DbHelper helper = new DbHelper(snippetHolder.heart.getContext());
                            helper.view(views, snippetItem.get(position).getSnippetID(), origin);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if(origin.equals("feed")) {
                        Snippet snippet = new Snippet(snippetHolder.heart.getContext());
                        snippet.view(snippetItem.get(position).getSnippetID());
                    }
                    snippetItem.get(position).setLikes(likes);
                    snippetItem.get(position).setIsLiked(isLiked);

                    Intent i=new Intent(snippetHolder.readMore.getContext(),SnippetView.class);
                    i.putExtra("snippetInfo",snippetItem.get(position));
                    i.putExtra("origin",origin);
                    snippetHolder.readMore.getContext().startActivity(i);
                }
            });


            snippetHolder.llSnippet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String likes=snippetHolder.likes.getText().toString();
                    int isLiked=snippetItem.get(position).isLiked();

                    if(origin.equals("feed")) {
                        try {
                            int views = Integer.valueOf(snippetHolder.views.getText().toString());
                            views++;
                            snippetItem.get(position).setViews(views + "");
                            DbHelper helper = new DbHelper(snippetHolder.heart.getContext());
                            helper.view(views, snippetItem.get(position).getSnippetID(), origin);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if(origin.equals("feed")) {
                        Snippet snippet = new Snippet(snippetHolder.heart.getContext());
                        snippet.view(snippetItem.get(position).getSnippetID());
                    }
                    snippetItem.get(position).setLikes(likes);
                    snippetItem.get(position).setIsLiked(isLiked);

                    Intent i=new Intent(snippetHolder.readMore.getContext(),SnippetView.class);
                    i.putExtra("snippetInfo",snippetItem.get(position));
                    i.putExtra("origin",origin);
                    snippetHolder.readMore.getContext().startActivity(i);
                }
            });
            /*
                setting on click listener to like
             */
            snippetHolder.heart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int like;
                    boolean checked=snippetHolder.heart.isChecked();

                    if(checked) {
                        like = 1;
                        try{
                            int likeCount=Integer.valueOf(snippetHolder.likes.getText().toString());
                            likeCount++;
                            snippetHolder.likes.setText(likeCount+"");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    else {
                        like = 0;
                        try{
                            int likeCount=Integer.valueOf(snippetHolder.likes.getText().toString());
                            likeCount--;
                            snippetHolder.likes.setText(likeCount+"");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    DbHelper dbHelper=new DbHelper(snippetHolder.heart.getContext());

                    if(origin.equals("feed"))
                        dbHelper.likeFeedSnippet(snippetItem.get(position).getSnippetID(),like);
                    else if(origin.equals("timeline"))
                        dbHelper.likeTimelineSnippet(snippetItem.get(position).getSnippetID(),like);

                    snippetItem.get(position).setIsLiked(like);

                    Snippet snippet=new Snippet(snippetHolder.heart.getContext());
                    snippet.like(snippetItem.get(position).getSnippetID(),like);
                }
            });

            /*
                setting on long click listener to linear layout if origin is timeline
             */
            if(origin.equals("timeline")&&snippetItem.get(position).getUserID()==selfUserID) {
                snippetHolder.llSnippet.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        final String[] list = new String[]{"Edit", "Delete", "Add to Diary"};
                        new LovelyChoiceDialog(snippetHolder.category.getContext())
                                .setTopColorRes(R.color.colorPrimary)
                                .setTitle("Choose an action")
                                .setIcon(R.mipmap.action)
                                .setItems(list, new LovelyChoiceDialog.OnItemSelectedListener<String>() {
                                    @Override
                                    public void onItemSelected(int which, String item) {
                                        switch (which) {
                                            case 0:
                                                Intent i = new Intent(snippetHolder.category.getContext(), NewSnippet.class);
                                                i.putExtra("tab", "timeline");
                                                i.putExtra("type", "edit");
                                                i.putExtra("snippetID", snippetItem.get(position).getSnippetID());
                                                i.putExtra("title", snippetItem.get(position).getTitle());
                                                i.putExtra("category", snippetItem.get(position).getCategory());
                                                i.putExtra("content", snippetItem.get(position).getContent());
                                                i.putExtra("date", snippetItem.get(position).getDate());
                                                i.putExtra("isAnonymous", snippetItem.get(position).getAnonymous());
                                                snippetHolder.category.getContext().startActivity(i);
                                                break;
                                            case 1:
                                                new LovelyStandardDialog(snippetHolder.category.getContext())
                                                        .setTopColorRes(R.color.colorPrimary)
                                                        .setButtonsColorRes(R.color.colorAccent)
                                                        .setIcon(R.mipmap.info)
                                                        .setTitle("Delete Snippet?")
                                                        .setMessage("This cannot be undone")
                                                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                Snippet snippet = new Snippet(snippetHolder.category.getContext());
                                                                snippet.deleteSnippet(snippetItem.get(position).getSnippetID());
                                                            }
                                                        })
                                                        .setNegativeButton("Cancel", null)
                                                        .show();
                                                break;
                                            case 2:
                                                new LovelyStandardDialog(snippetHolder.category.getContext())
                                                        .setTopColorRes(R.color.colorPrimary)
                                                        .setButtonsColorRes(R.color.colorAccent)
                                                        .setIcon(R.mipmap.info)
                                                        .setTitle("Add to Diary?")
                                                        .setMessage("Snippet will be displayed in timeline as well as diary.")
                                                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                DiarySnippet diarySnippet = new DiarySnippet(snippetHolder.llSnippet.getContext());
                                                                diarySnippet.addToDiary(snippetItem.get(position).getTitle(), snippetItem.get(position).getCategory(), snippetItem.get(position).getContent());
                                                            }
                                                        })
                                                        .setNegativeButton("Cancel", null)
                                                        .show();
                                                break;
                                        }
                                    }
                                })
                                .show();

                        return false;
                    }
                });

            }

             /*
                    setting on click listener to like count to start usersView activity
                 */
            snippetHolder.likes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(snippetHolder.category.getContext(), UsersView.class);
                    i.putExtra("origin","Likes");
                    i.putExtra("snippetID",snippetItem.get(position).getSnippetID());
                    snippetHolder.heart.getContext().startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return snippetItem.size();
    }


    private class SnippetHolder extends RecyclerView.ViewHolder {
        TextView personName, content, likes, readMore,views,category;
        ShineButton heart;
        AutofitTextView tvTitle;
        LinearLayout llSnippet;
        private SnippetHolder(View itemView) {
            super(itemView);
            personName = (TextView) itemView.findViewById(R.id.tv_person_name);
            content = (TextView) itemView.findViewById(R.id.tv_snippet_content);
            readMore = (TextView) itemView.findViewById(R.id.tv_readMore);
            likes = (TextView) itemView.findViewById(R.id.likes);
            heart = (ShineButton) itemView.findViewById(R.id.sb_snippet_heart);
            views=(TextView)itemView.findViewById(R.id.tvViews);
            category=(TextView)itemView.findViewById(R.id.tvCategory_Snippet);
            tvTitle=(AutofitTextView)itemView.findViewById(R.id.tvTitle);
            llSnippet=(LinearLayout)itemView.findViewById(R.id.llSnippet);
        }

    }

    public void updateAdapter(ArrayList<SnippetItem> snippetItems){
        snippetItem.clear();
        snippetItem=snippetItems;
        this.notifyDataSetChanged();
    }

}
