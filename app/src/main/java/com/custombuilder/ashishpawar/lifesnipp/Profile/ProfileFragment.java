package com.custombuilder.ashishpawar.lifesnipp.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.custombuilder.ashishpawar.lifesnipp.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/6/17.
 */

public class ProfileFragment extends Fragment {

    private ProfilePagerAdapter profilePagerAdapter;
    private ViewPager viewPager;
    private int userID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initialize adapter
         */
        profilePagerAdapter=new ProfilePagerAdapter(getChildFragmentManager());

        /*
            initialize viewpager
         */
        viewPager= (ViewPager) view.findViewById(R.id.container_profile);
        viewPager.setAdapter(profilePagerAdapter);

        /*
            getting userID
         */
        SharedPreferences preferences=getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        userID=preferences.getInt("userID",0);

        /*
            setting tab layout
         */
        TabLayout tabLayout= (TabLayout) view.findViewById(R.id.tabs_profile);
        tabLayout.setupWithViewPager(viewPager);
    }


    private class ProfilePagerAdapter extends FragmentStatePagerAdapter {
        public ProfilePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle args=new Bundle();
            switch (position){
                case 0:
                    /*
                        about fragment
                     */
                    AboutFragment aboutFragment=new AboutFragment();
                    args.putString("origin","ProfileFragment");
                    args.putInt("userID",userID);
                    aboutFragment.setArguments(args);
                    return aboutFragment;
                case 1:
                    /*
                        timeline fragment
                     */
                    TimelineFragment timelineFragment=new TimelineFragment();
                    args.putInt("userID",userID);
                    args.putString("origin","ProfileFragment");
                    timelineFragment.setArguments(args);
                    return timelineFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "About";
                case 1:
                    return "Timeline";
            }
            return null;
        }
    }
}
