package com.custombuilder.ashishpawar.lifesnipp.ViewUsers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FollowEvent;
import com.custombuilder.ashishpawar.lifesnipp.Profile.ProfileView;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.FetchProfileInfo;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import info.hoang8f.widget.FButton;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 13/7/17.
 */

public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private  ArrayList<UsersItem> usersItems;
    private Context context;

    public UsersAdapter(ArrayList<UsersItem> usersItems){
        this.usersItems = usersItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.users_view_single_item,parent,false);

        /*
            register event bus
         */
        try {
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }

        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final UserHolder userHolder =(UserHolder)holder;

        context= userHolder.bFollow.getContext();

        /*
            setting text in text views
         */
        userHolder.tvName.setText(usersItems.get(position).getPersonName());
        userHolder.tvUserName.setText("@"+usersItems.get(position).getUserName());

        /*
            setting follow/unfollow button
         */
        if(usersItems.get(position).getIsFollowing()==1) {
            userHolder.bFollow.setText(R.string.unfollow);
        }else {
            userHolder.bFollow.setText(R.string.follow);
        }

        /*
            starting ProfileView class if userID!= selfUserID
         */
        if(!(usersItems.get(position).getUserID()== context.getSharedPreferences("Profile",Context.MODE_PRIVATE).getInt("userID",0))) {

            userHolder.cvUsers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ProfileView.class);
                    i.putExtra("userID", usersItems.get(position).getUserID());
                    context.startActivity(i);
                }
            });
        }

        /*
            setting follow/unfollow button on click listener
         */
        userHolder.bFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FetchProfileInfo fetchProfileInfo=new FetchProfileInfo(context);
                fetchProfileInfo.followUnfollow(usersItems.get(position).getUserID(),usersItems.get(position).getIsFollowing()==1,"Users");
            }
        });

        /*
            hiding follow/unfollow button if userID==selfUserID
         */
        if(usersItems.get(position).getUserID()== context.getSharedPreferences("Profile",Context.MODE_PRIVATE).getInt("userID",0)){
            userHolder.bFollow.setVisibility(View.GONE);
        }else{
            userHolder.bFollow.setVisibility(View.VISIBLE);
        }

        /*
            getting profile pic
         */
        Drawable defaultProfPic=context.getResources().getDrawable(R.drawable.generic_profile);
        Glide.with(context).load("http://www.lifesnipp.com/profile_pic/"+usersItems.get(position).getUserID()+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(userHolder.ivProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                userHolder.ivProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    public void onEvent(FollowEvent followEvent){
        /*
            after follow
         */
        DbHelper helper=new DbHelper(context);
        usersItems=helper.getUsers();
        Log.d("follow","onEvent");
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return usersItems.size();
    }

    private class UserHolder extends RecyclerView.ViewHolder {
        TextView tvName,tvUserName;
        FButton bFollow;
        CircularImageView ivProfile;
        CardView cvUsers;
        public UserHolder(View itemView) {
            super(itemView);
            tvName=(TextView)itemView.findViewById(R.id.tvNameFollow);
            tvUserName=(TextView)itemView.findViewById(R.id.tvUserNameFollow);
            bFollow=(FButton)itemView.findViewById(R.id.bFollowView);
            ivProfile=(CircularImageView)itemView.findViewById(R.id.ivProfileFollow);
            cvUsers=(CardView)itemView.findViewById(R.id.cvUsersViewSingleItem);
        }
    }

    public void update(ArrayList<UsersItem> userItems){
        Log.d("usersadapter","update");
        //this.usersItems.clear();
        usersItems=userItems;
        this.notifyDataSetChanged();
    }
}
