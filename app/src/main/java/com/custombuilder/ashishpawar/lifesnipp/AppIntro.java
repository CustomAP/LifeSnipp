package com.custombuilder.ashishpawar.lifesnipp;

import android.os.Build;
import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/8/17.
 */

public class AppIntro extends IntroActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setButtonBackVisible(false);
        setButtonNextVisible(true);
        setButtonCtaVisible(false);
        setButtonCtaTintMode(BUTTON_CTA_TINT_MODE_BACKGROUND);

        setPageScrollDuration(500);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setPageScrollInterpolator(android.R.interpolator.fast_out_slow_in);
        }

        addSlide(new SimpleSlide.Builder()
                .title(R.string.app_name)
                .description(R.string.intro_1)
                .image(R.drawable.intro_snippet)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .layout(R.layout.slide_intro)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.interact)
                .description(R.string.intro_2)
                .image(R.drawable.intro_profile)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .layout(R.layout.slide_intro)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.personal_diary)
                .description(R.string.intro_3)
                .image(R.drawable.intro_diary)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .layout(R.layout.slide_intro)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.anonymous)
                .description(R.string.intro_4)
                .image(R.drawable.intro_anonymous)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .layout(R.layout.slide_intro)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.get_started)
                .description(R.string.intro_5)
                .image(R.drawable.intro_feed)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .layout(R.layout.slide_intro)
                .build());

        //autoplay(2500, INFINITE);
    }

}
