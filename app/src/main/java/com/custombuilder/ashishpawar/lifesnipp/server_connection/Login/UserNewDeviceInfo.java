package com.custombuilder.ashishpawar.lifesnipp.server_connection.Login;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/6/17.
 */

public class UserNewDeviceInfo {
    private Context context;
    private String userName,password;
    private AQuery aQuery;

    private String replaceUserDeviceInfo="http://www.lifesnipp.com/index.php/User_login/replaceUserDeviceInfo";

    public UserNewDeviceInfo(Context context,String userName,String password){
        this.context=context;
        this.password=password;
        this.userName=userName;
        aQuery=new AQuery(context);
    }

    public void InsertInfo(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userName" , userName);
        params.put("password",password);
        params.put("board", Build.BOARD );
        params.put("bootloader", Build.BOOTLOADER);
        params.put("brand", Build.BRAND);
        params.put("device", Build.DEVICE);
        params.put("display", Build.DISPLAY);
        params.put("hardware", Build.HARDWARE);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("model", Build.MODEL);
        params.put("product", Build.PRODUCT);


        //replace data in server db.
        aQuery.ajax(replaceUserDeviceInfo,params, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try {
                        code=object.getInt("ResultSet");
                        Log.d("ReplaceUserDeviceInfo",code+"");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("ReplaceUserDeviceInfo","object=null");
                    Toast.makeText(context,"Failed to login",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    if(code==0){
                        Log.d("ReplaceUserDeviceInfo","code=0");

                        //assign sessionID
                        SessionID sessionID=new SessionID(context);
                        sessionID.assignSessionID(userName,password);

                        timer.start();

                    }else if(code==1){
                        Log.d("ReplaceUserDeviceInfo","code=1");
                        Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                    }else if(code==2){
                        Log.d("ReplaceUserDeviceInfo","code=2");
                        Toast.makeText(context,"Username and password don't match",Toast.LENGTH_SHORT).show();
                    }else if(code==3){
                        Log.d("ReplaceUserDeviceInfo","code=3");
                        Toast.makeText(context,"Username does not exists.\nSign in if you are new to Life Snippet",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    Thread timer =new Thread(){
      public void run(){
          try {
              sleep(1500);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }finally {
              /*
                   fetch profile info
               */
              FetchProfileInfo fetchProfileInfo = new FetchProfileInfo(context);
              fetchProfileInfo.getSelfProfileInfo(userName,password,"Login");
          }
      }
    };
}
