package com.custombuilder.ashishpawar.lifesnipp.Notifications;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/6/17.
 */

public class NotificationsItem {
    public String notificationContent,date,time,notifType;
    public int notificationID, snippet_user_ID;

    public String getNotificationContent() {
        return notificationContent;
    }


    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public int getNotificationID() {
        return notificationID;
    }

    public int getSnippet_user_ID() {
        return snippet_user_ID;
    }

    public String getNotifType() {
        return notifType;
    }

    public NotificationsItem(String notificationContent,String notifType, String date, String time, int notificationID, int snippet_user_ID) {

        this.notificationContent = notificationContent;
        this.date = date;
        this.time = time;
        this.notificationID = notificationID;
        this.snippet_user_ID = snippet_user_ID;
        this.notifType=notifType;
    }
}
