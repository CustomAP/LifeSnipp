package com.custombuilder.ashishpawar.lifesnipp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 14/9/17.
 */

public class Multi_Dex extends Application {
    /*
        creates support for 4.4 by solving class not found exception
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
