package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 22/6/17.
 */

public class ShareEvent {
    private boolean sharedSnippet,addedToDiary;

    public boolean isSharedSnippet() {
        return sharedSnippet;
    }

    public boolean isAddedToDiary() {
        return addedToDiary;
    }

    public ShareEvent(boolean sharedSnippet, boolean addedToDiary) {
        this.sharedSnippet = sharedSnippet;
        this.addedToDiary=addedToDiary;
    }
}
