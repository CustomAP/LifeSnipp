package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/8/17.
 */

public class FetchFailed {
    private boolean isRefreshFailed;

    public boolean isRefreshFailed() {
        return isRefreshFailed;
    }

    public FetchFailed(boolean isRefreshFailed) {

        this.isRefreshFailed = isRefreshFailed;
    }
}
