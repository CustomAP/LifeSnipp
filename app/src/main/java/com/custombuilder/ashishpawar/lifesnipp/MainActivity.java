package com.custombuilder.ashishpawar.lifesnipp;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.custombuilder.ashishpawar.lifesnipp.EventBus.NotificationCountEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.NotificationEvent;
import com.custombuilder.ashishpawar.lifesnipp.Home.HomeFragment;
import com.custombuilder.ashishpawar.lifesnipp.Notifications.NotificationsFragment;
import com.custombuilder.ashishpawar.lifesnipp.Notifications.NotificationsItem;
import com.custombuilder.ashishpawar.lifesnipp.Profile.ProfileFragment;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Extras;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.FetchNotifications;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private NonScrollableViewPagerAdapter mViewPager;
    private TabLayout tabLayout;

    private UIData uiData = new UIData();
    private TextView appTitleTextview;
    private boolean exit = false;

    final int INTERVAL = 1000 * 30;
    Handler mHandler = new Handler();

    LinearLayout llSearch;
    SearchFragment searchFragment;

    ArrayList<NotificationsItem> notificationsItems = new ArrayList<>();

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
            updating number of times user opened the app
         */
        updateRunCount.start();

        /*
            getting notifications
         */
        getNotifications.start();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        /*
            setting adapter and viewpager
         */
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (NonScrollableViewPagerAdapter) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        llSearch = (LinearLayout) findViewById(R.id.llMainTitle);

        /*
            setting tablayout
         */
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        /*
            setting font
         */
        appTitleTextview = (TextView) findViewById(R.id.textview_main_title);
        Typeface font = Typeface.createFromAsset(getAssets(), "ForeverBrushScriptDemo.otf");
        appTitleTextview.setTypeface(font);

        searchFragment = new SearchFragment();
//        TitleFragment titleFragment=new TitleFragment();
//        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.llMainTitle,titleFragment);
//        transaction.commit();

        /*
            Setting tab icons
         */
        setTabIcons();

        /*
            tab change listener to change icons
         */
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.setIcon(uiData.getIconSelected(tab.getPosition()));
                appTitleTextview.setText(uiData.getMainActivityTabName(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.setIcon(uiData.getIconNormal(tab.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /*
            Setting Home as first tab
         */
        TabLayout.Tab homeTab = tabLayout.getTabAt(1);
        homeTab.select();

        /*
            getting extras
         */
        getExtras.start();

        /*
            check updates
         */
        update.start();
    }

    Thread update = new Thread() {

        public void run() {

            SharedPreferences preferences = getSharedPreferences("AppInfo", Context.MODE_PRIVATE);
            int toUpdate = preferences.getInt("toUpdate", 0);
            int isMandatory = preferences.getInt("isMandatory", 0);

            if (toUpdate == 1) {
                if (isMandatory == 1) {
                    /*
                        mandatory update
                     */
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new LovelyStandardDialog(MainActivity.this, R.style.myDialog)
                                    .setTopColorRes(R.color.colorPrimary)
                                    .setButtonsColorRes(R.color.colorAccent)
                                    .setIcon(R.mipmap.info)
                                    .setTitle("Update")
                                    .setMessage("This version has expired.Kindly update to the latest version.")
                                    .setPositiveButton("Update", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            openMarket();
                                        }
                                    })
                                    .setCancelable(false)
                                    .show();
                        }
                    });
                } else if (isMandatory == 0) {
                    /*
                        not mandatory update
                     */
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new LovelyStandardDialog(MainActivity.this, R.style.myDialog)
                                    .setTopColorRes(R.color.colorPrimary)
                                    .setButtonsColorRes(R.color.colorAccent)
                                    .setIcon(R.mipmap.info)
                                    .setTitle("Update")
                                    .setMessage("New version available.Update for the best experience.")
                                    .setPositiveButton("Update", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            openMarket();
                                        }
                                    })
                                    .setNegativeButton("Later", null)
                                    .show();
                        }
                    });
                }
            } else if (toUpdate == 0) {
                /*
                    rate
                 */
                final SharedPreferences preferences1 = getSharedPreferences("AppInfo", Context.MODE_PRIVATE);
                boolean isRated = preferences1.getBoolean("isRated", false);
                boolean isNeverClicked = preferences1.getBoolean("isNeverClicked", false);
                if (!isRated && !isNeverClicked) {
                    if (preferences1.getInt("run", 0) > 5) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new LovelyStandardDialog(MainActivity.this, R.style.myDialog)
                                        .setTopColorRes(R.color.colorPrimary)
                                        .setButtonsColorRes(R.color.colorAccent)
                                        .setIcon(R.mipmap.rate)
                                        .setTitle("Loved this app?")
                                        .setMessage("Rate it a 5 star to support us.")
                                        .setPositiveButton("Rate", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                openMarket();
                                                SharedPreferences preferences = getApplicationContext().getSharedPreferences("AppInfo", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = preferences.edit();
                                                editor.putBoolean("isRated", true);
                                                editor.apply();
                                            }
                                        })
                                        .setNegativeButton("Never", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                SharedPreferences.Editor editor = preferences1.edit();
                                                editor.putBoolean("isNeverClicked", true);
                                                editor.apply();
                                            }
                                        })
                                        .show();
                            }
                        });
                    }
                }
            }
        }
    };

    Thread getExtras = new Thread() {
        public void run() {
            Extras extras = new Extras(getApplicationContext());
            extras.checkUpdates();
            extras.getCategories();
        }
    };

    Thread updateRunCount = new Thread() {
        public void run() {
            SharedPreferences preferences = getSharedPreferences("AppInfo", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            int run = preferences.getInt("run", 0);
            run++;
            editor.putInt("run", run);
            editor.apply();
        }
    };

    /*
        sets tab icons
     */
    private void setTabIcons() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            /*
                Tab icons are stored in UIData class
             */
            tabLayout.getTabAt(i).setIcon(uiData.getIconNormal(i));
        }
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {
        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new HomeFragment();
                case 0:
                    return new NotificationsFragment();
                case 2:
                    return new ProfileFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return uiData.mainActivityTabsCount();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        /*
            hiding the search box
         */

        if (appTitleTextview.getVisibility() == View.GONE) {
            hideSearchBox();
        } else {

            if (exit) {
            /*
                finish activity
             */
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to exit", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2300);
            }
        }
    }

    @Override
    protected void onDestroy() {
        /*
            delete timeline snippets of other users
         */
        super.onDestroy();
        DbHelper dbHelper = new DbHelper(getApplicationContext());
        dbHelper.deleteTimelineSnippets();
        mHandler.removeCallbacks(mHandlerTask);
    }

    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            FetchNotifications fetchNotifications = new FetchNotifications(getApplicationContext());
            fetchNotifications.getNotifications();

            DbHelper helper = new DbHelper(getApplicationContext());
            notificationsItems = helper.getNotifications();

            EventBus.getDefault().post(new NotificationEvent(notificationsItems));

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };


    Thread getNotifications = new Thread() {
        public void run() {

            while (true) {

                /*
                    getting notifications
                 */
                FetchNotifications fetchNotifications = new FetchNotifications(getApplicationContext());
                fetchNotifications.getNotifications();

                DbHelper helper = new DbHelper(getApplicationContext());
                notificationsItems = helper.getNotifications();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new NotificationEvent(notificationsItems));
                    }
                });

                try {
                    sleep(INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    };

    public void onEvent(NotificationCountEvent notificationCountEvent) {
        int count = notificationCountEvent.getCount();
        if (count > 0) {
            tabLayout.getTabAt(0).setIcon(R.mipmap.notifications_unseen);

            /*
                Vibrate for 500 milliseconds
              */
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                /*
                    replacing title with search box
                 */
                appTitleTextview.setVisibility(View.GONE);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.llMainTitle, searchFragment);
                transaction.commit();
                break;
        }

        return true;
    }

    private void hideSearchBox() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(searchFragment);
        transaction.commit();

        appTitleTextview.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (appTitleTextview.getVisibility() == View.GONE)
            hideSearchBox();
    }

    private void openMarket(){
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }
}
