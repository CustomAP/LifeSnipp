package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baoyz.widget.PullRefreshLayout;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.DiaryUpdateEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ShareEvent;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.FetchDiaryContent;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar on 3/28/2017.
 */

public class DiaryFragment extends Fragment implements View.OnClickListener {
    private FloatingActionButton plus;
    RecyclerView rvDiary;
    DiarySnippetAdapter diarySnippetAdapter;
    ArrayList<DiarySnippetItem> diarySnippetItems;
    private CardView cvEmptyDiary;
    private DbHelper dbHelper;
    PullRefreshLayout prlDiary;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_diary, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing views
         */
        plus = (FloatingActionButton) view.findViewById(R.id.fabPlusDiary);
        cvEmptyDiary=(CardView)view.findViewById(R.id.cvEmptyDiary);
        rvDiary = (RecyclerView) view.findViewById(R.id.rvDiary);
        prlDiary=(PullRefreshLayout)view.findViewById(R.id.prlDiary);
        prlDiary.setColor(R.color.colorPrimaryDark);
        plus.setOnClickListener(this);

        /*
            initializing variables
         */
        diarySnippetItems = new ArrayList<>();
        dbHelper=new DbHelper(getContext());

        /*
            registering event bus
         */
        try {
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }

        /*
            setting recycler view
         */
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());

        rvDiary.setItemAnimator(new DefaultItemAnimator());
        rvDiary.setLayoutManager(manager);

        /*
            filling recycler view items
         */
        fillList();

        /*
            adding on scroll listener
         */
        rvDiary.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    plus.show();
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && plus.isShown())
                    plus.hide();
            }
        });

        /*
            setting on refresh listener
         */
        prlDiary.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FetchDiaryContent fetchDiaryContent=new FetchDiaryContent(getContext());
                fetchDiaryContent.getDiaryContent("Diary");
            }
        });
    }

    private void fillList() {
        diarySnippetItems.clear();
        diarySnippetItems=dbHelper.getDiarySnippets();
        diarySnippetAdapter = new DiarySnippetAdapter(diarySnippetItems);
        rvDiary.setAdapter(diarySnippetAdapter);
        /*
            setting empty diary message visibility
         */
        if(!diarySnippetItems.isEmpty())
            cvEmptyDiary.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabPlusDiary:
                /*
                    starting new snippet activity on fab clicked
                 */
                Intent i = new Intent(getActivity(), NewSnippet.class);
                i.putExtra("tab", "diary");
                i.putExtra("type","new");
                startActivity(i);
                break;
        }
    }

    public void onEvent(ShareEvent shareEvent){
        /*
            updating recycler view after snippet added to diary
         */
        boolean isAddedToDiary= shareEvent.isAddedToDiary();
        if(isAddedToDiary){
            fillList();
        }
    }

    public void onEvent(DiaryUpdateEvent diaryUpdateEvent){
        Log.d("onEvent","diaryUpdate");
        /*
            updating recycler view after refresh
         */
        boolean isDiaryUpdated=diaryUpdateEvent.isDiaryUpdated();
        if(isDiaryUpdated){
            fillList();
            try{
                prlDiary.setRefreshing(false);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void onEvent(FetchFailed fetchFailed){
        if(fetchFailed.isRefreshFailed()){
            prlDiary.setRefreshing(false);
        }
    }
}
