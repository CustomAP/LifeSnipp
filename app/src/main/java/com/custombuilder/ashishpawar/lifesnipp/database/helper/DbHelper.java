package com.custombuilder.ashishpawar.lifesnipp.database.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.custombuilder.ashishpawar.lifesnipp.Home.CommentItem;
import com.custombuilder.ashishpawar.lifesnipp.Home.DiarySnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.Notifications.NotificationsItem;
import com.custombuilder.ashishpawar.lifesnipp.ViewUsers.UsersItem;

import java.util.ArrayList;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 9/6/17.
 */

public class DbHelper extends SQLiteOpenHelper {

    private Context context;

    private static final String DBName ="User";


    private static final String snippetTable="Snippet";
    private static final String diaryTable="Diary";
    private static final String notificationsTable="Notifications";
    private static final String timelineTable="Timeline";
    private static final String commentsTable="Comments";
    private static final String usersTable="Users";

    private static final String column_snippetID ="SnippetID";
    private static final String column_date="date";
    private static final String column_time="time";
    private static final String column_title="title";
    private static final String column_snippetContent="content";
    private static final String column_likesCount="likesCount";
    private static final String column_commentsCount="commentsCount";
    private static final String column_category="category";
    private static final String column_userID="userID";
    private static final String column_personName ="personName";
    private static final String column_userInfo="userInfo";
    private static final String column_diarySnippetID="DiarySnippetID";
    private static final String column_notificationID="NotificationID";
    private static final String column_notificationContent="content";
    private static final String column_viewsCount="viewsCount";
    private static final String column_isLiked="isLiked";
    private static final String column_commentID="commentID";
    private static final String column_comment="comment";
    private static final String column_anonymous="anonymous";
    private static final String column_isFollowing="isFollowing";
    private static final String column_userName="userName";
    private static final String column_notifType="notifType";
    private static final String column_snippet_user_ID="snippet_user_ID";

    private static final int DBVersion=1;

    public DbHelper(Context context){
        super(context,DBName,null,DBVersion);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSnippetTable="CREATE TABLE "+snippetTable+ " ("
                + column_snippetID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                + column_date + " DATE, "
                + column_time + " TIME, "
                + column_title+ " VARCHAR(255), "
                + column_snippetContent+ " TEXT, "
                + column_category+" VARCHAR(255), "
                + column_userID+" INT, "
                + column_personName +" VARCHAR(255), "
                + column_userInfo+" VARCHAR(255), "
                + column_likesCount+" VARCHAR(255), "
                + column_commentsCount+" VARCHAR(255), "
                + column_viewsCount+" VARCHAR(255), "
                +column_anonymous+" INT, "
                + column_isLiked+" INT);";


        String createDiaryTable="CREATE TABLE "+diaryTable+" ("
                + column_diarySnippetID+" INTEGER PRIMARY KEY, "
                + column_date+" DATE, "
                + column_time+" TIME, "
                + column_snippetContent+" TEXT, "
                + column_title+" VARCHAR(255), "
                + column_category+" VARCHAR(255));";

        String createNotificationsTable="CREATE TABLE "+notificationsTable+" ("
                + column_notificationID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                + column_date+" DATE, "
                + column_time+" TIME, "
                + column_notificationContent+" TEXT,"
                + column_notifType+" VARCHAR(255), "
                + column_snippet_user_ID+" INT, "
                + "FOREIGN KEY ("+column_snippet_user_ID+") REFERENCES "
                + snippetTable +" ("+column_snippetID+") "
                + "ON DELETE CASCADE);";

        String createTimelineTable="CREATE TABLE "+timelineTable+ " ("
                + column_snippetID +" INTEGER PRIMARY KEY, "
                + column_date + " DATE, "
                + column_time + " TIME, "
                + column_title+ " VARCHAR(255), "
                + column_snippetContent+ " TEXT, "
                + column_category+" VARCHAR(255), "
                + column_userID+" INT, "
                + column_personName +" VARCHAR(255), "
                + column_userInfo+" VARCHAR(255), "
                + column_likesCount+" VARCHAR(255), "
                + column_commentsCount+" VARCHAR(255), "
                + column_viewsCount+" VARCHAR(255), "
                + column_anonymous+" INT, "
                + column_isLiked+" INT);";

        String createCommentsTable="CREATE TABLE "+commentsTable+" ("
                + column_commentID+" INTEGER PRIMARY KEY, "
                + column_date+" DATE, "
                + column_personName+" VARCHAR(255), "
                + column_comment+" TEXT, "
                + column_userID+" INT, "
                + column_snippetID+" INT);";

        String createUsersTable="CREATE TABLE "+usersTable+" ("
                +column_userID+" INTEGER PRIMARY KEY, "
                +column_personName+" VARCHAR(255), "
                +column_userName+" VARCHAR(255),"
                +column_isFollowing+" INT);";

        try{
            db.execSQL(createSnippetTable);
            db.execSQL(createDiaryTable);
            db.execSQL(createNotificationsTable);
            db.execSQL(createTimelineTable);
            db.execSQL(createCommentsTable);
            db.execSQL(createUsersTable);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropSnippetTable="DROP TABLE IF EXISTS '"+snippetTable+"'";
        String dropDiaryTable="DROP TABLE IF EXISTS '"+diaryTable+"'";
        String dropNotificationsTable="DROP TABLE IF EXISTS '"+notificationsTable+"'";
        String dropTimelineTable="DROP TABLE IF EXISTS '"+timelineTable+"'";

        try{
            db.execSQL(dropSnippetTable);
            db.execSQL(dropDiaryTable);
            db.execSQL(dropNotificationsTable);
            db.execSQL(dropTimelineTable);
            onCreate(db);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addSnippet(SnippetItem snippetItem){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(column_snippetID,snippetItem.getSnippetID());
        values.put(column_date,snippetItem.getDate());
        values.put(column_time,snippetItem.getTime());
        values.put(column_title,snippetItem.getTitle());
        values.put(column_snippetContent,snippetItem.getContent());
        values.put(column_category,snippetItem.getCategory());
        values.put(column_userID,snippetItem.getUserID());
        values.put(column_personName,snippetItem.getPersonName());
        values.put(column_userInfo,snippetItem.getUserInfo());
        values.put(column_likesCount,snippetItem.getLikes());
        values.put(column_commentsCount,snippetItem.getComments());
        values.put(column_viewsCount,snippetItem.getViews());
        values.put(column_isLiked,snippetItem.isLiked());
        values.put(column_anonymous,snippetItem.getAnonymous());

        try {
            database.insert(snippetTable, null, values);
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
    }

    public ArrayList<SnippetItem> getSnippets(){
        ArrayList<SnippetItem> snippetItems =new ArrayList<>();
        String selectQuery="SELECT * FROM "+snippetTable+" ORDER BY "+column_snippetID+" DESC";
        try{
            SQLiteDatabase database=this.getReadableDatabase();
            Cursor cursor=database.rawQuery(selectQuery,null);
            if(cursor.moveToFirst()){
                do{
                    snippetItems.add(new SnippetItem(cursor.getInt(cursor.getColumnIndex(column_snippetID)),
                            cursor.getInt(cursor.getColumnIndex(column_userID)),
                            cursor.getString(cursor.getColumnIndex(column_snippetContent)),
                            cursor.getString(cursor.getColumnIndex(column_viewsCount)),
                            cursor.getString(cursor.getColumnIndex(column_personName)),
                            cursor.getString(cursor.getColumnIndex(column_likesCount)),
                            cursor.getString(cursor.getColumnIndex(column_commentsCount)),
                            cursor.getString(cursor.getColumnIndex(column_date)),
                            cursor.getString(cursor.getColumnIndex(column_time)),
                            cursor.getString(cursor.getColumnIndex(column_title)),
                            cursor.getString(cursor.getColumnIndex(column_category)),
                            cursor.getString(cursor.getColumnIndex(column_userInfo)),
                            cursor.getInt(cursor.getColumnIndex(column_isLiked)),
                            cursor.getInt(cursor.getColumnIndex(column_anonymous))));
                }while (cursor.moveToNext());
            }
            cursor.close();
            database.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return snippetItems;
    }

    public void addTimelineSnippet(SnippetItem snippetItem){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(column_snippetID,snippetItem.getSnippetID());
        values.put(column_date,snippetItem.getDate());
        values.put(column_time,snippetItem.getTime());
        values.put(column_title,snippetItem.getTitle());
        values.put(column_snippetContent,snippetItem.getContent());
        values.put(column_category,snippetItem.getCategory());
        values.put(column_userID,snippetItem.getUserID());
        values.put(column_personName,snippetItem.getPersonName());
        values.put(column_userInfo,snippetItem.getUserInfo());
        values.put(column_likesCount,snippetItem.getLikes());
        values.put(column_commentsCount,snippetItem.getComments());
        values.put(column_viewsCount,snippetItem.getViews());
        values.put(column_isLiked,snippetItem.isLiked());
        values.put(column_anonymous,snippetItem.getAnonymous());

        try {
            database.insert(timelineTable, null, values);
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
    }

    public ArrayList<SnippetItem> getTimelineSnippets(int userID){
        ArrayList<SnippetItem> snippetItems =new ArrayList<>();
        String selectQuery="SELECT * FROM "+timelineTable+" WHERE "+column_userID+" = "+userID;
        try{
            SQLiteDatabase database=this.getReadableDatabase();
            Cursor cursor=database.rawQuery(selectQuery,null);
            if(cursor.moveToFirst()){
                do{
                    snippetItems.add(new SnippetItem(cursor.getInt(cursor.getColumnIndex(column_snippetID)),
                            cursor.getInt(cursor.getColumnIndex(column_userID)),
                            cursor.getString(cursor.getColumnIndex(column_snippetContent)),
                            cursor.getString(cursor.getColumnIndex(column_viewsCount)),
                            cursor.getString(cursor.getColumnIndex(column_personName)),
                            cursor.getString(cursor.getColumnIndex(column_likesCount)),
                            cursor.getString(cursor.getColumnIndex(column_commentsCount)),
                            cursor.getString(cursor.getColumnIndex(column_date)),
                            cursor.getString(cursor.getColumnIndex(column_time)),
                            cursor.getString(cursor.getColumnIndex(column_title)),
                            cursor.getString(cursor.getColumnIndex(column_category)),
                            cursor.getString(cursor.getColumnIndex(column_userInfo)),
                            cursor.getInt(cursor.getColumnIndex(column_isLiked)),
                            cursor.getInt(cursor.getColumnIndex(column_anonymous))));
                }while (cursor.moveToNext());
            }
            cursor.close();
            database.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return snippetItems;
    }

    public void addDiarySnippets(DiarySnippetItem diarySnippetItem){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(column_diarySnippetID,diarySnippetItem.getDiarySnippetID());
        contentValues.put(column_date,diarySnippetItem.getDate());
        contentValues.put(column_time,diarySnippetItem.getTime());
        contentValues.put(column_snippetContent,diarySnippetItem.getContent());
        contentValues.put(column_title,diarySnippetItem.getTitle());
        contentValues.put(column_category,diarySnippetItem.getCategory());

        try {
            database.insert(diaryTable, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
    }

    public ArrayList<DiarySnippetItem> getDiarySnippets(){
        ArrayList<DiarySnippetItem> diarySnippetItems=new ArrayList<>();
        String selectQuery="SELECT * FROM "+diaryTable;
        try{
            SQLiteDatabase database=this.getReadableDatabase();
            Cursor cursor=database.rawQuery(selectQuery,null);
            if(cursor.moveToFirst()){
                do{
                    diarySnippetItems.add(new DiarySnippetItem(cursor.getString(cursor.getColumnIndex(column_snippetContent)),
                            cursor.getString(cursor.getColumnIndex(column_title)),
                            cursor.getString(cursor.getColumnIndex(column_category)),
                            cursor.getString(cursor.getColumnIndex(column_date)),
                            cursor.getString(cursor.getColumnIndex(column_time)),
                            cursor.getInt(cursor.getColumnIndex(column_diarySnippetID))));
                }while (cursor.moveToNext());
            }
            cursor.close();
            database.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return diarySnippetItems;
    }

    public void addNotification(NotificationsItem notificationsItem){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(column_date,notificationsItem.getDate());
        contentValues.put(column_notifType,notificationsItem.getNotifType());
        contentValues.put(column_time,notificationsItem.getTime());
        contentValues.put(column_notificationContent,notificationsItem.getNotificationContent());
        contentValues.put(column_snippet_user_ID,notificationsItem.getSnippet_user_ID());

        database.insert(notificationsTable,null,contentValues);
        database.close();
    }

    public ArrayList<NotificationsItem> getNotifications(){
        ArrayList<NotificationsItem> notificationsItems=new ArrayList<>();
        String selectQuery="SELECT * FROM "+notificationsTable+" ORDER BY "+column_notificationID+" DESC";
        try{
            SQLiteDatabase database=this.getReadableDatabase();
            Cursor cursor=database.rawQuery(selectQuery,null);
            if(cursor.moveToFirst()){
                do{
                    notificationsItems.add(new NotificationsItem(cursor.getString(cursor.getColumnIndex(column_notificationContent)),
                            cursor.getString(cursor.getColumnIndex(column_notifType)),
                            cursor.getString(cursor.getColumnIndex(column_date)),
                            cursor.getString(cursor.getColumnIndex(column_time)),
                            cursor.getInt(cursor.getColumnIndex(column_notificationID)),
                            cursor.getInt(cursor.getColumnIndex(column_snippet_user_ID))));
                }while (cursor.moveToNext());
            }
            cursor.close();
            database.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return notificationsItems;
    }

    public void likeFeedSnippet(int snippetID, int like){
        String updateQuery="UPDATE "+snippetTable+" SET "+column_isLiked+" = "+like+" WHERE "+column_snippetID+" = "+snippetID;
        SQLiteDatabase database=this.getWritableDatabase();

        try {
            String selectQuery = "SELECT " + column_likesCount + " FROM " + snippetTable + " WHERE " + column_snippetID + " = " + snippetID;
            Cursor cursor = database.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()) {
                int likesCount = Integer.valueOf(cursor.getString(cursor.getColumnIndex(column_likesCount)));

                if (like == 1)
                    likesCount++;
                else
                    likesCount--;
                String updateQuery1 = "UPDATE " + snippetTable + " SET " + column_likesCount + " = " + likesCount + " WHERE " + column_snippetID + " = " + snippetID;
                database.execSQL(updateQuery1);
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            database.execSQL(updateQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public void likeTimelineSnippet(int snippetID,int like){
        SQLiteDatabase database=this.getWritableDatabase();

        String updateQuery="UPDATE "+timelineTable+" SET "+column_isLiked+" = "+like+" WHERE "+column_snippetID+" = "+snippetID;

        try {
            String selectQuery = "SELECT " + column_likesCount + " FROM " + timelineTable + " WHERE " + column_snippetID + " = " + snippetID;
            Cursor cursor = database.rawQuery(selectQuery, null);

            if(cursor.moveToFirst()) {
                int likesCount = Integer.valueOf(cursor.getString(cursor.getColumnIndex(column_likesCount)));

                if (like == 1)
                    likesCount++;
                else
                    likesCount--;

                String updateQuery1 = "UPDATE " + timelineTable + " SET " + column_likesCount + " = " + likesCount + " WHERE " + column_snippetID + " = " + snippetID;
                database.execSQL(updateQuery1);
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }


        try {
            database.execSQL(updateQuery);
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
    }

    public void deleteTimelineSnippets(){

        SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
        int userID=preferences.getInt("userID",0);

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

        String deleteQuery="DELETE FROM "+timelineTable+" WHERE NOT "+column_userID+" = "+userID;

        try{
            sqLiteDatabase.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public void view(int views,int snippetID,String origin){
        SQLiteDatabase database=this.getWritableDatabase();

        String updateQuery=null;

        if(origin.equals("timeline"))
            updateQuery="UPDATE "+timelineTable+" SET "+column_viewsCount+" = " +views+" WHERE "+column_snippetID+" = "+snippetID;
        else if(origin.equals("feed"))
            updateQuery="UPDATE "+snippetTable+" SET "+column_viewsCount+" = " +views+" WHERE "+column_snippetID+" = "+snippetID;

        try{
            database.execSQL(updateQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public void addComments(CommentItem commentItem){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(column_commentID,commentItem.getCommentID());
        contentValues.put(column_personName,commentItem.getPersonName());
        contentValues.put(column_comment,commentItem.getComment());
        contentValues.put(column_date,commentItem.getDate());
        contentValues.put(column_userID,commentItem.getUserID());
        contentValues.put(column_snippetID,commentItem.getSnippetID());


        try {
            database.insert(commentsTable, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public ArrayList<CommentItem> getComments(int snippetID){
        SQLiteDatabase database=this.getReadableDatabase();
        String selectQuery="SELECT * FROM "+commentsTable+" WHERE "+column_snippetID+" = "+snippetID+" ORDER BY "+column_commentID+" DESC";

        ArrayList<CommentItem> commentItems=new ArrayList<>();

        try{
            Cursor cursor=database.rawQuery(selectQuery,null);
            if(cursor.moveToFirst()){
                do{
                    commentItems.add(new CommentItem(cursor.getInt(cursor.getColumnIndex(column_commentID)),
                            cursor.getString(cursor.getColumnIndex(column_personName)),
                            cursor.getString(cursor.getColumnIndex(column_comment)),
                            cursor.getString(cursor.getColumnIndex(column_date)),
                            cursor.getInt(cursor.getColumnIndex(column_userID)),
                            cursor.getInt(cursor.getColumnIndex(column_snippetID))));
                }while (cursor.moveToNext());
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
        return commentItems;
    }

    public void clearComments(){
        String deleteQuery="DELETE FROM "+commentsTable;

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        try{
            sqLiteDatabase.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public void editSnippet(int snippetID,String title,String category,String content){
        String updateQuery="UPDATE "+timelineTable+" SET "+column_title+" = ?, "
                +column_category+" = '"+category+"' , "
                + column_snippetContent+" = ? WHERE "+column_snippetID+" = "+snippetID;

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        try{
            sqLiteDatabase.execSQL(updateQuery,new String[]{title,content});
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public void editDiarySnippet(int snippetID,String title,String category,String content){
        String updateQuery="UPDATE "+diaryTable+" SET "+column_title+" = ?, "
                +column_category+" = '"+category+"' , "
                +column_snippetContent+" = ? WHERE "+column_diarySnippetID+" = "+snippetID;

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        try{
            sqLiteDatabase.execSQL(updateQuery,new String[]{title,content});
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public void deleteSnippet(int snippetID){
        String deleteQuery="DELETE FROM "+timelineTable+" WHERE "+column_snippetID+" = "+snippetID;

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

        try{
            sqLiteDatabase.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public void deleteDiarySnippet(int snippetID){
        String deleteQuery="DELETE FROM "+diaryTable+" WHERE "+column_diarySnippetID+" = "+snippetID;

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

        try{
            sqLiteDatabase.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public void updateDiary(DiarySnippetItem diarySnippetItem){
        SQLiteDatabase database=this.getWritableDatabase();

        String selectQuery="SELECT * FROM "+diaryTable+" WHERE "+column_diarySnippetID+" = "+diarySnippetItem.getDiarySnippetID();

        Cursor cursor=database.rawQuery(selectQuery,null);

        if(cursor.getCount()>0) {
            Log.d("updateDiary","count>0");
            String updateQuery = "UPDATE " + diaryTable + " SET " + column_title + " = ?, "
                    + column_category + " = '" + diarySnippetItem.getCategory() + "' , "
                    + column_snippetContent + " = ?, "
                    + column_date + " = '" + diarySnippetItem.getDate() + "' , "
                    + column_time + " = '" + diarySnippetItem.getTime() + "' WHERE " + column_diarySnippetID + " = " + diarySnippetItem.getDiarySnippetID();

            try {
                database.execSQL(updateQuery,new String[]{diarySnippetItem.getTitle(),diarySnippetItem.getContent()});
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            Log.d("updateDiary","count=0");
            this.addDiarySnippets(diarySnippetItem);
        }

        database.close();
        cursor.close();
    }

    public void updateTimeline(SnippetItem snippetItem){
        SQLiteDatabase database=this.getWritableDatabase();

        String selectQuery="SELECT * FROM "+timelineTable+" WHERE "+column_snippetID+" = "+snippetItem.getSnippetID();

        Cursor cursor=database.rawQuery(selectQuery,null);

        if(cursor.getCount()>0){
            Log.d("updateTimeline","count>0");
            String updateQuery="UPDATE "+timelineTable+" SET "+column_title+" = ?, "
                    +column_category+" = '"+snippetItem.getCategory()+"' , "
                    +column_snippetContent+" = ?, "
                    +column_personName+" = '"+snippetItem.getPersonName()+"' , "
                    +column_date+" = '"+snippetItem.getDate()+"' , "
                    +column_time+" = '"+snippetItem.getTime()+"' , "
                    +column_viewsCount+" = '"+snippetItem.getViews()+"' , "
                    +column_likesCount+" = '"+snippetItem.getLikes()+"' , "
                    +column_commentsCount+" = '"+snippetItem.getComments()+"' , "
                    +column_userInfo+" = '"+snippetItem.getUserInfo()+"' , "
                    +column_isLiked+" = "+snippetItem.isLiked()+" , "
                    +column_userID+" = "+snippetItem.getUserID()+" , "
                    +column_anonymous+" = "+snippetItem.getAnonymous()+" WHERE "+column_snippetID+" = "+snippetItem.getSnippetID();
            try {
                database.execSQL(updateQuery,new String[]{snippetItem.getTitle(),snippetItem.getContent()});
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            Log.d("updateTimeline","count=0");
            this.addTimelineSnippet(snippetItem);
        }
        database.close();
        cursor.close();
    }

    public void updateSnippet(SnippetItem snippetItem,String origin){
        Log.d("updatesnippet ",origin);
        SQLiteDatabase database=this.getWritableDatabase();

        String table=null;

        if(origin.equals("feed"))
            table=snippetTable;
        else if(origin.equals("timeline"))
            table=timelineTable;

        String updateQuery="UPDATE "+table+" SET "+column_title+" = ?, "
                +column_category+" = '"+snippetItem.getCategory()+"' , "
                +column_snippetContent+" = ?,"
                +column_viewsCount+" = '"+snippetItem.getViews()+"' , "
                +column_likesCount+" = '"+snippetItem.getLikes()+"' , "
                +column_personName+" = '"+snippetItem.getPersonName()+"' , "
                +column_userInfo+" = '"+snippetItem.getUserInfo()+"' , "
                +column_commentsCount+" = '"+snippetItem.getComments()+"' WHERE "+column_snippetID+" = "+snippetItem.getSnippetID();

        try{
            database.execSQL(updateQuery,new String[]{snippetItem.getTitle(),snippetItem.getContent()});
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
    }

    public SnippetItem getSnippet(int snippetID,String origin){
        Log.d("getSnippet ",origin);
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

        SnippetItem snippetItem=null;

        String table=null;

        if(origin.equals("timeline"))
            table=timelineTable;
        else if(origin.equals("feed"))
            table=snippetTable;


        String selectQuery="SELECT * FROM "+table+" WHERE "+column_snippetID+" = "+snippetID;

        Cursor cursor=sqLiteDatabase.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){
                snippetItem=new SnippetItem(cursor.getInt(cursor.getColumnIndex(column_snippetID)),
                        cursor.getInt(cursor.getColumnIndex(column_userID)),
                        cursor.getString(cursor.getColumnIndex(column_snippetContent)),
                        cursor.getString(cursor.getColumnIndex(column_viewsCount)),
                        cursor.getString(cursor.getColumnIndex(column_personName)),
                        cursor.getString(cursor.getColumnIndex(column_likesCount)),
                        cursor.getString(cursor.getColumnIndex(column_commentsCount)),
                        cursor.getString(cursor.getColumnIndex(column_date)),
                        cursor.getString(cursor.getColumnIndex(column_time)),
                        cursor.getString(cursor.getColumnIndex(column_title)),
                        cursor.getString(cursor.getColumnIndex(column_category)),
                        cursor.getString(cursor.getColumnIndex(column_userInfo)),
                        cursor.getInt(cursor.getColumnIndex(column_isLiked)),
                        cursor.getInt(cursor.getColumnIndex(column_anonymous)));
        }

        cursor.close();
        sqLiteDatabase.close();

        return snippetItem;
    }

    public void clearFeed(){
        SQLiteDatabase database=this.getWritableDatabase();

        String deleteQuery="DELETE FROM "+snippetTable;

        try{
            database.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }
        database.close();
    }

    public void addUsers(int userID,String personName,String userName,int isFollowing){
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put(column_userID,userID);
        contentValues.put(column_personName,personName);
        contentValues.put(column_userName,userName);
        contentValues.put(column_isFollowing,isFollowing);

        Log.d("addUsers",userID+"");

        try{
            sqLiteDatabase.insert(usersTable,null,contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        sqLiteDatabase.close();
    }

    public ArrayList<UsersItem> getUsers(){
        ArrayList<UsersItem> usersItem=new ArrayList<>();

        SQLiteDatabase sqLiteDatabase=this.getReadableDatabase();

        String selectQuery="SELECT * FROM "+usersTable+" ORDER BY "+column_personName;

        Cursor cursor=sqLiteDatabase.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){
            do{
                usersItem.add(new UsersItem(cursor.getString(cursor.getColumnIndex(column_personName)),
                        cursor.getString(cursor.getColumnIndex(column_userName)),
                        cursor.getInt(cursor.getColumnIndex(column_userID)),
                        cursor.getInt(cursor.getColumnIndex(column_isFollowing))
                ));
                Log.d("getUsers",cursor.getInt(cursor.getColumnIndex(column_userID))+"");
            }while (cursor.moveToNext());
        }

        cursor.close();
        sqLiteDatabase.close();

        return usersItem;
    }

    public void deleteUsers(){
        SQLiteDatabase database=this.getWritableDatabase();

        String deleteQuery="DELETE FROM "+usersTable;

        try{
            database.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public void followUnfollow(int userID,int isFollowing){
        SQLiteDatabase database=this.getWritableDatabase();

        String updateQuery="UPDATE "+usersTable+" SET "+column_isFollowing+" = "+isFollowing+" WHERE "+column_userID+" = "+userID;

        try{
            database.execSQL(updateQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public void deleteNotifications(int snippetID){
        String deleteQuery="DELETE FROM "+notificationsTable+" WHERE "+column_snippet_user_ID+" = "+snippetID+" AND ("+column_notifType+" = 'like' OR "+column_notifType+" = 'comment')";

        SQLiteDatabase database=this.getWritableDatabase();

        try{
            database.execSQL(deleteQuery);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }


    public void clearDB() {
        String deleteSnippetTable = "DROP TABLE " + snippetTable;
        String deleteDiaryTable = "DROP TABLE " + diaryTable;
        String deleteNotificationTable = "DROP TABLE " + notificationsTable;
        String deleteTimelineTable = "DROP TABLE " + timelineTable;
        String deleteUsersTable = "DROP TABLE " +usersTable;
        String deleteCommentsTable="DROP TABLE "+commentsTable;

        SQLiteDatabase database=this.getWritableDatabase();

        try{
            database.execSQL(deleteUsersTable);
            database.execSQL(deleteCommentsTable);
            database.execSQL(deleteNotificationTable);
            database.execSQL(deleteDiaryTable);
            database.execSQL(deleteTimelineTable);
            database.execSQL(deleteSnippetTable);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
