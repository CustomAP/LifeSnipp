package com.custombuilder.ashishpawar.lifesnipp.server_connection.SignIn;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.BasicInfo;
import com.custombuilder.ashishpawar.lifesnipp.Login;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.FetchFeed;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 1/7/17.
 */

public class SendBasicInfo {
    private Context context;
    AQuery aQuery;
    String sessionIDString,firstName,lastName;

    String profileInfo="http://www.lifesnipp.com/index.php/User_profile/addBasicInfo";

    public SendBasicInfo(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        SessionID sessionID=new SessionID(context);
        sessionIDString=sessionID.getSessionID();
    }

    public void sendBasicProfileInfo(int gender, String country, final String birthDate){

        SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
        firstName=preferences.getString("firstName",null);
        lastName=preferences.getString("lastName",null);

        Map<String,Object> profileParams=new HashMap<>();
        profileParams.put("sessionID",sessionIDString);
        profileParams.put("gender",gender);
        profileParams.put("country",country);
        profileParams.put("birthDate",birthDate);
        profileParams.put("firstName",firstName);
        profileParams.put("lastName",lastName);


        aQuery.ajax(profileInfo,profileParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("basicProfileInfo","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("basicProfileInfo","object=null");
                    Toast.makeText(context,"Failed to send basic profile info",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200&&status.getCode()<300) {
                    if (code == 0) {
                        /*
                            saving birth date in SP
                         */
                        SharedPreferences preferences = context.getSharedPreferences("Profile", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("birthDate", birthDate);
                        editor.apply();
                        /*
                            fetch feed
                         */
                        FetchFeed fetchFeed = new FetchFeed(context);
                        fetchFeed.getFeed("Login");

                        /*
                            static variables to kill login and basic info activities
                         */
                        Login.isLoggedInToKill=true;
                        BasicInfo.isLoggedInToKill=true;
                    }
                }
            }
        });
    }
}
