package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 24/6/17.
 */

public class TimelineEvent {
    private boolean receivedData;

    public boolean isReceivedData() {
        return receivedData;
    }

    public TimelineEvent(boolean receivedData) {

        this.receivedData = receivedData;
    }
}
