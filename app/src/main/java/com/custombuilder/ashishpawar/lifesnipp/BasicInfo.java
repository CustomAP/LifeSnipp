package com.custombuilder.ashishpawar.lifesnipp;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.custombuilder.ashishpawar.lifesnipp.server_connection.SignIn.SendBasicInfo;

import info.hoang8f.widget.FButton;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 1/7/17.
 */

public class BasicInfo extends AppCompatActivity implements View.OnClickListener{
    TextView tvMale,tvFemale,tvBirthDate;
    CardView cvBirthDate;
    Spinner sCountry;
    FButton bSaveBasicProfileInfo;
    int gender=-1;
    public static boolean isLoggedInToKill=false;

    @Override
    protected void onPause() {
        super.onPause();
        /*
            killing activity if logged in
         */
        if(isLoggedInToKill)
            finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_info);

        initialize();
    }

    private void initialize() {
        /*
            initializing views
         */
        tvMale = (TextView) findViewById(R.id.tvMale);
        tvFemale=(TextView)findViewById(R.id.tvFemale);
        cvBirthDate=(CardView)findViewById(R.id.cvBirthDate);
        sCountry=(Spinner)findViewById(R.id.sCountries);
        bSaveBasicProfileInfo=(FButton)findViewById(R.id.bSaveBasicProfileInfo);
        tvBirthDate=(TextView)findViewById(R.id.tvBirthDate);

        /*
            setting on click listeners
         */
        tvMale.setOnClickListener(this);
        tvFemale.setOnClickListener(this);
        cvBirthDate.setOnClickListener(this);
        bSaveBasicProfileInfo.setOnClickListener(this);

        /*
            setting items of spinner
         */
        String[] countries=getResources().getStringArray(R.array.countries_array);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,countries);

        sCountry.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvMale:
                /*
                    male
                 */
                tvMale.setTextColor(getResources().getColor(R.color.colorAccent));
                tvFemale.setTextColor(Color.parseColor("#808080"));
                gender=1;
                break;
            case R.id.tvFemale:
                /*
                    female
                 */
                tvFemale.setTextColor(getResources().getColor(R.color.colorAccent));
                tvMale.setTextColor(Color.parseColor("#808080"));
                gender=0;
                break;
            case R.id.cvBirthDate:
                /*
                    birthdate
                 */
                DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvBirthDate.setText(i+"-"+i1+"-"+i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvBirthDate.setText(i+"-"+i1+"-0"+i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvBirthDate.setText(i+"-0"+i1+"-"+i2);
                        else
                            tvBirthDate.setText(i+"-0"+i1+"-0"+i2);
                    }
                },1998,0,1);
                datePickerDialog.show();
                break;
            case R.id.bSaveBasicProfileInfo:
                /*
                    save
                 */
                if(gender!=-1&&!sCountry.getSelectedItem().toString().equals("")){
                    SendBasicInfo sendBasicInfo=new SendBasicInfo(this);
                    sendBasicInfo.sendBasicProfileInfo(gender,sCountry.getSelectedItem().toString(),tvBirthDate.getText().toString());
                }
                break;
        }
    }
}
