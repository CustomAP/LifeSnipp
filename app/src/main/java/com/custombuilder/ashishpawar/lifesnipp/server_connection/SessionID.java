package com.custombuilder.ashishpawar.lifesnipp.server_connection;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/6/17.
 */

public class SessionID {
    private Context context;
    private String userName,password,sessionID;
    private AQuery aQuery;

    private String assignSessionID="http://www.lifesnipp.com/index.php/User_login/assignSessionID";

    public SessionID(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void assignSessionID(String userName,String password){
        this.userName=userName;
        this.password=password;


        Map<String,Object> sessionParams=new HashMap<>();
        sessionParams.put("userName",userName);
        sessionParams.put("password",password);

        aQuery.ajax(assignSessionID,sessionParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                sessionID=null;
                if(object!=null){
                    try {
                        sessionID=object.getString("SessionID");
                        Log.d("assignSessionID",sessionID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("assignSessionID","object=null");
                    Toast.makeText(context,"Failed to login",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    if(sessionID.equals("")){
                        Log.d("assignSessionID","sessionID=null");
                        Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                    }else{
                        Log.d("assignSessionID",sessionID);
                        /*
                            saving session ID
                         */
                        saveSessionID();
                    }
                }
            }
        });

    }

    private void saveSessionID() {
        /*
            saving sessionID in SP
         */
        SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.remove("sessionID");
        editor.putString("sessionID",sessionID);
        editor.apply();
    }


    public String getSessionID(){
        String sessionID=null;

        /*
            getting sessionID from SP
         */
        SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
        sessionID=preferences.getString("sessionID",null);
        return sessionID;
    }

}
