package com.custombuilder.ashishpawar.lifesnipp.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.custombuilder.ashishpawar.lifesnipp.R;

/**
 * Created by Ashish Pawar on 5/14/2017.
 */

public class ProfileView extends AppCompatActivity implements View.OnClickListener{
    private ProfilePagerAdapter profilePagerAdapter;
    private ViewPager viewPager;
    private int userID;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        /*
            initialize adapter and view pager
         */
        profilePagerAdapter=new ProfilePagerAdapter(getSupportFragmentManager());
        viewPager= (ViewPager) findViewById(R.id.container_profile);
        viewPager.setAdapter(profilePagerAdapter);

        /*
            getting userID as extra
         */
        userID=getIntent().getIntExtra("userID",0);

        /*
            setting tablayout
         */
        TabLayout tabLayout= (TabLayout) findViewById(R.id.tabs_profile);
        tabLayout.setupWithViewPager(viewPager);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onClick(View view) {

    }
    private class ProfilePagerAdapter extends FragmentStatePagerAdapter {
        public ProfilePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle args=new Bundle();
            switch (position){
                case 0:
                    /*
                        about fragment
                     */
                    AboutFragment aboutFragment=new AboutFragment();
                    args.putString("origin","ProfileView");
                    args.putInt("userID",userID);
                    aboutFragment.setArguments(args);
                    return aboutFragment;
                case 1:
                    /*
                        timeline fragment
                     */
                    TimelineFragment timelineFragment=new TimelineFragment();
                    args.putInt("userID",userID);
                    args.putString("origin","ProfileView");
                    timelineFragment.setArguments(args);
                    return timelineFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "About";
                case 1:
                    return "Timeline";
            }
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) // Press Back Icon
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
