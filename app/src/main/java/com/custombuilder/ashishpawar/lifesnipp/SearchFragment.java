package com.custombuilder.ashishpawar.lifesnipp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.custombuilder.ashishpawar.lifesnipp.Profile.ProfileView;
import com.custombuilder.ashishpawar.lifesnipp.ViewUsers.UsersItem;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/8/17.
 */

public class SearchFragment extends Fragment {
    AutoCompleteTextView atvSearch;
    ArrayList<UsersItem> usersList;
    List<HashMap<String,String>> searchList;
    String sessionIDString;
    AQuery aQuery;
    String searchUsers="http://www.lifesnipp.com/index.php/User_profile/searchUsers";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing auto complete text view
         */
        atvSearch=(CustomAutoCompleteTextView)view.findViewById(R.id.atvSearch);

        atvSearch.setHint("Search by Username");
        atvSearch.setHintTextColor(getResources().getColor(android.R.color.white));

        atvSearch.setTextColor(getResources().getColor(android.R.color.white));

        /*
            initializing variables
         */
        usersList=new ArrayList<>();

        searchList=new ArrayList<>();

        aQuery=new AQuery(getContext());
        /*
            getting sessionID
         */

        SessionID sessionID=new SessionID(getContext());
        sessionIDString=sessionID.getSessionID();

        /*
            getting data from server based on input and adding to arraylist
         */

        atvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usersList.clear();
            }

            @Override
            public void afterTextChanged(Editable s) {
                usersList.clear();

                String input=atvSearch.getText().toString();

                Map<String,Object> searchParams=new HashMap<>();
                searchParams.put("sessionID",sessionIDString);
                searchParams.put("input",input);

                aQuery.ajax(searchUsers,searchParams, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        int num=0,userID;
                        String firstName,lastName,userName;

                        if(object!=null){
                            Log.d("searchUsers","object!=null");
                            try{
                                num=object.getInt("num");

                                String users=object.getString("users");

                                JSONObject reader=new JSONObject(users);

                                for(int i=0;i<num;i++){
                                    JSONObject obj=reader.getJSONObject(String.valueOf(i));

                                    firstName=obj.getString("firstName");
                                    lastName=obj.getString("lastName");
                                    userName=obj.getString("userName");
                                    userID=obj.getInt("userID");

                                    usersList.add(new UsersItem(firstName+" "+lastName,userName,userID,0));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            Log.d("searchUsers","object=null");
                            Toast.makeText(getContext(),"Failed to search users",Toast.LENGTH_SHORT).show();
                        }

                        if(status.getCode()==200){
                            updateAdapter(num);
                        }
                    }
                });
            }
        });

    }

    private void updateAdapter(int num) {
        searchList.clear();
        for(int i=0;i<num;i++){
            Log.d("num",""+num);
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("userName", "@"+usersList.get(i).getUserName());
            hm.put("personName", usersList.get(i).getPersonName());
            hm.put("userID",String.valueOf(usersList.get(i).getUserID()));
            searchList.add(hm);
        }

        /*
            Keys used in Hashmap
          */
        final String[] from = { "userName","personName","userID"};

        /*
            Ids of views in listview_layout
          */
        int[] to = { R.id.tvUserNameSearch,R.id.tvPersonNameSearch,R.id.ivSearchProfilePic};

        /*
            setting adapter
         */
        final Drawable defaultProfPic=getContext().getResources().getDrawable(R.drawable.generic_profile);

        SimpleAdapter adapter = new SimpleAdapter(getContext(), searchList, R.layout.search_single_item, from, to){
            @Override
            public void setViewImage(final ImageView v, String value) {
                Glide.with(getContext()).load("http://www.lifesnipp.com/profile_pic/"+value+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(v) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            v.setImageDrawable(circularBitmapDrawable);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        atvSearch.setAdapter(adapter);
        adapter.notifyDataSetChanged();     //removed the bug of not showing some users!!!!!!!!!!!

        atvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> item = (HashMap<String, String>) parent.getItemAtPosition(position);
                String userID = item.get("userID");

//                Toast.makeText(getContext(),userID,Toast.LENGTH_SHORT).show();
                atvSearch.setText("");

                Intent i=new Intent(getContext(),ProfileView.class);
                i.putExtra("userID",Integer.parseInt(userID));
                startActivity(i);
            }
        });
    }
}
