package com.custombuilder.ashishpawar.lifesnipp.server_connection.Login;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.DiaryUpdateEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.Home.DiarySnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.FetchFeed;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/6/17.
 */

public class FetchDiaryContent {
    private Context context;
    private ArrayList<DiarySnippetItem> diarySnippetItems;
    private AQuery aQuery;

    private String getDiarySnippets="http://www.lifesnipp.com/index.php/Diary/getDiarySnippets";

    public FetchDiaryContent(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        diarySnippetItems=new ArrayList<>();
    }

    public void getDiaryContent(final String origin){

        SessionID sessionID=new SessionID(context);
        String sessionIDString=sessionID.getSessionID();

        final Map<String,Object> diaryparams=new HashMap<>();
        diaryparams.put("sessionID",sessionIDString);

        aQuery.ajax(getDiarySnippets,diaryparams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int length=0;
                if(object!=null){
                    Log.d("getDiaryContent","object!=null");
                    String title,content,category,uploadDate,uploadTime;
                    int snippetID;
                    try{
                        length=object.getInt("num");
                        String diarySnippets=object.getString("diarySnippets");

                        JSONObject reader=new JSONObject(diarySnippets);

                        for(int i=0;i<length;i++){
                            JSONObject obj=reader.getJSONObject(String.valueOf(i));
                            title=obj.getString("title");
                            content=obj.getString("content");
                            category=obj.getString("category");
                            uploadDate=obj.getString("uploadDate");
                            uploadTime=obj.getString("uploadTime");
                            snippetID=obj.getInt("snippetID");
                            diarySnippetItems.add(new DiarySnippetItem(content,title,category,uploadDate,uploadTime,snippetID));
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    if(origin.equals("Login")){
                        /*
                            if user has not shared any snippets before signing out and the logged in.
                         */
                        FetchFeed fetchFeed=new FetchFeed(context);
                        fetchFeed.getFeed("Login");
                    }else {
                        EventBus.getDefault().post(new FetchFailed(true));
                        Log.d("getDiaryContent", "object=null");
                        Toast.makeText(context, "Failed to get Diary Snippets", Toast.LENGTH_SHORT).show();
                    }
                }
                if(status.getCode()==200 &&status.getCode()<300){

                    if(origin.equals("Login")) {
                        /*
                            adding diary contents in db
                         */
                        DbHelper dbHelper=new DbHelper(context);
                        for (int i=0;i<length;i++)
                            dbHelper.addDiarySnippets(diarySnippetItems.get(i));
                        /*
                            getting feed
                         */
                        FetchFeed fetchFeed=new FetchFeed(context);
                        fetchFeed.getFeed("Login");
                    }else if(origin.equals("Diary")){
                        Log.d("updateDiary","updating");
                        /*
                            updating diary contents
                         */
                        DbHelper helper=new DbHelper(context);
                        for(int i=0;i<length;i++){
                            helper.updateDiary(diarySnippetItems.get(i));
                        }
                        EventBus.getDefault().post(new DiaryUpdateEvent(true));
                    }
                }
            }
        });

    }
}
