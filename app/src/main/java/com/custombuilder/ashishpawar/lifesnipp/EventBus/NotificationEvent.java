package com.custombuilder.ashishpawar.lifesnipp.EventBus;

import com.custombuilder.ashishpawar.lifesnipp.Notifications.NotificationsItem;

import java.util.ArrayList;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/7/17.
 */

public class NotificationEvent {
    private ArrayList<NotificationsItem> notificationsItems;

    public ArrayList<NotificationsItem> getNotificationsItems() {
        return notificationsItems;
    }

    public NotificationEvent(ArrayList<NotificationsItem> notificationsItems) {

        this.notificationsItems = notificationsItems;
    }
}
