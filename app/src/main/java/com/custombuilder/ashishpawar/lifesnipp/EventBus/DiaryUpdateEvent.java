package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/7/17.
 */

public class DiaryUpdateEvent {
    private boolean isDiaryUpdated;

    public boolean isDiaryUpdated() {
        return isDiaryUpdated;
    }

    public DiaryUpdateEvent(boolean isDiaryUpdated) {

        this.isDiaryUpdated = isDiaryUpdated;
    }
}
