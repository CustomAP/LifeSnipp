package com.custombuilder.ashishpawar.lifesnipp.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FollowEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ProfileEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.ViewUsers.UsersView;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.FetchProfileInfo;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;
import info.hoang8f.widget.FButton;

/**
 * Created by Ashish Pawar on 3/12/2017.
 */

public class AboutFragment extends Fragment implements View.OnClickListener{
    private ImageView ivProfile;
    private ImageView ivSettings;
    private FButton bFollow;
    private TextView tvPersonName, tvBio,tvUserName,tvBirthDate,tvFollowers,tvFollowing,tvLikes,tvViews;
    private String origin;
    private int selfUserID,userID,followingCount,followersCount,follow,views,likes;
    private String firstName,lastName,userName,emailID, bio,birthDate;
    SharedPreferences preferences;
    LinearLayout llAboutFragment,llFollowers,llFollowing;
    AVLoadingIndicatorView avi;
    Bitmap profileBitmap;
    PullRefreshLayout prlAbout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_about,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing views
         */
        llAboutFragment=(LinearLayout)view.findViewById(R.id.llAboutFragment);
        avi=(AVLoadingIndicatorView)view.findViewById(R.id.aviAbout);

        /*
            getting arguments
         */
        origin=getArguments().getString("origin");
        userID=getArguments().getInt("userID");

        /*
            showing loading indicator if origin is profileView
         */
        if(origin.equals("ProfileView")){
            llAboutFragment.setVisibility(View.GONE);
            avi.show();
        }

        /*
            initializing views
         */
        ivProfile=(ImageView)view.findViewById(R.id.ivProfile);
        bFollow=(FButton)view.findViewById(R.id.bFollow);
        ivSettings=(ImageView) view.findViewById(R.id.ivSettings);
        tvPersonName=(TextView)view.findViewById(R.id.tvPersonNameAbout);
        tvBio =(TextView)view.findViewById(R.id.tvBioAbout);
        tvUserName=(TextView)view.findViewById(R.id.tvUserNameAbout);
        tvBirthDate=(TextView)view.findViewById(R.id.tvBirthDateAbout);
        tvFollowers=(TextView)view.findViewById(R.id.tvFollowers);
        tvFollowing=(TextView)view.findViewById(R.id.tvFollowing);
        tvLikes=(TextView)view.findViewById(R.id.tvLikesAbout);
        tvViews=(TextView)view.findViewById(R.id.tvViewsAbout);
        llFollowers=(LinearLayout)view.findViewById(R.id.llFollowers);
        llFollowing=(LinearLayout)view.findViewById(R.id.llFollowing);
        prlAbout=(PullRefreshLayout)view.findViewById(R.id.prlAbout);
        prlAbout.setColor(R.color.colorPrimaryDark);

        /*
            setting onclick listeners
         */
        ivProfile.setOnClickListener(this);
        bFollow.setOnClickListener(this);
        ivSettings.setOnClickListener(this);

        /*
            getting selfUserID
         */
        preferences=getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        selfUserID=preferences.getInt("userID",0);

        /*
            setting profile pic
         */
        setProfilePic();

        /*
            getting profileInfo from sharedPref or server
         */
        if(origin.equals("ProfileFragment"))
            getProfileInfoFromSharedPref();
        else if(origin.equals("ProfileView"))
            getProfileInfoFromServer();

        /*
            showing/hiding settings icon, follow button
         */
        assert origin != null;
        if(origin.equals("ProfileFragment")){
            bFollow.setVisibility(View.GONE);
            avi.setVisibility(View.GONE);
            llAboutFragment.setVisibility(View.VISIBLE);
        }else if(origin.equals("ProfileView")){
            ivSettings.setVisibility(View.GONE);
        }

        /*
            setting on refresh listener
         */
        prlAbout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        /*
            starting usersView activity on followers or following clicked
         */
       // if(origin.equals("ProfileFragment")){
            llFollowing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getContext(),UsersView.class);
                    i.putExtra("origin","Following");
                    i.putExtra("userID",userID);
                    startActivity(i);
                }
            });

            llFollowers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getContext(),UsersView.class);
                    i.putExtra("origin","Followers");
                    i.putExtra("userID",userID);
                    startActivity(i);
                }
            });
       // }

        if(userID==selfUserID){
            bFollow.setVisibility(View.GONE);
        }
    }

    private void refresh() {
        if(origin.equals("ProfileFragment")) {
            SharedPreferences preferences = getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
            String userName = preferences.getString("userName", null);
            String password = preferences.getString("password", null);
            FetchProfileInfo fetchProfileInfo = new FetchProfileInfo(getContext());
            fetchProfileInfo.getSelfProfileInfo(userName, password, origin);
        }else if(origin.equals("ProfileView")){
            FetchProfileInfo fetchProfileInfo=new FetchProfileInfo(getContext());
            fetchProfileInfo.getOthersProfileInfo(userID);
        }
    }

    private void setProfilePic() {
        if(origin.equals("ProfileFragment"))
            loadImageFromStorage();
        else if(origin.equals("ProfileView")){
            Log.d("gettingProfPic","glide");
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load("http://www.lifesnipp.com/profile_pic/"+userID+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(ivProfile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivProfile.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }



    private void setUI() {
        tvPersonName.setText(firstName + " " + lastName);
        tvBio.setText(bio);
        tvUserName.setText("@" + userName);

        if (follow == 1) {
            bFollow.setText(getResources().getString(R.string.unfollow));
        }

        tvBirthDate.setText(convertDate(birthDate,2));
        tvFollowers.setText(followersCount + "");
        tvFollowing.setText(followingCount + "");
        tvViews.setText(views + "");
        tvLikes.setText(likes+"");
    }

    private void getProfileInfoFromServer() {
        FetchProfileInfo fetchProfileInfo=new FetchProfileInfo(getContext());
        fetchProfileInfo.getOthersProfileInfo(userID);
    }

    private void getProfileInfoFromSharedPref() {
        firstName=preferences.getString("firstName",null);
        lastName=preferences.getString("lastName",null);
        userName=preferences.getString("userName",null);
        emailID=preferences.getString("email",null);
        bio =preferences.getString("bio",null);
        birthDate=preferences.getString("birthDate",null);
        followersCount=preferences.getInt("followersCount",0);
        followingCount=preferences.getInt("followingCount",0);
        views=preferences.getInt("views",0);
        likes=preferences.getInt("likes",0);

        setUI();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivProfile:
                /*
                    profile pic
                 */
                Intent i=new Intent(getContext(),ProfilePicDialog.class);
                i.putExtra("origin",origin);
                i.putExtra("userID",userID);
                startActivity(i);
                break;
            case R.id.bFollow:
                /*
                    follow
                 */
                boolean isFollowing;
                if(follow==0){
                    isFollowing=false;
                }else{
                    isFollowing=true;
                }

                FetchProfileInfo fetchProfileInfo=new FetchProfileInfo(getContext());
                fetchProfileInfo.followUnfollow(userID,isFollowing,"Profile");

                break;

            case R.id.ivSettings:
                /*
                    settings
                 */
                i=new Intent(getContext(),Settings.class);
                startActivity(i);
                break;
        }
    }

    public void onEvent(ProfileEvent profileEvent) {
        /*
            after fetching profile info from server
         */
        Log.d("onEvent", "profileEvent");
        firstName = profileEvent.getFirstName();
        lastName = profileEvent.getLastName();
        userName = profileEvent.getUserName();
        bio = profileEvent.getBio();
        birthDate = profileEvent.getBirthdate();
        followersCount = profileEvent.getFollowers();
        followingCount = profileEvent.getFollowing();
        follow = profileEvent.getFollow();
        views=profileEvent.getViews();
        likes=profileEvent.getLikes();

        llAboutFragment.setVisibility(View.VISIBLE);

        /*
            setting UI
         */
        setUI();

        /*
            hiding loading indicator
         */
        try {
            avi.hide();
            avi.setVisibility(View.GONE);
            prlAbout.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onEvent(FollowEvent followEvent){
        /*
            after followed/unfollowed
         */
        boolean followed,unfollowed;

        followed=followEvent.isFollowed();
        unfollowed=followEvent.isUnfollowed();

        /*
            if followed
         */
        if(followed) {
            bFollow.setText(getResources().getString(R.string.unfollow));
            follow=1;
            try{
                int followers=Integer.valueOf(tvFollowers.getText().toString());
                followers++;
                tvFollowers.setText(followers+"");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        /*
            if unfollowed
         */
        if(unfollowed){
            bFollow.setText(getResources().getString(R.string.follow));
            follow=0;
            try{
                int followers=Integer.valueOf(tvFollowers.getText().toString());
                followers--;
                tvFollowers.setText(followers+"");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private String convertDate(String time, int type) {
        String inputPattern = null;
        String outputPattern = null;
        if (type == 1) {
            inputPattern = "dd MMM yyyy";
            outputPattern = "yyyy-MM-dd";
        } else if (type == 2) {
            outputPattern = "dd MMM yyyy";
            inputPattern = "yyyy-MM-dd";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(origin.equals("ProfileFragment"))
            getProfileInfoFromSharedPref();

        if(origin.equals("ProfileFragment")){
            loadImageFromStorage();
        }

        /*
            this removes the bug of keeping correct profile pic but wrong data after pressing back button after repeated user profile views
            Also refreshes on resume so that follow count updates automatically
         */
        refresh();
    }

    public void loadImageFromStorage(){
        try {
            String path = preferences.getString("profilePic", null);
            File f = new File(path, "profilePic.jpg");
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load(f).asBitmap().centerCrop().error(defaultProfPic).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(new BitmapImageViewTarget(ivProfile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivProfile.setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onEvent(FetchFailed fetchFailed){
        if(fetchFailed.isRefreshFailed()){
            prlAbout.setRefreshing(false);
        }
    }
}
