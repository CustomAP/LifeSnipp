package com.custombuilder.ashishpawar.lifesnipp.server_connection.Login;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.TimelineEvent;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 20/6/17.
 */

public class FetchTimelineSnippets {
    private Context context;
    private AQuery aQuery;
    private ArrayList<SnippetItem> snippetItems;
    int length;
    Activity activity;
    private String getTimelineSnippets="http://www.lifesnipp.com/index.php/Snippets/getTimelineSnippets";

    public FetchTimelineSnippets(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        snippetItems=new ArrayList<>();
        activity=(Activity)context;
    }

    public void getTimelineSnippets(final int userID, int num, final String origin){

        SessionID sessionID=new SessionID(context);
        String sessionIDString = sessionID.getSessionID();

        final Map<String,Object> timelineParams=new HashMap<>();
        timelineParams.put("sessionID",sessionIDString);
        timelineParams.put("userID",userID);
        timelineParams.put("num",num);

        aQuery.ajax(getTimelineSnippets,timelineParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(object!=null){
                    Log.d("getTimelineSnippets","object!=null");
                    String title,content,category,uploadDate,uploadTime;
                    int userID,postID,views,likes,comments,isLiked,anonymous;
                    try{
                        length=object.getInt("num");
                        String firstName=object.getString("firstName");
                        String lastName=object.getString("lastName");
                        String bio=object.getString("bio");

                        String timelineSnippets=object.getString("snippets");

                        JSONObject reader=new JSONObject(timelineSnippets);

                        for(int i=0;i<length;i++){
                            JSONObject obj=reader.getJSONObject(String.valueOf(i));
                            postID=obj.getInt("postID");
                            title=obj.getString("title");
                            content=obj.getString("content");
                            category=obj.getString("category");
                            uploadDate=obj.getString("uploadDate");
                            uploadTime=obj.getString("uploadTime");
                            userID=obj.getInt("userID");
                            views=obj.getInt("views");
                            likes=obj.getInt("likes");
                            comments=obj.getInt("comments");
                            isLiked=obj.getInt("isLiked");
                            anonymous=obj.getInt("isAnonymous");

                            snippetItems.add(new SnippetItem(postID,userID,content,views+"",firstName+" "+lastName,likes+"",comments+"",uploadDate,uploadTime,title,category,bio,isLiked,anonymous));
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    if(origin.equals("Login")){
                        /*
                            If user has not added any snippets to diary before signing out and has now logged in
                         */
                        getDiaryContent();
                    }else {
                        EventBus.getDefault().post(new FetchFailed(true));
                        Log.d("getTimelineSnippets", "object=null");
                        Toast.makeText(context, "Failed to get timeline snippets", Toast.LENGTH_SHORT).show();
                    }
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
                            int userIDSelf=preferences.getInt("userID",0);
                            if(origin.equals("Login")) {
                                /*
                                    saving timeline snippets in db
                                 */
                                DbHelper helper = new DbHelper(context);
                                for (int i = 0; i < length; i++)
                                    helper.addTimelineSnippet(snippetItems.get(i));
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        /*
                                            getting diary content
                                         */
                                        getDiaryContent();
                                    }
                                });
                            }else if(origin.equals("ProfileView")) {
                                DbHelper helper = new DbHelper(context);
                                for (int i = 0; i < length; i++)
                                    helper.addTimelineSnippet(snippetItems.get(i));
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        /*
                                            others timeline snippets fetched
                                         */
                                        snippetsFetchedEvent();
                                    }
                                });
                            }else if(origin.equals("ProfileFragment_refresh")||origin.equals("ProfileView_refresh")) {
                                /*
                                    refresh timeline snippets
                                 */
                                DbHelper helper = new DbHelper(context);

                                for (int i = 0; i < length; i++)
                                    helper.updateTimeline(snippetItems.get(i));
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        refreshTimeline();
                                    }
                                });
                            }
                        }
                    }).start();
                }
            }
        });

    }

    private void refreshTimeline() {
        EventBus.getDefault().post(new TimelineEvent(true));
    }

    private void snippetsFetchedEvent() {
        EventBus.getDefault().post(new TimelineEvent(true));
    }

    private void getDiaryContent() {
        /*
            getting diary content
         */
        FetchDiaryContent fetchDiaryContent=new FetchDiaryContent(context);
        fetchDiaryContent.getDiaryContent("Login");
    }
}
