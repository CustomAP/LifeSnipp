package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.baoyz.widget.PullRefreshLayout;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FeedEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.FetchFeed;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Snippet;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import info.hoang8f.widget.FButton;

/**
 * Created by Ashish Pawar on 3/12/2017.
 */

public class FeedFragment extends Fragment {
    private RecyclerView feed;
    private ArrayList<SnippetItem> snippetItems;
    private FloatingActionButton fabPlus;
    HomeFragment homeFragment;
    private DbHelper dbHelper;
    private boolean loading = true;
    private SnippetAdapter adapter;
    private static boolean wasPaused = false;
    LinearLayout llFeed,llTryAgain;
    AVLoadingIndicatorView aviFeed, aviLoading;
    PullRefreshLayout prlFeed;
    FButton bTryAgain;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing variables
         */
        snippetItems = new ArrayList<>();
        dbHelper = new DbHelper(getContext());

        /*
            registering event bus
         */
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
            initializing views
         */
        feed = (RecyclerView) view.findViewById(R.id.rvFeed);
        fabPlus = (FloatingActionButton) view.findViewById(R.id.fabPlusFeed);
        llFeed = (LinearLayout) view.findViewById(R.id.llFeed);
        llTryAgain=(LinearLayout)view.findViewById(R.id.llTryAgain);
        aviFeed = (AVLoadingIndicatorView) view.findViewById(R.id.aviFeed);
        aviLoading = (AVLoadingIndicatorView) view.findViewById(R.id.aviFeedLoading);
        bTryAgain=(FButton)view.findViewById(R.id.bTryAgain);
        prlFeed = (PullRefreshLayout) view.findViewById(R.id.prlFeed);
        prlFeed.setColor(R.color.colorPrimaryDark);

        /*
            setting recycler view
         */
        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        feed.setItemAnimator(new DefaultItemAnimator());
        feed.setLayoutManager(manager);

        /*
            setting on click listener for fab
         */
        fabPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), NewSnippet.class);
                i.putExtra("tab", "feed");
                i.putExtra("type", "new");
                startActivity(i);
            }
        });

        /*
            adding on scroll listener to recycler view for hiding fab
         */
        feed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fabPlus.isShown()) {
                    fabPlus.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fabPlus.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        /*
            setting adapter
         */
        adapter = new SnippetAdapter(snippetItems, "feed");
        feed.setAdapter(adapter);

        /*
            getting data from server
         */
        getDataFromServer();

        /*
            adding on scroll listener for loading more feed
         */
        feed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = manager.getChildCount();
                    totalItemCount = manager.getItemCount();
                    pastVisiblesItems = manager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("timeline", "Last Item Wow !");
                            aviLoading.setVisibility(View.VISIBLE);
                            aviLoading.show();
                            getDataFromServer();
                        }
                    }
                }
            }
        });

        /*
            setting on refresh listener
         */
        prlFeed.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        bTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });


        if(!isNetworkConnected()){
            llFeed.setVisibility(View.GONE);
            llTryAgain.setVisibility(View.VISIBLE);
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    private void getDataFromServer() {
        FetchFeed fetchfeed = new FetchFeed(getContext());
        fetchfeed.getFeed("FeedFragment");

    }

    private void fillList() {
        snippetItems = dbHelper.getSnippets();
        Log.d("fillList", "done");
    }


    public void onEvent(FeedEvent feedEvent) {
        /*
            after refreshing
         */
        boolean isReceivedData = feedEvent.isReceivedData();
        if (isReceivedData) {
            fillList();
            aviLoading.setVisibility(View.GONE);
            aviLoading.hide();
            adapter.updateAdapter(snippetItems);
            loading = true;
            prlFeed.setRefreshing(false);
            aviFeed.setVisibility(View.GONE);
            aviFeed.hide();
            llFeed.setVisibility(View.VISIBLE);
            llTryAgain.setVisibility(View.GONE);
        }
    }

    private void refresh() {
        SharedPreferences preferences = getContext().getSharedPreferences("AppInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("lowerLimit");
        editor.apply();
        getDataFromServer();
    }

    public void onEvent(FetchFailed fetchFailed){
        if(fetchFailed.isRefreshFailed()){
            prlFeed.setRefreshing(false);
            aviFeed.setVisibility(View.GONE);
        }
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        Thread refresh=new Thread(){
//            public void run(){
//                try {
//                    sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }finally {
//                    new Runnable(){
//                        public void run(){
//                            adapter.updateAdapter(snippetItems);
//                        }
//                    };
//                }
//            }
//        };
//        refresh.start();
//    }

}
