package com.custombuilder.ashishpawar.lifesnipp;

import android.content.Context;
import android.util.AttributeSet;

import java.util.HashMap;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/8/17.
 */

public class CustomAutoCompleteTextView extends android.support.v7.widget.AppCompatAutoCompleteTextView {
    public CustomAutoCompleteTextView(Context context,AttributeSet attrs) {
        super(context,attrs);
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        return hm.get("userName");
    }
}
