package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/6/17.
 */

public class FeedEvent {
    private boolean receivedData;

    public boolean isReceivedData() {
        return receivedData;
    }

    public FeedEvent(boolean receivedData) {

        this.receivedData = receivedData;
    }
}
