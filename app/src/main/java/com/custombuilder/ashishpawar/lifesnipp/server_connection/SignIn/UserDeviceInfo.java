package com.custombuilder.ashishpawar.lifesnipp.server_connection.SignIn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.BasicInfo;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/6/17.
 */

public class UserDeviceInfo {
    private String emailId,userName,password,firstName,lastName;
    private Context context;
    private int userID;
    private TelephonyManager telephonyManager;
    Map<String, Object> userDeviceInfoParams;
    Map<String, Object> userAccountInfoParams;
    private AQuery aQuery;

    private String addUser="http://www.lifesnipp.com/index.php/User_login/sign_in";
    private String addDeviceInfo="http://www.lifesnipp.com/index.php/User_login/addUserDeviceInfo";

    public UserDeviceInfo(Context context, String emailId, String userName, String password, String firstName, String lastName){
        this.context=context;
        this.emailId=emailId;
        this.userName=userName;
        this.password=password;
        this.firstName=firstName;
        this.lastName=lastName;
        telephonyManager=(TelephonyManager)this.context.getSystemService(Context.TELEPHONY_SERVICE);
        aQuery=new AQuery(context);
    }

    public void insertInfo(){
        userAccountInfoParams =new HashMap<String,Object>();
        userAccountInfoParams.put("userName" , userName);
        userAccountInfoParams.put("password",password);
        userAccountInfoParams.put("emailID",emailId);


        aQuery.ajax(addUser,userAccountInfoParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try {
                        code=object.getInt("userID");
                        Log.d("addUser","userID:"+code);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("addUser","object=null");
                    Toast.makeText(context,"Failed to sign in",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    if(code==0){
                        Log.d("addUser","code=0");
                        Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                    }else{
                        Log.d("addUser","code!=0");
                        /*
                            getting userID
                         */
                        userID=code;
                        /*
                            inserting device info
                         */
                        insertDeviceInfo();
                    }
                }
            }
        });

    }

    private void insertDeviceInfo() {
        userDeviceInfoParams = new HashMap<String, Object>();
        userDeviceInfoParams.put("userID",userID);
        userDeviceInfoParams.put("board", Build.BOARD );
        userDeviceInfoParams.put("bootloader", Build.BOOTLOADER);
        userDeviceInfoParams.put("brand", Build.BRAND);
        userDeviceInfoParams.put("device", Build.DEVICE);
        userDeviceInfoParams.put("display", Build.DISPLAY);
        userDeviceInfoParams.put("hardware", Build.HARDWARE);
        userDeviceInfoParams.put("manufacturer", Build.MANUFACTURER);
        userDeviceInfoParams.put("model", Build.MODEL);
        userDeviceInfoParams.put("product", Build.PRODUCT);

        aQuery.ajax(addDeviceInfo,userDeviceInfoParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try {
                        code=object.getInt("ResultSet");
                        Log.d("addUserDeviceInfo",code+"");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("addUserDeviceInfo","object=null");
                    Toast.makeText(context,"Failed to sign in",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    if(code==0){
                        Log.d("addUserDeviceInfo","code=0");
                        /*
                            saving basic info
                         */
                        saveBasicInfo();
                    }else{
                        Log.d("addUserDeviceInfo","code!=0");
                        Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void saveBasicInfo(){
        /*
            add basic info
         */
        SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putInt("userID",userID);
        editor.putString("firstName",firstName);
        editor.putString("lastName",lastName);
        editor.putString("userName",userName);
        editor.putString("password",password);
        editor.putString("email",emailId);
        editor.putInt("followingCount",0);
        editor.putInt("followersCount",0);
        editor.apply();

        /*
            assign sessionID
         */
        SessionID sessionID=new SessionID(context);
        sessionID.assignSessionID(userName,password);

        timer.start();
    }

    private  Thread timer=new Thread(){
        public void run(){

            try{
                sleep(1500);
            }catch (Exception e){
                e.printStackTrace();
            }

            Intent i=new Intent(context, BasicInfo.class);
            context.startActivity(i);
        }
    };

}
