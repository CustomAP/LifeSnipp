package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.CommentEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FeedEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.Profile.ProfileView;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.ViewUsers.UsersView;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Snippet;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import de.greenrobot.event.EventBus;
import me.grantland.widget.AutofitTextView;

/**
 * Created by Ashish Pawar on 5/14/2017.
 */

public class SnippetView extends AppCompatActivity implements View.OnClickListener{
    CardView cvProfile,cvComments;
    TextView tvPersonName,tvAbout,tvCategory,tvContent,tvLikes,tvViews,tvComments;
    AutofitTextView tvTitle;
    MaterialEditText etComment;
    CircleButton bSendComment;
    private SnippetItem snippet;
    private int userID,selfUserID;
    private ShineButton heart;
    private String origin;
    private RecyclerView rvComments;
    private ArrayList<CommentItem> commentItems;
    private AVLoadingIndicatorView avi;
    private CommentAdapter commentAdapter;
    private LinearLayoutManager linearLayoutManager;
    private int num=0;
    private boolean loading=true;
    NestedScrollView nsv;
    CircularImageView ivProfile;
    PullRefreshLayout prlSnippetView;
    private ProgressBar pbSendComment;

    @Override
    protected void onPause() {
        super.onPause();

        /*
            unregistering eventbus
         */
        try{
            EventBus.getDefault().unregister(this);
        }catch (Exception e){
            e.printStackTrace();
        }

        /*
            deleting comments from database
         */
        DbHelper helper=new DbHelper(getApplicationContext());
        helper.clearComments();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snippet_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*
            registering eventbus
         */
        try{
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }

        /*
            initialize
         */
        initialize();

        /*
            setting recycler view
         */
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setAdapter(commentAdapter);
        rvComments.setItemAnimator(new DefaultItemAnimator());
        rvComments.setNestedScrollingEnabled(false);
    }

    private void initialize() {
        /*
            initializing views
         */
        cvProfile=(CardView)findViewById(R.id.cvProfileSnippetView);
        tvPersonName=(TextView)findViewById(R.id.tvPersonNameSnippetView);
        tvAbout=(TextView)findViewById(R.id.tvAboutSnippetView);
        tvCategory=(TextView)findViewById(R.id.tvCategorySnippetView);
        tvContent=(TextView)findViewById(R.id.tvSnippetContentSnippetView);
        tvLikes=(TextView)findViewById(R.id.tvLikesSnippetView);
        tvViews=(TextView)findViewById(R.id.tvViewsSnippetView);
        tvComments=(TextView)findViewById(R.id.tvCommentsSnippetView);
        tvTitle=(AutofitTextView)findViewById(R.id.tvTitleSnippetView);
        heart=(ShineButton)findViewById(R.id.sb_snippet_heart_SnippetView);
        rvComments=(RecyclerView)findViewById(R.id.rvComments);
        cvComments=(CardView)findViewById(R.id.cvComments);
        avi=(AVLoadingIndicatorView)findViewById(R.id.aviComments);
        etComment=(MaterialEditText)findViewById(R.id.etComment);
        bSendComment=(CircleButton)findViewById(R.id.bSendComment);
        nsv=(NestedScrollView)findViewById(R.id.nsv);
        ivProfile=(CircularImageView)findViewById(R.id.ivProfile_SnippetView);
        prlSnippetView=(PullRefreshLayout)findViewById(R.id.prlSnippetView);
        prlSnippetView.setColor(R.color.colorPrimaryDark);
        pbSendComment=(ProgressBar)findViewById(R.id.pbSendComment);

        pbSendComment.setVisibility(View.GONE);

        commentItems=new ArrayList<>();

        /*
            getting snippetInfo from Intent
         */
        snippet= (SnippetItem) getIntent().getExtras().getSerializable("snippetInfo");
        origin=getIntent().getStringExtra("origin");

        /*
            setting UI
         */
        setUI();

        /*
            getting selfUserID
         */
        SharedPreferences preferences=getSharedPreferences("Profile", Context.MODE_PRIVATE);
        selfUserID=preferences.getInt("userID",0);

        /*
            getting userID from snippet
         */
        userID=snippet.getUserID();


        /*
            setting profile pic
         */
        setProfileImage();

        /*
            setting onclick listener to card view if origin is feed
         */
        if(origin.equals("feed")&&userID!=0)
            cvProfile.setOnClickListener(this);

        /*
            setting on click listeners
         */
        heart.setOnClickListener(this);
        tvComments.setOnClickListener(this);
        etComment.setOnClickListener(this);
        bSendComment.setOnClickListener(this);

        /*
            initializing adapter and linearlayout manager
         */
        commentAdapter=new CommentAdapter(commentItems,snippet.getUserID(),snippet.getAnonymous());
        linearLayoutManager=new LinearLayoutManager(this);


        /*
            setting on scroll listener to nested scroll view
         */
        nsv.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        if (loading) {
                            loading = false;
                            Log.d("timeline", "Last Item Wow !");
                            Snippet snippetClass=new Snippet(getApplicationContext());
                            snippetClass.getComments(snippet.getSnippetID(),num);
                            avi.setVisibility(View.VISIBLE);
                            avi.show();
                        }

                    }
                }
            }
        });


        /*
            setting on refresh listener
         */
        prlSnippetView.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Snippet snippetClass=new Snippet(getApplicationContext());
                snippetClass.updateSnippet(snippet.getSnippetID(),origin);
            }
        });


        /*
            setting on click listener to likes count if origin is timeline and userID == selfUserID
         */
            tvLikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getApplicationContext(), UsersView.class);
                    i.putExtra("origin","Likes");
                    i.putExtra("snippetID",snippet.getSnippetID());
                    startActivity(i);
                }
            });
    }


    private void setProfileImage() {
        /*
        setting profile image from storage or getting from server
         */
        SharedPreferences preferences=getApplicationContext().getSharedPreferences("Profile",Context.MODE_PRIVATE);
        if(origin.equals("timeline")&&userID==selfUserID)
            loadImageFromStorage(preferences.getString("profilePic",null));
        else if(origin.equals("feed")){
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load("http://www.lifesnipp.com/profile_pic/"+userID+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(ivProfile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivProfile.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }

    private void fillList() {
        DbHelper dbHelper=new DbHelper(getApplicationContext());
        commentItems=dbHelper.getComments(snippet.getSnippetID());
        avi.setVisibility(View.GONE);
        rvComments.setVisibility(View.VISIBLE);
    }

    private void setUI() {
        tvPersonName.setText(snippet.getPersonName());
        tvCategory.setText("#"+snippet.getCategory());
        tvTitle.setText(snippet.getTitle());
        tvContent.setText(snippet.getContent());
        tvLikes.setText(snippet.getLikes());
        tvComments.setText(snippet.getComments()+" Comments");
        tvViews.setText(snippet.getViews());
        tvAbout.setText(snippet.getUserInfo());
        if(snippet.isLiked()>0)
            heart.setChecked(true);
        else
            heart.setChecked(false);
    }

    public void onEvent(CommentEvent commentEvent){
        /*
            after comments data received from server
         */
        boolean isDataReceived=commentEvent.isDataReceived();
        if(isDataReceived){
            DbHelper dbHelper=new DbHelper(getApplicationContext());
            commentItems=dbHelper.getComments(snippet.getSnippetID());
            commentAdapter.update(commentItems);
            tvComments.setText(snippet.getComments()+" Comments");
            avi.setVisibility(View.GONE);
            avi.hide();
            loading=true;
            num++;
            Log.d("num",num+"");
        }

        /*
            after commenting
         */
        boolean isCommented=commentEvent.isCommented();
        if(isCommented){
            etComment.setText("");
            pbSendComment.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(),"Commented",Toast.LENGTH_SHORT).show();
            Snippet snippetClass=new Snippet(getApplicationContext());
            snippetClass.getComments(snippet.getSnippetID(),0);
        }
    }

    public void onEvent(FeedEvent feedEvent){
        /*
            after refresh
         */
        boolean isDataReceived=feedEvent.isReceivedData();
        if(isDataReceived){
            DbHelper helper=new DbHelper(getApplicationContext());
            snippet=helper.getSnippet(snippet.getSnippetID(),origin);
            setUI();
            prlSnippetView.setRefreshing(false);
        }
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()){
            case R.id.cvProfileSnippetView:
                /*
                    starting profile view activity
                 */
                Intent i=new Intent(this, ProfileView.class);
                i.putExtra("userID",snippet.getUserID());
                startActivity(i);
                break;

            case R.id.sb_snippet_heart_SnippetView:
                /*
                    like
                 */
                int like;
                boolean checked=heart.isChecked();

                if(checked) {
                    like = 1;
                }
                else {
                    like = 0;
                }
                DbHelper dbHelper=new DbHelper(heart.getContext());

                if(origin.equals("feed"))
                    dbHelper.likeFeedSnippet(snippet.getSnippetID(),like);
                else if(origin.equals("timeline"))
                    dbHelper.likeTimelineSnippet(snippet.getSnippetID(),like);

                snippet.setIsLiked(like);

                Snippet snippetClass=new Snippet(getApplicationContext());
                snippetClass.like(snippet.getSnippetID(),like);

                /*
                    updating like count this way so that it reflects in feed fragment too
                 */
                snippetClass=new Snippet(getApplicationContext());
                snippetClass.updateSnippet(snippet.getSnippetID(),origin);

                break;

            case R.id.tvCommentsSnippetView:
                /*
                    getting comments
                 */
                if(cvComments.getVisibility()==View.GONE) {
                    cvComments.setVisibility(View.VISIBLE);
                    /*
                        scrolling to the bottom
                     */
                    nsv.post(new Runnable() {
                        @Override
                        public void run() {
                            rvComments.requestFocus();
                        }
                    });
                    avi.show();
                    snippetClass=new Snippet(getApplicationContext());
                    Log.d("comments",snippet.getSnippetID()+" "+num);
                    snippetClass.getComments(snippet.getSnippetID(),num);
                }else
                    cvComments.setVisibility(View.GONE);
                fillList();
                break;

            case R.id.bSendComment:
                /*
                    sending comment
                 */
                if(!etComment.getText().toString().trim().equals("")){
                    pbSendComment.setVisibility(View.VISIBLE);
                    String comment=etComment.getText().toString();
                    snippetClass=new Snippet(getApplicationContext());
                    snippetClass.comment(snippet.getSnippetID(),comment);
                }else{
                    Toast.makeText(getApplicationContext(),"Comment cannot be blank",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void loadImageFromStorage(String path)
    {
        try {
            File f = new File(path, "profilePic.jpg");
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load(f).asBitmap().centerCrop().error(defaultProfPic).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(new BitmapImageViewTarget(ivProfile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivProfile.setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onEvent(FetchFailed fetchFailed){
        if(fetchFailed.isRefreshFailed()){
            prlSnippetView.setRefreshing(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) // Press Back Icon
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
