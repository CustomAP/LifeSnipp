package com.custombuilder.ashishpawar.lifesnipp;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Ashish Pawar on 3/12/2017.
 */

public class NonScrollableViewPagerAdapter extends ViewPager {
    public NonScrollableViewPagerAdapter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NonScrollableViewPagerAdapter(Context context) {
        super(context);
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }
}
