package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/7/17.
 */

public class FollowEvent {
    boolean followed,unfollowed;

    public boolean isFollowed() {
        return followed;
    }

    public boolean isUnfollowed() {
        return unfollowed;
    }

    public FollowEvent(boolean followed, boolean unfollowed) {

        this.followed = followed;
        this.unfollowed = unfollowed;
    }
}
