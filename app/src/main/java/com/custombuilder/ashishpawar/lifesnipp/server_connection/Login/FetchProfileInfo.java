package com.custombuilder.ashishpawar.lifesnipp.server_connection.Login;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FollowEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ProfileEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ProfilePicEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/6/17.
 */

public class FetchProfileInfo {
    private Context context;
    private String userName,password,emailId, bio,firstName,lastName,birthDate,sessionIDString;
    private int followingCount,followersCount,userID,likes,views;
    private AQuery aQuery;
    String origin;

    /*
        links
     */
    private String getSelfProfileInfo ="http://www.lifesnipp.com/index.php/User_profile/getSelfProfileInfo";
    private String getOthersProfileInfo="http://www.lifesnipp.com/index.php/User_profile/getOthersProfileInfo";
    private String follow="http://www.lifesnipp.com/index.php/User_profile/follow";
    private String unfollow="http://www.lifesnipp.com/index.php/User_profile/unfollow";
    private String changeProfPic="http://www.lifesnipp.com/index.php/User_profile/changeProfPic";

    public FetchProfileInfo(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        SessionID sessionID=new SessionID(context);
        sessionIDString=sessionID.getSessionID();
    };

    public void getSelfProfileInfo(String userName,String password,String origin){
        this.userName=userName;
        this.password=password;
        this.origin=origin;

        Map<String,Object> sessionParams=new HashMap<>();
        sessionParams.put("sessionID",sessionIDString);

        aQuery.ajax(getSelfProfileInfo,sessionParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(object!=null){
                    try {
                        Log.d("getSelfProfileInfo","object!=null");

                        String profileInfo=object.getString("ProfileInfo");

                        emailId=object.getString("emailID");
                        userID=object.getInt("userID");

                        JSONObject obj=new JSONObject(profileInfo);

                        JSONObject reader=obj.getJSONObject("0");

                        firstName=reader.getString("firstName");
                        lastName=reader.getString("lastName");
                        bio =reader.getString("bio");
                        birthDate=reader.getString("birthdate");
                        followingCount=reader.getInt("following");
                        followersCount=reader.getInt("followers");
                        likes=reader.getInt("likes");
                        views=reader.getInt("views");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    EventBus.getDefault().post(new FetchFailed(true));
                    Log.d("getSelfProfileInfo","object=null");
                    Toast.makeText(context,"Failed to fetch profile info",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    /*
                        saving profile info
                     */
                    saveProfileInfo();
                }
            }
        });
    }

    private void saveProfileInfo() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                /*
                    saving in SP
                 */
                SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putInt("userID",userID);
                editor.putString("firstName",firstName);
                editor.putString("lastName",lastName);
                editor.putString("userName",userName);
                editor.putString("password",password);
                editor.putString("email",emailId);
                editor.putString("bio", bio);
                editor.putString("birthDate",birthDate);
                editor.putInt("followingCount",followingCount);
                editor.putInt("followersCount",followersCount);
                editor.putInt("views",views);
                editor.putInt("likes",likes);
                editor.apply();

                Log.d("userID",userID+"");
            }
        }).start();


        /*
            if origin in login get profile pic
         */
        if(origin.equals("Login")) {
            new AsyncTask<Void,Void,Void>() {
                Bitmap profPicBitmap=null;

                @Override
                protected Void doInBackground(Void... params) {
                    try{
                        profPicBitmap = Glide.with(context).load("http://www.lifesnipp.com/profile_pic/" + userID + ".JPG").asBitmap().into(-1, -1).get();
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    return null;
                }

                /*
                    saving profile pic locally
                 */
                @Override
                protected void onPostExecute(Void aVoid) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(profPicBitmap!=null){
                                ContextWrapper cw = new ContextWrapper(context);
                                File directory = cw.getDir("Profile", Context.MODE_PRIVATE);
                                File mypath = new File(directory, "profilePic.jpg");

                                FileOutputStream fos = null;
                                try {
                                    fos = new FileOutputStream(mypath);
                                    profPicBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        if (fos != null)
                                            fos.close();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                Log.d("path", directory.getAbsolutePath());
                                String path = directory.getAbsolutePath();

                                SharedPreferences preferences = context.getSharedPreferences("Profile", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("profilePic", path);
                                editor.apply();
                            }
                        }
                    }).start();

                    /*
                        getting timeline snippets
                     */
                    FetchTimelineSnippets fetchTimelineSnippets = new FetchTimelineSnippets(context);
                    fetchTimelineSnippets.getTimelineSnippets(userID, -1,"Login");
                }
            }.execute();


        }else if(origin.equals("ProfileFragment")){
            EventBus.getDefault().post(new ProfileEvent(firstName,lastName,userName, bio,birthDate,followersCount,followingCount,0,views,likes));
        }


    }



    public void getOthersProfileInfo(final int userID){

        Map<String,Object> profileParams=new HashMap<>();
        profileParams.put("sessionID",sessionIDString);
        profileParams.put("userID",userID);

        aQuery.ajax(getOthersProfileInfo,profileParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String firstName=null,lastName=null,bio=null,birthDate=null,userName=null;
                int followingCount=0,followersCount=0, follow=-1,views=0,likes=0;
                if(object!=null){
                    try{
                        Log.d("getOthersProfileInfo","object!=null");
                        String profileInfo=object.getString("ProfileInfo");
                        follow=object.getInt("follow");

                        JSONObject obj=new JSONObject(profileInfo);

                        JSONObject reader=obj.getJSONObject("0");

                        firstName=reader.getString("firstName");
                        lastName=reader.getString("lastName");
                        bio=reader.getString("bio");
                        birthDate=reader.getString("birthdate");
                        userName=reader.getString("userName");
                        followingCount=reader.getInt("following");
                        followersCount=reader.getInt("followers");
                        views=reader.getInt("views");
                        likes=reader.getInt("likes");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    EventBus.getDefault().post(new FetchFailed(true));
                    Log.d("getOthersProfileInfo","object=null");
                    Toast.makeText(context,"Server error try again later",Toast.LENGTH_SHORT).show();
                }
                if(status.getCode()==200 &&status.getCode()<300){
                    EventBus.getDefault().post(new ProfileEvent(firstName,lastName,userName,bio,birthDate,followersCount,followingCount,follow,views,likes));
                }
            }
        });
    }

    public void followUnfollow(final int userID, final boolean isFollowing, final String origin){
        String url=null;
        if(isFollowing){
            url=unfollow;
        }else{
            url=follow;
        }
        Map<String,Object> followParams=new HashMap<>();
        followParams.put("sessionID",sessionIDString);
        followParams.put("userID",userID);

        aQuery.ajax(url,followParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("followUnfollow","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("followUnfollow","object=null");
                    Toast.makeText(context,"Server error try again later",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200&&status.getCode()<300){
                    if(code==0) {
                        if(origin.equals("Users")){
                            DbHelper dbHelper=new DbHelper(context);
                            dbHelper.followUnfollow(userID,isFollowing?0:1);
                        }

                        if (isFollowing)
                            EventBus.getDefault().post(new FollowEvent(false, true));
                        else
                            EventBus.getDefault().post(new FollowEvent(true, false));
                    }else{
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void changeProfilePic(String image){

        Map<String,Object> profilePicParams=new HashMap<>();
        profilePicParams.put("image",image);
        profilePicParams.put("sessionID",sessionIDString);

        aQuery.ajax(changeProfPic,profilePicParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("changeProfPic","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    Log.d("changeProfPic","object=null");
                    Toast.makeText(context,"Server error try again later",Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new ProfilePicEvent(false));
                }

                if(status.getCode()==200&&status.getCode()<300){
                    if(code==0){
                        EventBus.getDefault().post(new ProfilePicEvent(true));
                    }
                }
            }
        });

    }

}
