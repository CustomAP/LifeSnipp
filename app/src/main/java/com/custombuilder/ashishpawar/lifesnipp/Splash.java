package com.custombuilder.ashishpawar.lifesnipp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 7/6/17.
 */

public class Splash extends Activity {
    Context context;
    private final int REQUEST_CODE_INTRO=1;
    int isFinished=0;
    Boolean isLoggedIn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        runnable.start();
    }
    Thread runnable =new Thread(){
        public void run(){
            try {
                sleep(500);

                /*
                    removing old lowerlimit set for feed
                 */
                SharedPreferences preferences=getSharedPreferences("AppInfo",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.remove("lowerLimit");
                editor.apply();

                DbHelper helper=new DbHelper(getApplicationContext());
                helper.clearFeed();
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                SharedPreferences preferences=getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
                isLoggedIn=preferences.getBoolean("isLoggedIn",false);
                Intent i;
                if(isLoggedIn) {
                    //fetch feed
                    //fetch notifications
                    i = new Intent("com.custombuilder.ashishpawar.lifesnipp.MAINACTIVITY");
                    startActivity(i);

                }else{
                    i = new Intent("com.custombuilder.ashishpawar.lifesnipp.APPINTRO");
                    startActivityForResult(i,REQUEST_CODE_INTRO);
                }

            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        if(isFinished==1||isLoggedIn)
        finish();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_INTRO) {
            if (resultCode == RESULT_OK) {
                // Finished the intro
                isFinished=1;
                Intent i=new Intent(this,Login.class);
                startActivity(i);
            } else {

            }
        }
    }
}
