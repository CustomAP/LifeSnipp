package com.custombuilder.ashishpawar.lifesnipp;

/**
 * Created by Ashish Pawar on 3/12/2017.
 */

public class UIData {

    //Tab Names(Main Activity)
    private final String[] MAIN_ACTIVITY_TAB_NAMES = new String[]{
            "Notifications",
            "LifeSnipp",
            "Profile"
    };

    //Tab Names(Home Fragment)
    private final String[] HOME_FRAGMENT_TAB_NAMES = new String[]{
            "FEED",
            "DIARY"
    };

    //Tab Icons
    private final int[] ICONS_NORMAL = new int[]{
            R.mipmap.notifications_icon,
            R.mipmap.home_icon,
            R.mipmap.profile_icon
    };
    private final int[] ICONS_SELECTED = new int[]{
            R.mipmap.notifications_selected,
            R.mipmap.home_selected,
            R.mipmap.profile_selected
    };

    public String getMainActivityTabName(int i) {
        return MAIN_ACTIVITY_TAB_NAMES[i];
    }

    public String getHomeFragmentTabName(int i) {
        return HOME_FRAGMENT_TAB_NAMES[i];
    }

    public int mainActivityTabsCount() {
        return MAIN_ACTIVITY_TAB_NAMES.length;
    }

    public int homeFragmentTabsCount() {
        return HOME_FRAGMENT_TAB_NAMES.length;
    }

    public int getIconNormal(int i) {
        return ICONS_NORMAL[i];
    }

    public int getIconSelected(int i) {
        return ICONS_SELECTED[i];
    }
}
