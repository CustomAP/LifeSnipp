package com.custombuilder.ashishpawar.lifesnipp;

        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.database.sqlite.SQLiteDatabase;
        import android.net.ConnectivityManager;
        import android.os.Bundle;
        import android.support.annotation.NonNull;
        import android.support.v7.app.AppCompatActivity;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.util.Log;
        import android.view.View;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.Toast;

        import com.androidquery.AQuery;
        import com.androidquery.callback.AjaxCallback;
        import com.androidquery.callback.AjaxStatus;
        import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
        import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
        import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.UserNewDeviceInfo;
        import com.custombuilder.ashishpawar.lifesnipp.server_connection.SignIn.UserDeviceInfo;
        import com.dd.processbutton.iml.ActionProcessButton;
        import com.google.android.gms.auth.api.Auth;
        import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
        import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
        import com.google.android.gms.auth.api.signin.GoogleSignInResult;
        import com.google.android.gms.common.ConnectionResult;
        import com.google.android.gms.common.SignInButton;
        import com.google.android.gms.common.api.GoogleApiClient;
        import com.google.android.gms.common.api.OptionalPendingResult;
        import com.google.android.gms.common.api.ResultCallback;
        import com.google.android.gms.common.api.Status;
        import com.rengwuxian.materialedittext.MaterialEditText;
        import com.wang.avi.AVLoadingIndicatorView;

        import org.json.JSONObject;

        import java.util.HashMap;
        import java.util.Map;

        import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 7/6/17.
 */

public class Login extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 420;

    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;

    private SignInButton bGSignIn;

    private MaterialEditText etUserNameLogin,etPasswordLogin,etUserNameSignIn,etPasswordSignIn,etRetypePwSignIn,etEmailId,etFirstName,etLastName;
    ActionProcessButton bLogin,bSignIn;
    LinearLayout llSignIn,llLogin;
    RelativeLayout rlLogin;
    AVLoadingIndicatorView aviLogin;

    private String userName,emailID,firstName,lastName,password,reEnterPassword,loginUserName,loginPassword,personName,email,familyName;

    public static Boolean isLoggedInToKill=false;

    public static boolean isUserNameAvailable=false;

    /*
        links
     */
    String isUserNameAvailableLink="http://www.lifesnipp.com/index.php/User_login/isUserNameAvailable";
    String isAlreadySignedIn="http://www.lifesnipp.com/index.php/User_login/isAlreadySignedIn";

    AQuery aQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        try{
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
        initializeControls();
        initializeGPlusSettings();
    }

    private void initializeControls(){
        /*
            initializing views
         */
        bGSignIn = (SignInButton) findViewById(R.id.bGsignIn);
        etUserNameLogin=(MaterialEditText)findViewById(R.id.etUserNameLogin);
        etPasswordLogin=(MaterialEditText)findViewById(R.id.etPasswordLogin);
        etUserNameSignIn=(MaterialEditText)findViewById(R.id.etUserName);
        etPasswordSignIn=(MaterialEditText)findViewById(R.id.etPassword);
        etRetypePwSignIn=(MaterialEditText)findViewById(R.id.etReenterPw);
        etEmailId=(MaterialEditText) findViewById(R.id.etEmailId);
        bLogin=(ActionProcessButton)findViewById(R.id.bLogin);
        bSignIn=(ActionProcessButton)findViewById(R.id.bSignIn);
        llSignIn=(LinearLayout)findViewById(R.id.llGSignIn);
        llLogin=(LinearLayout)findViewById(R.id.llLogin);
        rlLogin=(RelativeLayout)findViewById(R.id.rlLogin);
        etFirstName=(MaterialEditText)findViewById(R.id.etFirstName);
        etLastName=(MaterialEditText)findViewById(R.id.etLastName);
        aviLogin=(AVLoadingIndicatorView)findViewById(R.id.aviLogin);

        etRetypePwSignIn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                /*
                    checking password match on text changed
                 */
                password=etPasswordSignIn.getText().toString();
                reEnterPassword=etRetypePwSignIn.getText().toString();

                if(!password.equals(reEnterPassword))
                    etRetypePwSignIn.setError("Password does not match");
            }
        });

        aQuery=new AQuery(this);

        etUserNameSignIn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                /*
                    checking if userName is available
                 */
                Map<String,Object> userNameParams=new HashMap<>();
                userNameParams.put("userName",etUserNameSignIn.getText().toString().trim());

                aQuery.ajax(isUserNameAvailableLink,userNameParams, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        if(object!=null){
                            Log.d("isUserNameAvailable","object!=null");
                            try{
                                int available=object.getInt("ResultSet");
                                if(available==1)
                                    isUserNameAvailable=false;
                                else if(available==0)
                                    isUserNameAvailable=true;
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(!isUserNameAvailable) {
                                                Log.d("userName","if");
                                                etUserNameSignIn.setError("Username not available");
                                            }else {
                                                Log.d("userName","else");
                                                etUserNameSignIn.setError(null);
                                            }
                                        }
                                    });
                                }
                            }).start();
                        }else{
                            Log.d("isUserNameAvailable","object=null");
                            Toast.makeText(getApplicationContext(),"Network error",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        /*
            removing focus
         */
        etEmailId.setClickable(false);
        etEmailId.setFocusable(false);

        /*
            setting on click listeners
         */
        bGSignIn.setOnClickListener(this);
        bLogin.setOnClickListener(this);
        bSignIn.setOnClickListener(this);
        rlLogin.setOnClickListener(this);
    }

    private void initializeGPlusSettings(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        /*
            Customizing G+ button
          */
        bGSignIn.setSize(SignInButton.SIZE_STANDARD);
        bGSignIn.setScopes(gso.getScopeArray());
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    private void handleGPlusSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            /*
                Signed in successfully, show authenticated UI.
             */
            GoogleSignInAccount acct = result.getSignInAccount();

            /*
                Fetch values
             */
            personName = acct.getDisplayName();
            email = acct.getEmail();
            familyName = acct.getFamilyName();

            Log.e(TAG, "Name: " + personName +
                    ", email: " + email +
                    ", Family Name: " + familyName);



            llLogin.setVisibility(View.GONE);
            llSignIn.setVisibility(View.VISIBLE);

            try {
                if (personName.contains(" "))
                    personName = personName.substring(0, personName.indexOf(" "));
            }catch (Exception e){
                e.printStackTrace();
            }
            emailID=email;


            /*
                checking if email id is linked with other account
             */
            Map<String,Object> signInParams=new HashMap<>();
            signInParams.put("emailID",emailID);

            aQuery.ajax(isAlreadySignedIn,signInParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    if(object!=null) {
                        Log.d("isAlreadySignedIn","object!=null");
                        try {
                            final int isAlreadySignedIn = object.getInt("ResultSet");

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(isAlreadySignedIn==1){
                                                Toast.makeText(getApplicationContext(),"Email ID already linked with an account.\nLogin in to continue",Toast.LENGTH_SHORT).show();
                                                signOut();
                                                llLogin.setVisibility(View.VISIBLE);
                                                llSignIn.setVisibility(View.GONE);
                                            }else if(isAlreadySignedIn==0){
                                                etFirstName.setText(personName);
                                                etLastName.setText(familyName);
                                                etEmailId.setText(email);
                                            }
                                        }
                                    });
                                }
                            }).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Log.d("isAlreadySignedIn","object=null");
                    }
                }
            });


        } else {
            // Signed out, show unauthenticated UI.
           // updateUI(false);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.bGsignIn:
                /*
                    sign in
                 */
                signIn();
                break;
            case R.id.bLogin:
                /*
                    login
                 */
                SQLiteDatabase sqLiteDatabase=new DbHelper(this).getWritableDatabase();
                getLoginUIData();
                break;
            case R.id.rlLogin:
                /*
                    setting visibility
                 */
                llSignIn.setVisibility(View.GONE);
                llLogin.setVisibility(View.VISIBLE);
                break;
            case R.id.bSignIn:
                /*
                    initializing db and getting signin ui data
                 */
                sqLiteDatabase=new DbHelper(this).getWritableDatabase();
                getSignInUIData();
                break;

        }
    }

    private void getLoginUIData() {
        /*
            getting values
         */
        loginUserName=etUserNameLogin.getText().toString().trim();
        loginPassword=etPasswordLogin.getText().toString();

        if(loginUserName.length()>=4&&!loginPassword.equals("")) {
            /*
                network check
            */
            if (!isNetworkConnected())
                Toast.makeText(this, "Please check your network connection", Toast.LENGTH_SHORT).show();
            else {
                /*
                   getting user new device info
                 */
                aviLogin.setVisibility(View.VISIBLE);
                aviLogin.show();
                Toast.makeText(this,"Logging in Please wait",Toast.LENGTH_SHORT).show();
                UserNewDeviceInfo userNewDeviceInfo = new UserNewDeviceInfo(this, loginUserName, loginPassword);
                userNewDeviceInfo.InsertInfo();
            }
        }else if(loginUserName.length()<4){
            Toast.makeText(this,"Username should be of minimum 4 characters",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"Password cannot be blank",Toast.LENGTH_SHORT).show();
        }
    }

    private void getSignInUIData() {
        userName=etUserNameSignIn.getText().toString().trim();
        password=etPasswordSignIn.getText().toString();
        firstName=etFirstName.getText().toString().trim();
        lastName=etLastName.getText().toString().trim();
        reEnterPassword=etRetypePwSignIn.getText().toString();

        if(!firstName.equals("")) {
            if (!lastName.equals("")) {
                if(userName.length()>=4) {
                    //check password
                    if (password.equals(reEnterPassword)) {

                        if (password.length() >= 6) {
                            //check connection
                            if (!isNetworkConnected())
                                Toast.makeText(this, "Please check your network connection", Toast.LENGTH_SHORT).show();
                            else {
                                if(isUserNameAvailable) {
                                    aviLogin.setVisibility(View.VISIBLE);
                                    aviLogin.show();
                                    Toast.makeText(this, "Signing in Please wait", Toast.LENGTH_SHORT).show();
                                    //add entries to the server in the device info class
                                    UserDeviceInfo userDeviceInfo = new UserDeviceInfo(this, emailID, userName, password, firstName, lastName);
                                    userDeviceInfo.insertInfo();
                                }else{
                                    aviLogin.hide();
                                    aviLogin.setVisibility(View.GONE);
                                    Toast.makeText(this,"Username not available",Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(this, "Password should be of minimum 6 characters", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Password does not match", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this,"Username should be of minimum 4 characters",Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Last name cannot be blank", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this,"First name cannot be blank",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*
            Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
         */
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGPlusSignInResult(result);
            int statusCode = result.getStatus().getStatusCode();
            Log.d("status code",statusCode+"");
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            /*
                If the user's cached credentials are valid, the OptionalPendingResult will be "done"
             and the GoogleSignInResult will be available instantly.
             */
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleGPlusSignInResult(result);
        } else {
            /*
                If the user has not previously signed in on this device or the sign-in has expired,
             this asynchronous branch will attempt to sign in the user silently.  Cross-device
             single sign-on will occur in this branch.
             */
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleGPlusSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
         An unresolvable error has occurred and Google APIs (including Sign-In) will not
         be available.
        */
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }

        try {
            mProgressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(isLoggedInToKill)
            finish();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    public void onEvent(FetchFailed fetchFailed){
        if(fetchFailed.isRefreshFailed()){
            aviLogin.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            EventBus.getDefault().unregister(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
