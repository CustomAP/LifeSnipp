package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 23/6/17.
 */

public class ProfileEvent {
    private String firstName,lastName,userName, bio,birthdate;
    private int followers;
    private int following,likes,views;

    public int getFollow() {
        return follow;
    }

    private int follow;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getFollowers() {
        return followers;
    }

    public int getFollowing() {
        return following;
    }

    public String getUserName() {
        return userName;
    }

    public String getBio() {
        return bio;
    }


    public String getBirthdate() {
        return birthdate;
    }

    public int getLikes() {
        return likes;
    }

    public int getViews() {
        return views;
    }

    public ProfileEvent(String firstName, String lastName, String userName, String bio, String birthdate, int followers, int following, int follow, int views, int likes) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.bio = bio;
        this.birthdate = birthdate;
        this.followers=followers;
        this.following=following;
        this.follow=follow;
        this.views=views;
        this.likes=likes;

    }
}
