package com.custombuilder.ashishpawar.lifesnipp.Home;

/**
 * Created by Ashish Pawar on 5/16/2017.
 */

public class DiarySnippetItem {
    private String content, title,category,date,time;
    private int diarySnippetID;

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public int getDiarySnippetID() {

        return diarySnippetID;
    }

    public DiarySnippetItem(String content, String title, String category, String date, String time, int diarySnippetID) {

        this.content = content;
        this.title = title;
        this.category = category;
        this.date = date;
        this.time = time;
        this.diarySnippetID = diarySnippetID;
    }
}
