package com.custombuilder.ashishpawar.lifesnipp.server_connection;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ShareEvent;
import com.custombuilder.ashishpawar.lifesnipp.Home.DiarySnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 22/6/17.
 */

public class DiarySnippet {

    private Context context;
    private AQuery aQuery;
    private String sessionIDString;

    /*
        links
     */
    private String addToDiary="http://www.lifesnipp.com/index.php/Diary/addToDiary";
    private String editDiarySnippet="http://www.lifesnipp.com/index.php/Diary/editDiarySnippet";
    private String deleteDiarySnippet="http://www.lifesnipp.com/index.php/Diary/deleteDiarySnippet";

    public DiarySnippet(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        SessionID sessionID=new SessionID(context);
        sessionIDString=sessionID.getSessionID();
    }

    public void addToDiary(final String title, final String category, final String content){

        Map<String,Object> diaryParams=new HashMap<>();
        diaryParams.put("sessionID",sessionIDString);
        diaryParams.put("title",title);
        diaryParams.put("category",category);
        diaryParams.put("content",content);

        aQuery.ajax(addToDiary,diaryParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    Log.d("addToDiary","object!=null");
                    try{
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("addToDiary","object=null");
                    Toast.makeText(context,"Server error try again later",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 && status.getCode()<300){
                    if(code!=0){
                        try{
                            /*
                                getting date and time from server
                             */
                            String date=object.getString("date");
                            String time=object.getString("time");

                            DiarySnippetItem diarySnippetItem=new DiarySnippetItem(content,title,category,date,time,code);

                            /*
                                adding diary snippet to db
                             */
                            DbHelper helper=new DbHelper(context);
                            helper.addDiarySnippets(diarySnippetItem);

                            EventBus.getDefault().post(new ShareEvent(false,true));

                            Toast.makeText(context,"Added to diary",Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(context,"Failed to add snippet to diary",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    public void editDiarySnippet(final int snippetID, final String title, final String category, final String content){
        Map<String,Object> editParams=new HashMap<>();
        editParams.put("sessionID",sessionIDString);
        editParams.put("snippetID",snippetID);
        editParams.put("title",title);
        editParams.put("category",category);
        editParams.put("content",content);
        Log.d("editDiary",sessionIDString+" "+snippetID+" "+title+" "+category+" "+content);

        aQuery.ajax(editDiarySnippet,editParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("editDiarySnippet","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("status",status.getCode()+"");
                    Log.d("editDiarySnippet","object=null");
                    Toast.makeText(context,"Server error try again later",Toast.LENGTH_SHORT).show();
                }

                if (status.getCode() == 200 && status.getCode() < 300) {
                    if(code!=0){
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    }else{
                        /*
                            editing diary snippet in local Db
                         */
                        DbHelper helper=new DbHelper(context);
                        helper.editDiarySnippet(snippetID,title,category,content);
                        EventBus.getDefault().post(new ShareEvent(false,true));
                    }
                }
            }
        });
    }

    public void deleteDiarySnippet(final int snippetID){
        Map<String,Object> deleteParams=new HashMap<>();
        deleteParams.put("sessionID",sessionIDString);
        deleteParams.put("snippetID",snippetID);

        aQuery.ajax(deleteDiarySnippet,deleteParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("deleteDiarySnippet","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("deleteDiarySnippet","object=null");
                    Toast.makeText(context,"Server error try again later",Toast.LENGTH_SHORT).show();
                }
                if(status.getCode()==200&&status.getCode()<300){
                    if(code!=0)
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    else{
                        /*
                            deleting diary snippet from local Db
                         */
                        DbHelper helper=new DbHelper(context);
                        helper.deleteDiarySnippet(snippetID);
                        EventBus.getDefault().post(new ShareEvent(false,true));
                    }
                }
            }
        });
    }
}
