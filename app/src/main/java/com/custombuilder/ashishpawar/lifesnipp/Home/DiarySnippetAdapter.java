package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.DiarySnippet;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Snippet;
import com.yarolegovich.lovelydialog.LovelyChoiceDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Ashish Pawar on 5/16/2017.
 */

public class DiarySnippetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<DiarySnippetItem> diarySnippetItems;

    public DiarySnippetAdapter(ArrayList<DiarySnippetItem> diarySnippetItems) {
        this.diarySnippetItems = diarySnippetItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 0) {
            View view = inflater.inflate(R.layout.diary_snippet_single_item, parent, false);
            viewHolder = new DiarySnippetHolder(view);
        } else if (viewType == 1) {

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == 0) {
            final DiarySnippetHolder diarySnippetHolder = (DiarySnippetHolder) holder;

            /*
                setting text in views
             */
            diarySnippetHolder.tvTitle.setText(diarySnippetItems.get(position).getTitle());
            diarySnippetHolder.content.setText(diarySnippetItems.get(position).getContent());
            diarySnippetHolder.category.setText("#"+diarySnippetItems.get(position).getCategory());
            diarySnippetHolder.date.setText(convertDate(diarySnippetItems.get(position).getDate(),2));

            /*
                counting lines in snippet.
                if lines count < 7 then hiding read more.. text
             */
            diarySnippetHolder.content.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount=diarySnippetHolder.content.getLineCount();
                    if(lineCount<7)
                        diarySnippetHolder.readmore.setVisibility(View.GONE);
                }
            });

            /*
                setting on click listener to read more.. and read less..
             */
            diarySnippetHolder.readmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(diarySnippetHolder.readmore.getText().toString().equals(diarySnippetHolder.content.getContext().getString(R.string.read_more))) {
                        diarySnippetHolder.content.setMaxLines(Integer.MAX_VALUE);
                        diarySnippetHolder.readmore.setText(R.string.read_less);
                    }else if(diarySnippetHolder.readmore.getText().toString().equals(diarySnippetHolder.content.getContext().getString(R.string.read_less))){
                        diarySnippetHolder.content.setMaxLines(8);
                        diarySnippetHolder.readmore.setText(R.string.read_more);
                    }
                }
            });


            /*
                setting on long click listener to linear layout for edit, delete, share snippet options.
             */
            diarySnippetHolder.llDiarySnippet.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final String[] list = new String[]{"Edit", "Delete", "Share"};
                    new LovelyChoiceDialog(diarySnippetHolder.llDiarySnippet.getContext())
                            .setTopColorRes(R.color.colorPrimary)
                            .setTitle("Choose an action")
                            .setIcon(R.mipmap.action)
                            .setItems(list, new LovelyChoiceDialog.OnItemSelectedListener<String>() {
                                @Override
                                public void onItemSelected(int which, String item) {
                                    switch (which) {
                                        case 0:
                                            /*
                                                edit
                                             */
                                            Intent i=new Intent(diarySnippetHolder.category.getContext(),NewSnippet.class);
                                            i.putExtra("tab","diary");
                                            i.putExtra("type","edit");
                                            i.putExtra("snippetID",diarySnippetItems.get(position).getDiarySnippetID());
                                            i.putExtra("title",diarySnippetItems.get(position).getTitle());
                                            i.putExtra("category",diarySnippetItems.get(position).getCategory());
                                            i.putExtra("content",diarySnippetItems.get(position).getContent());
                                            i.putExtra("date",diarySnippetItems.get(position).getDate());
                                            diarySnippetHolder.content.getContext().startActivity(i);
                                            break;
                                        case 1:
                                            /*
                                                delete
                                             */
                                            new LovelyStandardDialog(diarySnippetHolder.llDiarySnippet.getContext())
                                                    .setTopColorRes(R.color.colorPrimary)
                                                    .setButtonsColorRes(R.color.colorAccent)
                                                    .setIcon(R.mipmap.info)
                                                    .setTitle("Delete Snippet?")
                                                    .setMessage("This cannot be undone")
                                                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            DiarySnippet diarySnippet=new DiarySnippet(diarySnippetHolder.category.getContext());
                                                            diarySnippet.deleteDiarySnippet(diarySnippetItems.get(position).getDiarySnippetID());
                                                        }
                                                    })
                                                    .setNegativeButton("Cancel", null)
                                                    .show();

                                            break;
                                        case 2:
                                            /*
                                                share
                                             */
                                            SharedPreferences preferences=diarySnippetHolder.category.getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                                            String[] items=new String[]{preferences.getString("firstName",null)+" "+preferences.getString("lastName",null),"Anonymous"};

                                            new LovelyChoiceDialog(diarySnippetHolder.llDiarySnippet.getContext())
                                                    .setTopColorRes(R.color.colorPrimary)
                                                    .setTitle("Share Snippet as")
                                                    .setIcon(R.mipmap.share)
                                                    .setItems(items, new LovelyChoiceDialog.OnItemSelectedListener<String>() {
                                                        @Override
                                                        public void onItemSelected(int which, String item) {
                                                            switch (which){
                                                                case 0:
                                                                    Snippet snippet=new Snippet(diarySnippetHolder.llDiarySnippet.getContext());
                                                                    snippet.shareSnippet(diarySnippetItems.get(position).getTitle(),diarySnippetItems.get(position).getCategory(),diarySnippetItems.get(position).getContent(),0);
                                                                    break;
                                                                case 1:
                                                                    snippet=new Snippet(diarySnippetHolder.llDiarySnippet.getContext());
                                                                    snippet.shareSnippet(diarySnippetItems.get(position).getTitle(),diarySnippetItems.get(position).getCategory(),diarySnippetItems.get(position).getContent(),1);
                                                                    break;
                                                            }
                                                        }
                                                    })
                                                    .show();
                                            break;
                                    }
                                }
                            })
                            .show();
                    return false;
               }
            });

        } else if (holder.getItemViewType() == 1) {

        }
    }


    @Override
    public int getItemCount() {
        return diarySnippetItems.size();
    }

    private class DiarySnippetHolder extends RecyclerView.ViewHolder {
        TextView category, content, readmore,date;
        LinearLayout llDiarySnippet;
        AutofitTextView tvTitle;
        CardView cvDiarySnippet;
        private DiarySnippetHolder(View itemView) {
            super(itemView);
            category = (TextView) itemView.findViewById(R.id.tvCategory_diarySnippet);
            content = (TextView) itemView.findViewById(R.id.tvSnippetContent_diarySnippet);
            tvTitle = (AutofitTextView) itemView.findViewById(R.id.tvHeading_diarySnippet);
            readmore=(TextView)itemView.findViewById(R.id.tvReadMore_diarySnippet);
            cvDiarySnippet=(CardView)itemView.findViewById(R.id.cvDiarySnippetItem);
            date=(TextView)itemView.findViewById(R.id.tvDateDiarySnippet);
            llDiarySnippet=(LinearLayout)itemView.findViewById(R.id.llDiarySnippet);
        }
    }

    private String convertDate(String time, int type) {
        String inputPattern = null;
        String outputPattern = null;
        if (type == 1) {
            inputPattern = "MMM dd";
            outputPattern = "yyyy-MM-dd";
        } else if (type == 2) {
            outputPattern = "MMM dd";
            inputPattern = "yyyy-MM-dd";
        }
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
