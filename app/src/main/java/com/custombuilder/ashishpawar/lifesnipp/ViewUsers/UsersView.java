package com.custombuilder.ashishpawar.lifesnipp.ViewUsers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 13/7/17.
 */

public class UsersView extends AppCompatActivity {
    RecyclerView rvUsers;
    ArrayList<UsersItem> usersItems;
    UsersAdapter usersAdapter;
    String origin;
    int snippetID=0,num=0,userID;
    AQuery aQuery;
    boolean loading=true;
    String sessionIDString;
    AVLoadingIndicatorView aviUsers;

    /*
        links
     */
    String getFollowers="http://www.lifesnipp.com/index.php/User_profile/getFollowers";
    String getFollowing="http://www.lifesnipp.com/index.php/User_profile/getFollowing";
    String getLikes="http://www.lifesnipp.com/index.php/Snippets/getLikes";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_users_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();

        getUserItems();

    }

    private void getUserItems() {
        if(origin.equals("Followers")||origin.equals("Following")){
            Log.d("num",num+" ");
            Map<String, Object> followParams = new HashMap<>();
            followParams.put("sessionID", sessionIDString);
            followParams.put("num", num);
            followParams.put("userID",userID);
            if(origin.equals("Followers")) {
                aQuery.ajax(getFollowers, followParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        int num = 0, userID = 0, isFollowing = 0;
                        String firstName = null, lastName = null, userName = null;
                        DbHelper helper=new DbHelper(getApplicationContext());

                        if (object != null) {
                            Log.d("getFollow", "object!=null");
                            try {
                                num = object.getInt("num");
                                Log.d("num received",num+" ");
                                String users = object.getString("users");
                                String isFollowingString = object.getString("isFollowing");

                                JSONObject userReader = new JSONObject(users);
                                JSONObject isFollowingReader = new JSONObject(isFollowingString);

                                for (int i = 0; i < num; i++) {
                                    JSONObject usersObj = userReader.getJSONObject(String.valueOf(i));
                                    JSONObject followObj = isFollowingReader.getJSONObject(String.valueOf(i));

                                    firstName = usersObj.getString("firstName");
                                    lastName = usersObj.getString("lastName");
                                    userName = usersObj.getString("userName");
                                    userID = usersObj.getInt("UserID");
                                    isFollowing = followObj.getInt("isFollowing");

                                    helper.addUsers(userID,firstName+" "+lastName,userName,isFollowing);
                                    Log.d("followers id ", "" + userID);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (status.getCode() == 200) {
                                /*
                                    updating recycler view and setting visibility of recyclerview and loading animation
                                 */
                                usersItems=helper.getUsers();
                                loading=true;
                                usersAdapter.update(usersItems);
                                aviUsers.setVisibility(View.GONE);
                                rvUsers.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Log.d("getFollow", "object=null");
                            Toast.makeText(getApplicationContext(), "Server error try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }else if(origin.equals("Following")){
                aQuery.ajax(getFollowing, followParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        int num = 0, userID = 0, isFollowing = 0;
                        String firstName = null, lastName = null, userName = null;
                        DbHelper helper=new DbHelper(getApplicationContext());

                        if (object != null) {
                            Log.d("getFollow", "object!=null");
                            try {
                                num = object.getInt("num");
                                String users = object.getString("users");
                                String isFollowingString = object.getString("isFollowing");

                                JSONObject userReader = new JSONObject(users);
                                JSONObject isFollowingReader = new JSONObject(isFollowingString);

                                for (int i = 0; i < num; i++) {
                                    JSONObject usersObj = userReader.getJSONObject(String.valueOf(i));
                                    JSONObject followObj = isFollowingReader.getJSONObject(String.valueOf(i));

                                    firstName = usersObj.getString("firstName");
                                    lastName = usersObj.getString("lastName");
                                    userName = usersObj.getString("userName");
                                    userID = usersObj.getInt("UserID");
                                    isFollowing = followObj.getInt("isFollowing");

                                    helper.addUsers(userID,firstName+" "+lastName,userName,isFollowing);
                                    Log.d("followers id ", "" + userID);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (status.getCode() == 200) {
                                /*
                                    updating recycler view and setting visibility of recyclerview and loading animation
                                 */
                                usersItems=helper.getUsers();
                                loading=true;
                                usersAdapter.update(usersItems);
                                aviUsers.setVisibility(View.GONE);
                                rvUsers.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Log.d("getFollow", "object=null");
                            Toast.makeText(getApplicationContext(), "Server error try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }


        }else if(origin.equals("Likes")){
            Map<String,Object> likeParams=new HashMap<>();
            likeParams.put("sessionID",sessionIDString);
            likeParams.put("snippetID",snippetID);
            likeParams.put("num", num);
            aQuery.ajax(getLikes,likeParams,JSONObject.class,new AjaxCallback<JSONObject>(){
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    int num = 0, userID = 0,isFollowing=0;
                    String firstName = null, lastName = null, userName = null;
                    DbHelper helper=new DbHelper(getApplicationContext());
                    if(object!=null){
                        Log.d("getLikedUsers","object!=null");
                        try{
                            num = object.getInt("num");
                            String users = object.getString("users");
                            String isFollowingString = object.getString("isFollowing");

                            JSONObject userReader = new JSONObject(users);
                            JSONObject isFollowingReader = new JSONObject(isFollowingString);

                            for (int i = 0; i < num; i++) {
                                JSONObject usersObj = userReader.getJSONObject(String.valueOf(i));
                                JSONObject followObj = isFollowingReader.getJSONObject(String.valueOf(i));


                                firstName = usersObj.getString("firstName");
                                lastName = usersObj.getString("lastName");
                                userName = usersObj.getString("userName");
                                userID = usersObj.getInt("UserID");
                                isFollowing = followObj.getInt("isFollowing");

                                helper.addUsers(userID,firstName+" "+lastName,userName,isFollowing);
                                Log.d("liked user id ", "" + userID);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else{
                        Log.d("getLikedUsers","object=null");
                        Toast.makeText(getApplicationContext(),"Server error try again later",Toast.LENGTH_SHORT).show();
                    }
                    if (status.getCode() == 200) {
                        /*
                              updating recycler view and setting visibility of recyclerview and loading animation
                          */
                        usersItems=helper.getUsers();
                        loading=true;
                        usersAdapter.update(usersItems);
                        aviUsers.setVisibility(View.GONE);
                        rvUsers.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    private void initialize() {

        /*
            getting origin and userID as extra
         */
        origin=getIntent().getStringExtra("origin");

        if(origin.equals("Following")||origin.equals("Followers"))
            userID=getIntent().getIntExtra("userID",0);

        /*
            getting snippetID if origin is likes
         */
        if(origin.equals("Likes"))
            snippetID=getIntent().getIntExtra("snippetID",0);

        /*
            setting action bar title
         */
        setActionBarTitle();

        /*
            initializing views
         */
        rvUsers =(RecyclerView)findViewById(R.id.rvUsers);
        aviUsers=(AVLoadingIndicatorView)findViewById(R.id.aviUsers);

        /*
            setting visibility of loading animation and recyclerview
         */
        rvUsers.setVisibility(View.GONE);
        aviUsers.setVisibility(View.VISIBLE);

        /*
            setting recycler view
         */
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        rvUsers.setLayoutManager(linearLayoutManager);
        rvUsers.setItemAnimator(new DefaultItemAnimator());

        /*
            setting adapter
         */
        usersItems =new ArrayList<>();
        usersAdapter =new UsersAdapter(usersItems);
        rvUsers.setAdapter(usersAdapter);

        aQuery=new AQuery(this);

        /*
            getting sessionID string
         */
        SessionID sessionID=new SessionID(this);
        sessionIDString=sessionID.getSessionID();

        /*
            adding onscroll listener
         */
        rvUsers.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (loading) {
                    loading = false;
                    Log.d("timeline", "Last Item Wow !");
                    num++;
                    /*
                        getting user items after scrolled
                     */
                    getUserItems();
                }
            }
        });

        /*
            clearing previous users data
         */
        DbHelper helper=new DbHelper(getApplicationContext());
        helper.deleteUsers();
    }

    private void setActionBarTitle() {
        /*
            setting action bar title to : Following/Followers/Likes
         */
        if(origin.equals("Following"))
            getSupportActionBar().setTitle(R.string.following);

        else if(origin.equals("Followers"))
            getSupportActionBar().setTitle(R.string.followers);

        else if(origin.equals("Likes"))
            getSupportActionBar().setTitle(R.string.likes);
    }

    @Override
    protected void onDestroy() {
        /*
            deleting users from db
         */
        super.onDestroy();
        Log.d("usersview","ondestroy");
        DbHelper helper=new DbHelper(getApplicationContext());
        helper.deleteUsers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) // Press Back Icon
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
