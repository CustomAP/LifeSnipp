package com.custombuilder.ashishpawar.lifesnipp.server_connection;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/6/17.
 */

public class Extras {
    Context context;
    String sessionIDString;
    AQuery aQuery;
    String getCategories="http://www.lifesnipp.com/index.php/Extras/getCategories";
    String checkUpdates="http://www.lifesnipp.com/index.php/Extras/checkUpdate";

    public Extras(Context context){
        this.context=context;
        SessionID sessionID=new SessionID(context);
        sessionIDString=sessionID.getSessionID();
        aQuery=new AQuery(context);
    }

    public void getCategories(){
        final Map<String,Object> categoriesParams=new HashMap<>();
        categoriesParams.put("sessionID",sessionIDString);

        aQuery.ajax(getCategories,categoriesParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int num;
                if(object!=null){
                    try{
                        Log.d("getCategories","object!=null");
                        num=object.getInt("num");

                        String category=object.getString("categories");

                        JSONObject reader=new JSONObject(category);

                        /*
                            saving categories in SP
                         */
                        SharedPreferences preferences=context.getSharedPreferences("Categories",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor=preferences.edit();
                        editor.putInt("num",num);
                        for(int i=0;i<num;i++){
                            Log.d("cat",reader.getString(String.valueOf(i)));
                            editor.putString(String.valueOf(i),reader.getString(String.valueOf(i)));
                        }
                        editor.apply();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getCategories","object=null");
                }
            }
        });
    }


    public void checkUpdates(){
        Map<String,Object> updateParams=new HashMap<>();
        updateParams.put("sessionID",sessionIDString);
        updateParams.put("appVersion",context.getResources().getString(R.string.app_version));

        aQuery.ajax(checkUpdates, updateParams,JSONObject.class,new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int toUpdate = 0, isMandatory = 0;
                if (object != null) {

                    try {
                        Log.d("checkUpdates", "object!=null");
                        toUpdate = object.getInt("toUpdate");
                        isMandatory = object.getInt("isMandatory");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("checkUpdate", "object=null");
                }
                if (status.getCode() == 200 && status.getCode() < 300) {
                    /*
                        storing update values in SP
                     */
                    SharedPreferences preferences=context.getSharedPreferences("AppInfo",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.remove("toUpdate");
                    editor.remove("isMandatory");
                    editor.putInt("toUpdate",toUpdate);
                    editor.putInt("isMandatory",isMandatory);
                    editor.apply();
                    Log.d("checkUpdate","toUpdate="+toUpdate+"Mandatory="+isMandatory);
                }
            }
        });
    }
}
