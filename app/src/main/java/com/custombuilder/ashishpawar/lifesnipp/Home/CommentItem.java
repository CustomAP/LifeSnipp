package com.custombuilder.ashishpawar.lifesnipp.Home;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/6/17.
 */

public class CommentItem {
    private String personName,comment,date;
    private int commentID;
    private int userID;

    public int getSnippetID() {
        return snippetID;
    }

    private int snippetID;

    public int getCommentID() {
        return commentID;
    }

    public int getUserID() {
        return userID;
    }

    public CommentItem(int commentID, String personName, String comment, String date, int userID,int snippetID) {
        this.personName = personName;
        this.comment = comment;
        this.date=date;
        this.commentID=commentID;
        this.userID=userID;
        this.snippetID=snippetID;

    }

    public String getPersonName() {
        return personName;
    }

    public String getComment() {
        return comment;
    }

    public String getDate() {
        return date;
    }
}
