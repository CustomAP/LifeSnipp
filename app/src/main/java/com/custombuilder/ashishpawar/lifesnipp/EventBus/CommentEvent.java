package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/6/17.
 */

public class CommentEvent {
    private boolean isDataReceived,isCommented;

    public boolean isCommented() {
        return isCommented;
    }

    public boolean isDataReceived() {
        return isDataReceived;
    }

    public CommentEvent(boolean isDataReceived,boolean isCommented) {
        this.isCommented=isCommented;
        this.isDataReceived = isDataReceived;
    }
}
