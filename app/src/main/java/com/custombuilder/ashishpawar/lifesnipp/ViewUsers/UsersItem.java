package com.custombuilder.ashishpawar.lifesnipp.ViewUsers;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 13/7/17.
 */

public class UsersItem {
    private String personName,userName;
    private int userID;

    public int getIsFollowing() {
        return isFollowing;
    }

    private int isFollowing;

    public String getPersonName() {
        return personName;
    }

    public String getUserName() {
        return userName;
    }

    public int getUserID() {
        return userID;
    }

    public UsersItem(String personName, String userName, int userID,int isFollowing) {

        this.personName = personName;
        this.userName = userName;
        this.userID = userID;
        this.isFollowing=isFollowing;
    }
}
