package com.custombuilder.ashishpawar.lifesnipp.Profile;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ProfilePicEvent;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Login.FetchProfileInfo;
import com.soundcloud.android.crop.Crop;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar on 5/13/2017.
 */

public class ProfilePicDialog extends Activity{
    ImageButton ibEditProfilePic;
    ImageView ivProfilePic;
    AVLoadingIndicatorView avi;
    Intent resultIntent;
    Bitmap imageBitmap;
    String origin;
    int userID;

    final int PICK_IMAGE=3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_pic);
        initialize();
    }

    private void initialize() {
        /*
            initialize views
         */
        ibEditProfilePic=(ImageButton)findViewById(R.id.ibEditProfilePic);
        ivProfilePic=(ImageView)findViewById(R.id.ivProfilePicActivity);
        avi=(AVLoadingIndicatorView)findViewById(R.id.aviProfilePic);

        /*
            getting extras
         */
        origin=getIntent().getStringExtra("origin");
        userID=getIntent().getIntExtra("userID",0);


        /*
            load image from storage if origin is profile fragment
         */
        if(origin.equals("ProfileFragment"))
        loadImageFromStorage();


        /*
            start activity for result to choose profile pic
         */
        if(origin.equals("ProfileFragment")) {
            ibEditProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

                }
            });
        }else if(origin.equals("ProfileView")){
            /*
                getting profile pic from server
             */
            ibEditProfilePic.setVisibility(View.GONE);
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load("http://www.lifesnipp.com/profile_pic/"+userID+".JPG").error(defaultProfPic).into(ivProfilePic);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
            after getting image
         */
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                beginCrop(data.getData());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(requestCode==Crop.REQUEST_CROP){
            /*
                start crop
             */
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, final Intent result) {
        if (resultCode == RESULT_OK) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        /*
                            after crop
                         */
                        InputStream iStream = getContentResolver().openInputStream(Crop.getOutput(result));
                        Bitmap image=BitmapFactory.decodeStream(iStream);

                        resultIntent=result;
                        imageBitmap=image;

                        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
                        image.compress(Bitmap.CompressFormat.JPEG,50,byteArrayOutputStream);
                        String encodedImage= Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);

                        FetchProfileInfo fetchProfileInfo=new FetchProfileInfo(getApplicationContext());
                        fetchProfileInfo.changeProfilePic(encodedImage);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                /*
                                    stop loading animation
                                 */
                                avi.setVisibility(View.VISIBLE);
                                avi.show();
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }).start();
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onEvent(ProfilePicEvent profilePicEvent){
        /*
            after image is uploaded
         */
        boolean isImageUploaded=profilePicEvent.isImageUploaded();
        avi.setVisibility(View.GONE);
        avi.hide();
        if(isImageUploaded){
            Toast.makeText(this,"Profile Pic updated successfully",Toast.LENGTH_SHORT).show();
            ivProfilePic.setImageURI(Crop.getOutput(resultIntent));
            saveToInternalStorage.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
            register event bus
         */
        try{
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*
            unregister event bus
         */
        try{
            EventBus.getDefault().unregister(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    Thread saveToInternalStorage=new Thread(){
        public void run() {
            /*
                begin saving to internal storage
             */
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("Profile", Context.MODE_PRIVATE);
            File mypath = new File(directory, "profilePic.jpg");

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fos != null)
                        fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Log.d("path", directory.getAbsolutePath());
            String path = directory.getAbsolutePath();

            /*
                saving profile pic path is SP
             */
            SharedPreferences preferences = getSharedPreferences("Profile", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("profilePic", path);
            editor.apply();
        }
    };


    public void loadImageFromStorage(){
        /*
            load image from storage using path saved in SP
         */
        SharedPreferences preferences=getSharedPreferences("Profile",Context.MODE_PRIVATE);
        String path=preferences.getString("profilePic",null);
        File f = new File(path, "profilePic.jpg");
        Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
        Glide.with(this).load(f).asBitmap().error(defaultProfPic).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(ivProfilePic);
    }
}
