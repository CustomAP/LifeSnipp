package com.custombuilder.ashishpawar.lifesnipp.Home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.custombuilder.ashishpawar.lifesnipp.EventBus.ShareEvent;
import com.custombuilder.ashishpawar.lifesnipp.R;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.DiarySnippet;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Snippet;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.Switch;

import java.util.Arrays;

import de.greenrobot.event.EventBus;
import info.hoang8f.widget.FButton;

/**
 * Created by Ashish Pawar on 3/29/2017.
 */

public class NewSnippet extends AppCompatActivity implements View.OnClickListener{
    private FButton save_share;
    private MaterialEditText etTitle, etContent;
    Spinner sCategory;
    CardView cvAnonymous;
    TextView tvSelectCategory;
    String tab,type,title,content,category,date;
    int snippetID;
    Switch switchAnonymous;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_snippet);

        initialize();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);//hide keyboard
        imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);//hide keyboard
        imm.hideSoftInputFromWindow(etContent.getWindowToken(), 0);
    }

    private void initialize() {
        /*
            initializing views
         */
        save_share = (FButton) findViewById(R.id.save_new_snippet);
        etTitle = (MaterialEditText) findViewById(R.id.heading_diary_content);
        etContent = (MaterialEditText) findViewById(R.id.snippet_diary_content);
        cvAnonymous = (CardView) findViewById(R.id.cvAnonymous);
        sCategory=(Spinner)findViewById(R.id.sCategory);
        tvSelectCategory=(TextView)findViewById(R.id.tvSelectCategory);
        switchAnonymous=(Switch)findViewById(R.id.switchAnonymous);

        /*
            adding categories in spinner
         */
        addCategoryItems();

        /*
            getting extras
         */
        tab = getIntent().getStringExtra("tab");
        type=getIntent().getStringExtra("type");


        /*
            setting text of button
         */
        if (!tab.isEmpty()) {
            if (tab.equals("feed"))
                save_share.setText(getResources().getString(R.string.share));
            else if (tab.equals("diary")&&type.equals("new"))
                save_share.setText(getResources().getString(R.string.add_to_diary));
            else if(tab.equals("diary")&&type.equals("edit"))
                save_share.setText(getResources().getString(R.string.save_changes));
            else if(tab.equals("timeline"))
                save_share.setText(getResources().getString(R.string.save_changes));
        }

        /*
            setting visibility of isAnonymous if tab is diary
         */
        if(tab.equals("diary"))
            cvAnonymous.setVisibility(View.GONE);


        /*
            if type is edit, getting extras and then setting text in views
          */
        if(!type.isEmpty()){
            if(type.equals("edit")){
                title=getIntent().getStringExtra("title");
                content=getIntent().getStringExtra("content");
                date=getIntent().getStringExtra("date");
                category=getIntent().getStringExtra("category");
                snippetID=getIntent().getIntExtra("snippetID",0);

                int anonymous=getIntent().getIntExtra("isAnonymous",0);
                boolean isAnonymous;
                if(anonymous==0)
                    isAnonymous=false;
                else
                    isAnonymous=true;
                switchAnonymous.setChecked(isAnonymous);

                etContent.setText(content);
                etTitle.setText(title);
            }
        }

        /*
            setting onclick listeners on select category spinner and share button
         */
        save_share.setOnClickListener(this);
        tvSelectCategory.setOnClickListener(this);

        try{
            EventBus.getDefault().register(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addCategoryItems() {

        SharedPreferences preferences=getSharedPreferences("Categories",Context.MODE_PRIVATE);
        int num=preferences.getInt("num",0);

        String[] categories=new String[num];

        if(num!=0) {
            for (int i = 0; i < num; i++)
                categories[i] = preferences.getString(String.valueOf(i), null);
        }else {
            categories=new String[]{"sadMoment", "embarrassingMoment", "memorableMoment", "collegeLife", "depressed", "cleverMoment"};
        }

        Arrays.sort(categories);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,categories);

        sCategory.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        /*
            showing dialog to confirm discard of snippet
         */
        final AlertDialog.Builder alertdialog=new AlertDialog.Builder(this);
        alertdialog.setTitle("Discard changes?");
        alertdialog.setMessage("The Snippet will be discarded");
        alertdialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertdialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.save_new_snippet:
                /*
                    getting text from edit texts
                 */
                String title=etTitle.getText().toString();
                String content=etContent.getText().toString();
                String category=sCategory.getSelectedItem().toString();

                /*
                    isAnonymous check
                 */
                boolean isAnonymous=switchAnonymous.isChecked();
                int anonymous;
                if(isAnonymous)
                    anonymous=1;
                else
                    anonymous=0;


                /*
                    condition checks
                 */
                if(title.trim().equals("")){
                    Toast.makeText(getApplicationContext(),"Title cannot be empty",Toast.LENGTH_SHORT).show();
                }else if(content.trim().equals("")){
                    Toast.makeText(getApplicationContext(),"Snippet cannot be empty",Toast.LENGTH_SHORT).show();
                }else if(tvSelectCategory.getVisibility()==View.VISIBLE){
                    Toast.makeText(getApplicationContext(),"Please select a category",Toast.LENGTH_SHORT).show();
                }else {
                    if(type.equals("new")) {
                        if (tab.equals("feed")) {
                            /*
                                sharing snippet
                             */
                            Snippet snippet = new Snippet(getApplicationContext());
                            snippet.shareSnippet(title, category, content,anonymous);
                        } else if (tab.equals("diary")) {
                            /*
                                add to diary
                             */
                            DiarySnippet diarySnippet = new DiarySnippet(getApplicationContext());
                            diarySnippet.addToDiary(title, category, content);
                        }
                    }else if(type.equals("edit")){
                        if(tab.equals("diary")){
                            /*
                                edit diary snippet
                             */
                            DiarySnippet diarySnippet=new DiarySnippet(getApplicationContext());
                            diarySnippet.editDiarySnippet(snippetID,title,category,content);
                        }else if(tab.equals("timeline")){
                            /*
                                edit timeline snippet
                             */
                            Snippet snippet=new Snippet(getApplicationContext());
                            snippet.editSnippet(snippetID,content,category,title,anonymous);
                        }
                    }
                }
                break;
            case R.id.tvSelectCategory:
                tvSelectCategory.setVisibility(View.GONE);
                sCategory.setVisibility(View.VISIBLE);
                sCategory.performClick();
                break;
        }
    }

    public void onEvent(ShareEvent shareEvent){
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            EventBus.getDefault().unregister(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
