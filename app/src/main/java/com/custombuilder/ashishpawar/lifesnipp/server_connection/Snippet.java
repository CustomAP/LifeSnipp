package com.custombuilder.ashishpawar.lifesnipp.server_connection;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.CommentEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FeedEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.ShareEvent;
import com.custombuilder.ashishpawar.lifesnipp.Home.CommentItem;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 21/6/17.
 */

public class Snippet {

    private Context context;
    private AQuery aQuery;

    /*
        links
     */
    private String shareSnippet="http://www.lifesnipp.com/index.php/Snippets/shareSnippet";
    private String likeSnippet="http://www.lifesnipp.com/index.php/Snippets/likeSnippet";
    private String viewSnippet="http://www.lifesnipp.com/index.php/Snippets/viewSnippet";
    private String getComments="http://www.lifesnipp.com/index.php/Snippets/getComments";
    private String comment="http://www.lifesnipp.com/index.php/Snippets/comment";
    private String editSnippet="http://www.lifesnipp.com/index.php/Snippets/editSnippet";
    private String deleteSnippet="http://www.lifesnipp.com/index.php/Snippets/deleteSnippet";
    private String updateSnippet="http://www.lifesnipp.com/index.php/Snippets/getSnippet";

    private String sessionIDString;
    private ArrayList<CommentItem> commentItems;

    public Snippet(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        SessionID sessionID=new SessionID(context);
        sessionIDString=sessionID.getSessionID();
        commentItems=new ArrayList<>();
    }

    public void shareSnippet(final String title, final String category, final String content, final int anonymous){

        Map<String,Object> shareSnippetParams =new HashMap<>();
        shareSnippetParams.put("sessionID",sessionIDString);
        shareSnippetParams.put("title",title);
        shareSnippetParams.put("category",category);
        shareSnippetParams.put("content",content);
        shareSnippetParams.put("isAnonymous",anonymous);

        aQuery.ajax(shareSnippet,shareSnippetParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                Log.d("sharesnippet","here");
                int code=0;
                if(object!=null){
                    try{
                        Log.d("shareSnippet","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("shareSnippet","object=null");
                    Toast.makeText(context,"Failed to share snippet",Toast.LENGTH_SHORT).show();
                }
                if(status.getCode()==200 &&status.getCode()<300) if (code != 0) {
                    /*
                        getting date and time from server
                     */
                    String date = null, time = null;
                    try {
                        date = object.getString("date");
                        time = object.getString("time");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Log.d("shareSnippet", "code=0");

                    Toast.makeText(context, "Snippet shared", Toast.LENGTH_SHORT).show();

                    /*
                        getting userID, firstname, lastname, bio from SP
                     */
                    SharedPreferences preferences = context.getSharedPreferences("Profile", Context.MODE_PRIVATE);
                    int userID = preferences.getInt("userID", 0);
                    String firstName = preferences.getString("firstName", null);
                    String lastName = preferences.getString("lastName", null);
                    String bio = preferences.getString("bio", null);

                    SnippetItem snippetItem = new SnippetItem(code, userID, content, "0", firstName + " " + lastName, "0", "0", date, time, title, category, bio,0,anonymous);

                    /*
                        saving snippets in local db
                     */
                    DbHelper helper = new DbHelper(context);
                    helper.addTimelineSnippet(snippetItem);

                    updateRecyclerViewInTimelineFragment();
                } else {
                    Log.d("shareSnippet", "code!=0");
                    Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateRecyclerViewInTimelineFragment() {
        EventBus.getDefault().post(new ShareEvent(true,false));
    }

    public void like(int snippetID,int like){
        Map<String,Object> likeParams =new HashMap<>();
        likeParams.put("sessionID",sessionIDString);
        likeParams.put("snippetID",snippetID);
        likeParams.put("like",like);

        aQuery.ajax(likeSnippet,likeParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null) {
                    try {
                        Log.d("likeSnippet","object!=null");
                        code=object.getInt("ResultSet");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("likeSnippet","object=null");
                    Toast.makeText(context,"Failed to like snippet",Toast.LENGTH_SHORT).show();
                }

                if(status.getCode()==200 &&status.getCode()<300){
                    if(code==0)
                        Log.d("likeSnippet","code=0");
                    else
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void view(int snippetID){
        Map<String,Object> viewParams=new HashMap<>();
        viewParams.put("sessionID",sessionIDString);
        viewParams.put("snippetID",snippetID);

        aQuery.ajax(viewSnippet,viewParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {

            }
        });
    }

    public void getComments(final int snippetID, int num) {
        Map<String, Object> commentParams = new HashMap<>();
        commentParams.put("sessionID", sessionIDString);
        commentParams.put("snippetID", snippetID);
        commentParams.put("num", num);

        aQuery.ajax(getComments, commentParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String firstName, lastName, comment, date;
                int num = 0, commentID, userID;
                if (object != null) {
                    try {
                        Log.d("getComments", "object!=null");
                        num = object.getInt("num");
                        String comments = object.getString("comments");

                        JSONObject reader = new JSONObject(comments);

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            firstName = obj.getString("firstName");
                            lastName = obj.getString("lastName");
                            comment = obj.getString("comment");
                            date = obj.getString("commentDate");
                            commentID = obj.getInt("commentID");
                            userID = obj.getInt("userID");

                            commentItems.add(new CommentItem(commentID, firstName + " " + lastName, comment, date, userID, snippetID));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("getComments", "object=null");
                    Toast.makeText(context, "Failed to fetch comments", Toast.LENGTH_SHORT).show();
                }
                if (status.getCode() == 200 && status.getCode() < 300) {
                    /*
                        saving comments in local db
                     */
                    DbHelper helper = new DbHelper(context);
                    for (int i = 0; i < num; i++)
                        helper.addComments(commentItems.get(i));

                    EventBus.getDefault().post(new CommentEvent(true,false));
                }
            }
        });
    }

    public void comment(int snippetID, String commentContent) {
        Map<String,Object> commentParams=new HashMap<>();
        commentParams.put("sessionID",sessionIDString);
        commentParams.put("snippetID",snippetID);
        commentParams.put("comment",commentContent);

        aQuery.ajax(comment,commentParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("comment","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("comment","object=null");
                    Toast.makeText(context,"Failed to comment",Toast.LENGTH_SHORT).show();
                }

                if (status.getCode() == 200 && status.getCode() < 300) {
                    if(code!=0)
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    else if(code==0)
                        EventBus.getDefault().post(new CommentEvent(false,true));
                }
            }
        });
    }

    public void editSnippet(final int snippetID, final String content, final String category, final String title,int anonymous){
        Map<String,Object> editParams=new HashMap<>();
        editParams.put("sessionID",sessionIDString);
        editParams.put("snippetID",snippetID);
        editParams.put("content",content);
        editParams.put("category",category);
        editParams.put("title",title);
        editParams.put("isAnonymous",anonymous);

        aQuery.ajax(editSnippet,editParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("editSnippet","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("editSnippet","object=null");
                    Toast.makeText(context,"Failed to edit snippet",Toast.LENGTH_SHORT).show();
                }
                if (status.getCode() == 200 && status.getCode() < 300) {
                    if(code!=0)
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    else if(code==0){
                        /*
                            editing snippet in local db
                         */
                        DbHelper helper=new DbHelper(context);
                        helper.editSnippet(snippetID,title,category,content);
                        EventBus.getDefault().post(new ShareEvent(true,false));
                    }
                }

            }
        });
    }


    public void deleteSnippet(final int snippetID){
        Map<String,Object> deleteParams=new HashMap<>();
        deleteParams.put("sessionID",sessionIDString);
        deleteParams.put("snippetID",snippetID);

        aQuery.ajax(deleteSnippet,deleteParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("deleteSnippet","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("deleteSnippet","object=null");
                    Toast.makeText(context,"Failed to delete snippet",Toast.LENGTH_SHORT).show();
                }
                if(status.getCode()==200&&status.getCode()<300){
                    if(code!=0)
                        Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    else{
                        /*
                            deleting snippet and its related notifications from local db
                         */
                        DbHelper helper=new DbHelper(context);
                        helper.deleteSnippet(snippetID);
                        helper.deleteNotifications(snippetID);

                        EventBus.getDefault().post(new ShareEvent(true,false));
                    }
                }
            }
        });
    }

    public void updateSnippet(final int snippetID,final String origin){
        Map<String,Object> snippetParams=new HashMap<>();
        snippetParams.put("sessionID",sessionIDString);
        snippetParams.put("snippetID",snippetID);

        aQuery.ajax(updateSnippet,snippetParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String title, category, content, date, time, firstName, lastName, bio;
                int userID, postID, views, likes, comments,isLiked,anonymous;
                SnippetItem snippetItem=null;
                if(object!=null) {
                    Log.d("updateSnippet","object!=null");
                    try {

                        JSONObject obj=object.getJSONObject("0");

                        title = obj.getString("title");
                        category = obj.getString("category");
                        content = obj.getString("content");
                        date = obj.getString("uploadDate");
                        time = obj.getString("uploadTime");
                        userID = obj.getInt("userID");
                        postID = obj.getInt("postID");
                        views = obj.getInt("views");
                        likes = obj.getInt("likes");
                        comments = obj.getInt("comments");
                        firstName = obj.getString("firstName");
                        lastName = obj.getString("lastName");
                        bio = obj.getString("bio");
                        isLiked = obj.getInt("isLiked");
                        anonymous = obj.getInt("isAnonymous");

                        snippetItem=new SnippetItem(postID, userID, content, views + "", firstName + " " + lastName, likes + "", comments + "", date, time, title, category, bio,isLiked,anonymous);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    EventBus.getDefault().post(new FetchFailed(true));
                    Log.d("updateSnippet","object=null");
                    Toast.makeText(context,"Failed to update snippet",Toast.LENGTH_SHORT).show();
                }
                if(status.getCode()==200&&status.getCode()<300){
                    /*
                        updating snippet in local db
                     */
                    Log.d("updateSnippet","statuscode=200");
                    DbHelper helper=new DbHelper(context);
                    helper.updateSnippet(snippetItem,origin);
                    EventBus.getDefault().post(new FeedEvent(true));
                }
            }
        });
    }
}


