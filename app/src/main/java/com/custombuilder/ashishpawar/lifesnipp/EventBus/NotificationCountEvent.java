package com.custombuilder.ashishpawar.lifesnipp.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 8/8/17.
 */

public class NotificationCountEvent {
    private int count;

    public int getCount() {
        return count;
    }

    public NotificationCountEvent(int count) {

        this.count = count;
    }
}
