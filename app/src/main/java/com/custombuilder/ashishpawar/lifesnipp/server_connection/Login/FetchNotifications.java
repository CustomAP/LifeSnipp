package com.custombuilder.ashishpawar.lifesnipp.server_connection.Login;

import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.NotificationCountEvent;
import com.custombuilder.ashishpawar.lifesnipp.Notifications.NotificationsItem;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.SessionID;
import com.custombuilder.ashishpawar.lifesnipp.server_connection.Snippet;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/6/17.
 */

public class FetchNotifications {
    private Context context;
    AQuery aQuery;
    String sessionIDString;
    String getNotifications = "http://www.lifesnipp.com/index.php/Notifications/getNotifications";

    private ArrayList<NotificationsItem> notificationsItems;

    public FetchNotifications(Context context) {
        this.context = context;
        aQuery = new AQuery(context);
        SessionID sessionID = new SessionID(context);
        sessionIDString = sessionID.getSessionID();
    }

    public void getNotifications() {
        notificationsItems = new ArrayList<>();

        Map<String, Object> notificationParams = new HashMap<>();
        notificationParams.put("sessionID", sessionIDString);
        aQuery.ajax(getNotifications, notificationParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                ArrayList<NotificationsItem> notificationsItems = new ArrayList<NotificationsItem>();
                String notificationContent, notifType;
                int snippet_user_ID, num = 0;
                String[] dateTime = getDateTime();
                if (object != null) {
                    try {
                        Log.d("getNotifications", "object!=null");
                        num = object.getInt("num");
                        String notifications = object.getString("notifications");
                        String snippet_user_IDs = object.getString("snippet_user_ID");
                        String type = object.getString("type");

                        JSONObject notificationsReader = new JSONObject(notifications);
                        JSONObject snippet_user_IDReader = new JSONObject(snippet_user_IDs);
                        JSONObject typeReader = new JSONObject(type);

                        for (int i = 0; i < num; i++) {
                            notificationContent = notificationsReader.getString(String.valueOf(i));
                            snippet_user_ID = snippet_user_IDReader.getInt(String.valueOf(i));
                            notifType = typeReader.getString(String.valueOf(i));

                            notificationsItems.add(new NotificationsItem(notificationContent, notifType, dateTime[0], dateTime[1], 0, snippet_user_ID));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("getNotifications", "object=null");
                }
                if (status.getCode() == 200) {
                    DbHelper helper = new DbHelper(context);
                    for (int i = 0; i < num; i++) {
                        /*
                            adding notifications in db
                         */
                        helper.addNotification(notificationsItems.get(i));

                        Snippet snippet = new Snippet(context);

                        /*
                            update snippet for which notification is received
                         */
                        if (notificationsItems.get(i).getNotifType().equals("like") || notificationsItems.get(i).getNotifType().equals("comment"))
                            snippet.updateSnippet(notificationsItems.get(i).getSnippet_user_ID(), "timeline");
                    }

                    EventBus.getDefault().post(new NotificationCountEvent(num));
                }
            }
        });

    }


    private String[] getDateTime() {
        Date date = new Date();

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = sdfDate.format(date);

        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        String timeString = sdfTime.format(date);

        String dateArray[] = {dateString, timeString};

        return dateArray;
    }
}
