package com.custombuilder.ashishpawar.lifesnipp.server_connection;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FeedEvent;
import com.custombuilder.ashishpawar.lifesnipp.EventBus.FetchFailed;
import com.custombuilder.ashishpawar.lifesnipp.Home.SnippetItem;
import com.custombuilder.ashishpawar.lifesnipp.Login;
import com.custombuilder.ashishpawar.lifesnipp.MainActivity;
import com.custombuilder.ashishpawar.lifesnipp.database.helper.DbHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/6/17.
 */

public class FetchFeed {
    private Context context;
    private ArrayList<SnippetItem> snippetItems;
    private AQuery aQuery;
    DbHelper helper;
    private int length;

    private String getFeed="http://www.lifesnipp.com/index.php/Snippets/getFeed";

    public FetchFeed(Context context){
        this.context=context;
        aQuery=new AQuery(context);
        helper=new DbHelper(context);
        snippetItems=new ArrayList<>();
    }

    public void getFeed(final String origin){

        /*
            getting lowerlimit
         */
        final SharedPreferences preferences=context.getSharedPreferences("AppInfo",Context.MODE_PRIVATE);
        int lowerLimit=preferences.getInt("lowerLimit",0);

        Log.d("lowerLimit",lowerLimit+"");
        SessionID sessionID=new SessionID(context);
        String sessionIDString=sessionID.getSessionID();

        Map<String,Object> feedParams=new HashMap<>();
        feedParams.put("sessionID",sessionIDString);
        feedParams.put("lowerLimit",lowerLimit);

        aQuery.ajax(getFeed,feedParams, JSONObject.class,new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {

                if(object!=null) {
                    Log.d("getFeed","object!=null");
                    String title, category, content, date, time, firstName, lastName, bio;
                    int userID, postID, views, likes, comments,isLiked,anonymous;
                    try {
                        length = object.getInt("num");

                        String feed = object.getString("feed");

                        JSONObject reader = new JSONObject(feed);

                        for (int i = 0; i < length; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            title = obj.getString("title");
                            category = obj.getString("category");
                            content = obj.getString("content");
                            date = obj.getString("uploadDate");
                            time = obj.getString("uploadTime");
                            userID = obj.getInt("userID");
                            postID = obj.getInt("postID");
                            views = obj.getInt("views");
                            likes = obj.getInt("likes");
                            comments = obj.getInt("comments");
                            firstName = obj.getString("firstName");
                            lastName = obj.getString("lastName");
                            bio = obj.getString("bio");
                            isLiked=obj.getInt("isLiked");
                            anonymous=obj.getInt("isAnonymous");
                            snippetItems.add(new SnippetItem(postID, userID, content, views + "", firstName + " " + lastName, likes + "", comments + "", date, time, title, category, bio,isLiked,anonymous));
                        }

                        for (int i = 0; i < length; i++)
                            helper.addSnippet(snippetItems.get(i));

                        Log.d("feed number",snippetItems.size()+"");

                        SharedPreferences.Editor editor=preferences.edit();
                        if(snippetItems.size()>0)
                        editor.putInt("lowerLimit",snippetItems.get(0).getSnippetID());
                        editor.apply();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    EventBus.getDefault().post(new FetchFailed(true));
                    Log.d("getFeed","object=null");
                    Toast.makeText(context,"Failed to fetch feed",Toast.LENGTH_SHORT).show();
                }
                if(status.getCode()==200 &&status.getCode()<300){
                    /*
                        starting main activity or sending event
                     */
                    if(origin.equals("Login"))
                        startMainActivity();
                    else if(origin.equals("FeedFragment"))
                        sendEvent();
                }
            }
        });

    }

    private void sendEvent() {
        EventBus.getDefault().post(new FeedEvent(true));
    }

    private void startMainActivity() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                /*
                    kill login activity
                 */
                Login.isLoggedInToKill=true;

                /*
                    storing lowerlimit
                 */
                SharedPreferences preferences=context.getSharedPreferences("Profile",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1=preferences.edit();
                editor1.putInt("lowerLimit",snippetItems.get(0).getSnippetID());
                editor1.apply();

                /*
                    setting isLoggedIn in SP
                 */
                SharedPreferences sharedPreferences=context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putBoolean("isLoggedIn",true);
                editor.apply();

                /*
                    start mainactivity
                 */
                Intent i=new Intent(context, MainActivity.class);
                context.startActivity(i);
            }
        }).start();

    }
}
